import 'dart:io';

import 'package:appresort/src/app.dart';
import 'package:appresort/src/presentation/bloc/repository/app_repository.dart';
import 'package:flutter/material.dart';
import 'package:get_storage/get_storage.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await initServices();
  runApp(const AppRepository(child: App()));
}

Future<void> initServices() async {
  HttpOverrides.global = MyHttpOverrides();
  await GetStorage.init();
}

class MyHttpOverrides extends HttpOverrides {
  @override
  HttpClient createHttpClient(SecurityContext? context) {
    return super.createHttpClient(context)
      ..badCertificateCallback = (X509Certificate cert, String host, int port) => true;
  }
}
