import 'package:appresort/src/presentation/core/navigator/routes.dart';
import 'package:appresort/src/presentation/core/themes/theme_style.dart';
import 'package:appresort/src/presentation/core/utils/i18.dart';
import 'package:appresort/src/presentation/shared/logger/logger_utils.dart';
import 'package:appresort/src/presentation/views/splash/splash_view.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:get/get.dart';

class App extends StatelessWidget {
  const App({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    ThemeStyle.init(context);
    SystemChrome.setSystemUIOverlayStyle(
      const SystemUiOverlayStyle(
        systemNavigationBarColor: Colors.transparent, // navigation bar color
        statusBarColor: Colors.transparent, // status bar color
        statusBarBrightness: Brightness.light, //status bar brigtness
        statusBarIconBrightness: Brightness.dark, //status barIcon Brightness
        systemNavigationBarIconBrightness: Brightness.dark, //navigation bar icon
      ),
    );
    return GetMaterialApp(
      title: 'App Resort',
      debugShowCheckedModeBanner: false,
      getPages: Routes.routes,
      theme: ThemeStyle.lightTheme,
      locale: Get.deviceLocale,
      translations: I18(),
      defaultTransition: Transition.fadeIn,
      home: const SplashView(),
      localizationsDelegates: const [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
      ],
      localeResolutionCallback: (deviceLocale, supporteLocate) => supporteLocate.first,
      supportedLocales: const [
        Locale('es'), // Español
        Locale('en'), // English
      ],
      enableLog: true,
      logWriterCallback: Logger.write,
    );
  }
}
