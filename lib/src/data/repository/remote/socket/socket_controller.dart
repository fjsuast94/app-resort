import 'package:flutter/foundation.dart';
import 'package:socket_io_client/socket_io_client.dart' as io;

class SocketController {
  SocketController({
    required String baseUrl,
  }) : _baseUrl = baseUrl;

  late io.Socket _socket;
  final String _baseUrl;

  void connect() {
    _socket = io.io(_baseUrl, io.OptionBuilder().setTransports(['websocket']).build());

    _socket.onConnect((_) {
      debugPrint('connect $_baseUrl');
    });

    _socket.onConnectError((_) {
      debugPrint('$_');
    });

    _socket.onDisconnect((_) => debugPrint('disconnect'));
    _socket.connect();
  }

  void emitEvent(String event, {dynamic params}) {
    _socket.emit(event, params);
  }

  void recibeEvent(String event, ValueChanged<dynamic> callback) {
    _socket.on(event, (data) {
      callback(data);
    });
  }

  void disconnect() {
    debugPrint('disconnect');
    _socket.disconnect();
  }
}
