import 'dart:convert';
import 'package:flutter/foundation.dart';
import 'package:appresort/src/data/model/conekta.dart';
import 'package:appresort/src/data/model/payment.dart';
import 'package:appresort/src/data/repository/local/http/http_dio.dart';

import 'package:device_info/device_info.dart';
import 'package:http/http.dart' as http;
import 'dart:io' show Platform;

class ConektaFlutterTokenizer {
  ConektaFlutterTokenizer({
    required HttpDio http,
  }) : _http = http;

  final HttpDio _http;
  final String _conektaTokenizationApiUrl = "https://api.conekta.io/tokens";

  dynamic _buildConektaTokenizeRequestHeaders() async => {
        "Accept": "application/vnd.conekta-v1.0.0+json",
        "Conekta-Client-User-Agent":
            "{\"agent\": \"Conekta JavascriptBindings-AJAX/v1.0.0 build 2.0.14\"}",
        "Authorization": "Basic " + await _encodeKey()
      };

  Future<String> _encodeKey() async {
    String _publicKeys = await _getPublicKey();
    List<int> utf16KeyBytes = _publicKeys.codeUnits;
    return const Base64Encoder().convert(utf16KeyBytes);
  }

  Future<String> _getPublicKey() async => await _conektaKey();

  Future<String> _getDeviceId() async {
    DeviceInfoPlugin deviceInfoPlugin = DeviceInfoPlugin();

    if (Platform.isAndroid) {
      AndroidDeviceInfo androidDeviceInfo = await deviceInfoPlugin.androidInfo;
      return androidDeviceInfo.androidId;
    }

    IosDeviceInfo iosDeviceInfo = await deviceInfoPlugin.iosInfo;
    return iosDeviceInfo.identifierForVendor;
  }

  dynamic _buildUrlEncodedForm(PaymentMethod paymentMethod) async {
    String deviceId = await _getDeviceId();

    return {
      "card[name]": paymentMethod.name,
      "card[number]": paymentMethod.number,
      "card[exp_month]": paymentMethod.expirationMonth,
      "card[exp_year]": paymentMethod.expirationYear,
      "card[cvc]": paymentMethod.cvv,
      "card[device_fingerprint]": deviceId
    };
  }

  Future<String> _conektaKey() async {
    final response = await _http.post(route: "/conekta/publicKey");
    return response.status ? response.data['public_key'] ?? "" : "";
  }

  Future<ConektaModel?> tokenizePaymentMethod(PaymentMethod paymentMethod) async {
    dynamic requestHeaders = await _buildConektaTokenizeRequestHeaders();
    dynamic requestBody = await _buildUrlEncodedForm(paymentMethod);

    var conektaResponse = await http.post(
      Uri.parse(_conektaTokenizationApiUrl),
      headers: requestHeaders,
      body: requestBody,
    );

    debugPrint(conektaResponse.statusCode.toString());

    if (conektaResponse.statusCode == 200) {
      return ConektaModel.fromJson(json.decode(conektaResponse.body));
    }

    if (conektaResponse.statusCode == 422) {
      return ConektaModel.fromJson(json.decode(conektaResponse.body));
    }

    if (conektaResponse.statusCode == 401) {
      return ConektaModel.fromJson(json.decode(conektaResponse.body));
    }

    return null;
  }
}
