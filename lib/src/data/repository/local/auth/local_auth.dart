import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:appresort/src/data/repository/local/storage/storage_repository.dart';
import 'package:device_info/device_info.dart';
import 'package:flutter/services.dart';
import 'package:local_auth/auth_strings.dart';
import 'package:local_auth/local_auth.dart';

class LocalAuthRepository {
  LocalAuthRepository({
    required LocalAuthentication localAuth,
    required DeviceInfoPlugin deviceInfo,
    required StorageRepository storage,
  })  : _localAuth = localAuth,
        _deviceInfo = deviceInfo,
        _storage = storage;

  final LocalAuthentication _localAuth;
  final DeviceInfoPlugin _deviceInfo;
  final StorageRepository _storage;

  final String title = "App Resort";

  Future<bool> isAndroid8OrInferiror() async {
    if (Platform.isAndroid) {
      AndroidDeviceInfo android = await _deviceInfo.androidInfo;
      return !_getVersionAndroid(android.version.release);
    }
    return true;
  }

  Future<bool> hasBiometrics() async {
    try {
      return await _localAuth.canCheckBiometrics;
    } on PlatformException catch (e) {
      debugPrint(e.message);
      return false;
    }
  }

  Future<List<BiometricType>> getBiometrics() async {
    try {
      return await _localAuth.getAvailableBiometrics();
    } on PlatformException catch (e) {
      debugPrint(e.message);
      return <BiometricType>[];
    }
  }

  Future<bool> fingerprint() async {
    final isAvailable = await hasBiometrics();
    if (!isAvailable) return false;

    try {
      return await _localAuth.authenticate(
        localizedReason: 'Escanea tu huella para ingresar',
        androidAuthStrings: AndroidAuthMessages(
          signInTitle: "Ingresa a $title",
          cancelButton: 'Usar contraseña',
          biometricHint: "",
        ),
        iOSAuthStrings: const IOSAuthMessages(
          cancelButton: 'Usar contraseña',
          lockOut: 'Que es ',
        ),
        useErrorDialogs: true,
        stickyAuth: true,
      );
    } on PlatformException catch (e) {
      debugPrint(e.message);
      return false;
    }
  }

  Future<bool> faceID() async {
    final isAvailable = await hasBiometrics();
    if (!isAvailable) return false;

    try {
      return await _localAuth.authenticate(
        localizedReason: 'Escanear Face ID para autenticar',
        androidAuthStrings: AndroidAuthMessages(
          signInTitle: 'Ingresa a $title',
          cancelButton: 'Usar contraseña',
          biometricHint: '',
        ),
        iOSAuthStrings: const IOSAuthMessages(
          cancelButton: 'Usar contraseña',
        ),
        useErrorDialogs: true,
        stickyAuth: true,
      );
    } on PlatformException catch (e) {
      debugPrint(e.message);
      return false;
    }
  }

  Future<bool> authenticate() async {
    if (_storage.biometric) {
      if (await isAndroid8OrInferiror()) {
        final hasBiometic = await hasBiometrics();
        //print(hasBiometic);
        if (hasBiometic) {
          final biometrics = await getBiometrics();
          //print(biometrics);
          if (biometrics.contains(BiometricType.fingerprint)) {
            return await fingerprint();
          } else if (biometrics.contains(BiometricType.face)) {
            return await faceID();
          }
        }
        return false;
      }
    }
    return false;
  }

  bool _getVersionAndroid(String version) => ['5', '6', '7', '8'].any((e) => version.contains(e));
}
