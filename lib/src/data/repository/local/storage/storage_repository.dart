import 'package:appresort/src/data/model/user.dart';
import 'package:get_storage/get_storage.dart';

class StorageRepository {
  StorageRepository({
    required GetStorage storage,
  }) : _storage = storage;

  final GetStorage _storage;

  Future<void> clear() async {
    final _boarding = onboarding;
    final _qrInitial = qrInitial;

    await _storage.erase();
    onboarding = _boarding;
    qrInitial = _qrInitial;
  }

  // Setter y getters de datos
  set server(String valor) => _storage.write('server', valor);
  String get server =>
      _storage.read('server') ?? 'https://imperial.appresort.com';

  set serverSocket(String valor) => _storage.write('serverSocket', valor);
  String get serverSocket => _storage.read('serverSocket') ?? '$server:3000';

  set api(String valor) => _storage.write('api', valor);
  String get api => _storage.read('api') ?? '$server/server/api';

  set page(String valor) => _storage.write('page', valor);
  String get page => _storage.read('page') ?? '/';

  set biometric(bool valor) => _storage.write('biometric', valor);
  bool get biometric => _storage.read('biometric') ?? false;

  set onboarding(bool valor) => _storage.write('onboarding', valor);
  bool get onboarding => _storage.read('onboarding') ?? true;

  set qrInitial(bool valor) => _storage.write('qrInitial', valor);
  bool get qrInitial => _storage.read('qrInitial') ?? true;

  set user(UserModel userModel) {
    userModel.photo = userModel.photo.isEmpty
        ? "$server/../site_media/assets/images/profile-image.png"
        : userModel.photo;
    _storage.write('user', userModel.toJson());
  }

  UserModel get user => UserModel.fromJson(_storage.read('user') ?? {});
}
