import 'package:appresort/src/data/model/chat.dart';
import 'package:appresort/src/data/model/list_page.dart';
import 'package:appresort/src/data/model/response.dart';
import 'package:appresort/src/data/model/ticket.dart';
import 'package:appresort/src/data/model/time_line.dart';
import 'package:appresort/src/data/repository/local/http/http_dio.dart';
import 'package:appresort/src/data/repository/local/storage/storage_repository.dart';
import 'package:dio/dio.dart';

class TicketUseCase {
  TicketUseCase({
    required HttpDio http,
    required StorageRepository storage,
  })  : _http = http,
        _storage = storage;

  final HttpDio _http;
  final StorageRepository _storage;

  Future<ListPage<TicketsModel>> tickets({int page = 1}) async {
    final response = await _http.post(
      route: '/app/tickets',
      data: {
        "idpropietario": _storage.user.idpropietario,
        "sistema": 1,
        "page": page,
      },
    );

    final List<TicketsModel> list = <TicketsModel>[];
    String message = "";
    response.status
        ? response.data.forEach((e) => list.add(TicketsModel.fromJson(e)))
        : message = response.message!;

    return ListPage<TicketsModel>(
      itemList: list,
      totalCount: list.length,
      message: message,
    );
  }

  Future<List<CatalogoTicketModel>> catalogoTicket() async {
    final List<CatalogoTicketModel> list = <CatalogoTicketModel>[];
    final response = await _http.post(
      route: '/app/catalogosTickets',
      data: {
        "idpropietario": _storage.user.iddesarrollo,
        "sistema": 0,
      },
    );

    if (response.status) {
      response.data.forEach((item) => list.add(CatalogoTicketModel.fromJson(item)));
    }
    return list;
  }

  Future<ResponseModel> addTicket({
    required int idCatTicket,
    required String message,
    MultipartFile? image,
  }) async {
    return _http.postFormData(
      route: '/app/agregarTicket',
      data: {
        "id_cat_ticket": idCatTicket,
        "idpropietario": _storage.user.idpropietario,
        "mensaje": message,
        "image": image,
      },
    );
  }

  Future<ListPage<TimeLineModel>> ticketTimeLine({int idTicket = 0, int page = 1}) async {
    final response = await _http.post(
      route: '/app/ticketTimeLine',
      data: {
        "id_ticket": idTicket,
        "page": page,
      },
    );

    final List<TimeLineModel> list = <TimeLineModel>[];
    String message = "";
    response.status
        ? response.data.forEach((e) => list.add(TimeLineModel.fromJson(e)))
        : message = response.message!;
    return ListPage<TimeLineModel>(
      itemList: list,
      totalCount: list.length,
      message: message,
    );
  }

  Future<ListPage<ChatModel>> ticketChat({int idTicket = 0, int page = 1}) async {
    final response = await _http.post(
      route: '/app/ticketChat',
      data: {
        "id_ticket": idTicket,
        "id_user": _storage.user.id,
        "page": page,
      },
    );

    final List<ChatModel> list = <ChatModel>[];
    String message = "";
    response.status
        ? response.data.forEach((e) => list.add(ChatModel.fromJson(e)))
        : message = response.message!;

    return ListPage<ChatModel>(
      itemList: list,
      totalCount: list.length,
      message: message,
    );
  }

  Future<ResponseModel> ticketChatAdd({int idTicket = 0, required String message}) {
    return _http.post(
      route: '/app/ticketChatAdd',
      data: {
        "id_ticket": idTicket,
        "id_user": _storage.user.id,
        "message": message,
      },
    );
  }
}
