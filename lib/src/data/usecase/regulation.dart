import 'package:appresort/src/data/model/list_page.dart';
import 'package:appresort/src/data/model/regulation.dart';
import 'package:appresort/src/data/repository/local/http/http_dio.dart';
import 'package:appresort/src/data/repository/local/storage/storage_repository.dart';

class RegulationUseCase {
  RegulationUseCase({
    required HttpDio http,
    required StorageRepository storage,
  })  : _http = http,
        _storage = storage;

  final HttpDio _http;
  final StorageRepository _storage;

  Future<ListPage<RegulationModel>> regulations(int page) async {
    final response = await _http.post(
      route: '/app/regulations',
      data: {
        "desarrollo": _storage.user.iddesarrollo,
        "page": page,
      },
    );
    final List<RegulationModel> list = <RegulationModel>[];
    String message = "";
    response.status
        ? response.data.forEach((e) => list.add(RegulationModel.fromJson(e)))
        : message = response.message!;

    return ListPage<RegulationModel>(
      itemList: list,
      totalCount: list.length,
      message: message,
    );
  }
}
