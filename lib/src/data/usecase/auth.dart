import 'package:appresort/src/data/model/response.dart';
import 'package:appresort/src/data/model/user.dart';
import 'package:appresort/src/data/repository/local/auth/local_auth.dart';
import 'package:appresort/src/data/repository/local/http/http_dio.dart';
import 'package:appresort/src/data/repository/local/storage/storage_repository.dart';
import 'package:appresort/src/presentation/shared/logger/logger_utils.dart';
import 'package:jwt_decoder/jwt_decoder.dart';

class AuthUseCase {
  AuthUseCase({
    required HttpDio http,
    required StorageRepository storage,
    required LocalAuthRepository localAuth,
  })  : _http = http,
        _localAuth = localAuth,
        _storage = storage;

  final HttpDio _http;
  final StorageRepository _storage;
  final LocalAuthRepository _localAuth;

  Future<bool> tokenIsExpired() async {
    try {
      await Future<void>.delayed(const Duration(milliseconds: 800));
      return !JwtDecoder.isExpired(_storage.user.token);
    } on FormatException catch (e) {
      Logger.write(e.message);
      return false;
    }
  }

  Future<bool> showQr() async {
    const String baseUrl = "https://imperial.appresort.com/server/api";
    _http.setBaseUrl(baseUrl);
    final response = await _http.post(
      route: '/app/showqr',
    );
    if (!response.status) {
      _http.setBaseUrl("${response.data["url"]}/server/api");
    }
    return response.status;
  }

  Future<bool> qrInitial() async {
    await Future<void>.delayed(const Duration(milliseconds: 400));
    return _storage.qrInitial;
  }

  Future<void> setUrlBase(String url) async {
    await Future<void>.delayed(const Duration(milliseconds: 400));
    _storage.qrInitial = false;
    _storage.server = url;
    _http.setBaseUrl(_storage.api);
  }

  Future<String> login(String username, String password) async {
    String message = '';
    final response = await _http.post(
      route: '/app/login',
      data: {
        "username": username,
        "password": password,
      },
    );
    response.status
        ? _storage.user = UserModel.fromJson(response.data)
        : message = response.message!;
    return message;
  }

  Future<bool> hasBiometrics() async {
    return await _localAuth.hasBiometrics() && !_storage.biometric;
  }

  Future<bool> hasUserBiometric() async {
    return !_storage.biometric && _storage.user.fullname.isEmpty;
  }

  Future<bool> logout() async {
    _storage.clear();
    return true;
  }

  UserModel getUser() {
    return _storage.user;
  }

  Future<void> setUser(UserModel user) async {
    _storage.user = user;
  }

  Future<bool> getBiometric() async {
    return _storage.biometric;
  }

  Future<void> setBiometric(bool biometric) async {
    _storage.biometric = biometric;
  }

  Future<bool> getOnboarding() async {
    return _storage.onboarding;
  }

  Future<void> setOnboarding(bool onboarding) async {
    _storage.onboarding = onboarding;
  }

  Future<bool> authenticate() async {
    return await _localAuth.authenticate();
  }

  Future<bool> isAndroid8OrInferiror() async {
    return await _localAuth.isAndroid8OrInferiror();
  }

  Future<ResponseModel> changePassWord(String password) async {
    ResponseModel response = ResponseModel(
      status: false,
      message: '',
    );

    final request = await _http.post(
      route: '/v2/app/change-password',
      data: {
        "user": _storage.user.id,
        "password": password,
      },
    );

    if (request.status) {
      response.status = true;
      response.message = request.message;
      _storage.clear();
    } else {
      response.message = request.message;
    }

    return response;
  }
}
