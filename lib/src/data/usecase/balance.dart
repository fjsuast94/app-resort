import 'package:appresort/src/data/model/actions.dart';
import 'package:appresort/src/data/model/balance_saldo.dart';
import 'package:appresort/src/data/model/balance_total.dart';
import 'package:appresort/src/data/model/charges_model.dart';
import 'package:appresort/src/data/model/last_transactions.dart';
import 'package:appresort/src/data/model/list_page.dart';
import 'package:appresort/src/data/model/user.dart';
import 'package:appresort/src/data/repository/local/http/http_dio.dart';
import 'package:appresort/src/data/repository/local/storage/storage_repository.dart';
import 'package:appresort/src/presentation/core/utils/my_icons.dart';
import 'package:flutter/material.dart';

class BalanceUseCase {
  BalanceUseCase({
    required HttpDio http,
    required StorageRepository storage,
  })  : _http = http,
        _storage = storage;

  final HttpDio _http;
  final StorageRepository _storage;

  UserModel getUser() {
    return _storage.user;
  }

  Future<BalanceTotalModel> balances() async {
    final response = await _http.post(
      route: '/app/balanceTotal',
      data: {
        'id_propietario': _storage.user.idpropietario,
      },
    );

    return response.status
        ? BalanceTotalModel.fromJson(response.data)
        : BalanceTotalModel.fromJson({});
  }

  Future<ListPage<BalanceSaldoModel>> balanceReport() async {
    final response = await _http.post(
      route: '/app/balanceReport',
      data: {
        "id_propietario": _storage.user.idpropietario,
      },
    );

    final List<BalanceSaldoModel> list = <BalanceSaldoModel>[];
    String message = "";
    response.status
        ? list.add(BalanceSaldoModel.fromJson(response.data))
        : message = response.message!;

    return ListPage<BalanceSaldoModel>(
      itemList: list,
      totalCount: list.length,
      message: message,
    );
  }

  Future<ListPage<LastTransations>> lastTransactions() async {
    final response = await _http.post(
      route: '/app/lastTransactions',
      data: {
        "id_propietario": _storage.user.idpropietario,
      },
    );

    final List<LastTransations> list = <LastTransations>[];
    String message = "";
    response.status
        ? response.data.forEach((e) => list.add(LastTransations.fromJson(e)))
        : message = response.message!;

    return ListPage<LastTransations>(
      itemList: list,
      totalCount: list.length,
      message: message,
    );
  }

  Future<ListPage<ChargesModel>> getCharges({int page = 1}) async {
    final response = await _http.post(
      route: '/app/balanceCharges',
      data: {
        "id_propietario": _storage.user.idpropietario,
        "page": page,
      },
    );

    final List<ChargesModel> list = <ChargesModel>[];
    String message = "";
    response.status
        ? response.data.forEach((e) => list.add(ChargesModel.fromJson(e)))
        : message = response.message!;

    return ListPage<ChargesModel>(
      itemList: list,
      totalCount: list.length,
      message: message,
    );
  }

  Future<String> bankStatement(
      {int type = 1, int mes = 1, int anio = 2020, int lastDay = 10}) async {
    final response = await _http.post(
      route: '/app/descargarEstadoDeCuenta',
      data: {
        'id_propietario': _storage.user.idpropietario,
        'tipo': type,
        'mes': mes,
        'anio': anio,
        'diasEdo': lastDay
      },
    );

    return response.status ? response.data['file'] ?? '' : '';
  }

  Future<List<ActionsModel>> getActions() async {
    final actions = [
      ActionsModel(
        title: 'Reglamentos',
        icon: MyIcons.name(
          name: 'bookmark',
          size: 30.0,
        ),
        page: '/regulation',
      ),
      ActionsModel(
        backGroundColor: const Color(0xFFC7DFFC),
        title: 'Publicaciones',
        icon: MyIcons.name(
          name: 'post_add_outlined',
          size: 30.0,
        ),
        page: '/post',
      ),
      ActionsModel(
        backGroundColor: const Color(0xFFC7DFFC),
        title: 'Saldos',
        icon: MyIcons.name(
          name: 'balance_scale',
          size: 35.0,
        ),
        page: '/balance',
      ),
      ActionsModel(
        backGroundColor: const Color(0xFFC7DFFC),
        title: 'Asambleas',
        icon: MyIcons.name(
          name: 'gavel',
          size: 35.0,
        ),
        page: '/assemblies',
      ),
      ActionsModel(
        backGroundColor: const Color(0xFFC7DFFC),
        title: 'Servicios',
        icon: MyIcons.name(
          name: 'menu_book',
          size: 35.0,
        ),
        page: '/services',
      ),
      ActionsModel(
        backGroundColor: const Color(0xFFC7DFFC),
        title: 'Tickets',
        icon: MyIcons.name(
          name: 'support_agent',
          size: 35.0,
        ),
        page: '/ticket',
      ),
    ];
    return actions;
  }
}
