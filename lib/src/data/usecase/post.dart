import 'package:appresort/src/data/model/list_page.dart';
import 'package:appresort/src/data/model/post.dart';
import 'package:appresort/src/data/repository/local/http/http_dio.dart';
import 'package:appresort/src/data/repository/local/storage/storage_repository.dart';

class PostUseCase {
  PostUseCase({
    required HttpDio http,
    required StorageRepository storage,
  })  : _http = http,
        _storage = storage;

  final HttpDio _http;
  final StorageRepository _storage;

  Future<ListPage<PostModel>> posts(int page) async {
    final response = await _http.post(
      route: '/app/publicaciones',
      data: {
        "idusuario": _storage.user.id,
        "sistema": _storage.user.sistema,
        "page": page,
      },
    );
    final List<PostModel> list = <PostModel>[];
    String message = "";
    response.status
        ? response.data.forEach((e) => list.add(PostModel.fromJson(e)))
        : message = response.message!;

    return ListPage<PostModel>(
      itemList: list,
      totalCount: list.length,
      message: message,
    );
  }
}
