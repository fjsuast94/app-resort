import 'package:appresort/src/data/model/acceso.dart';
import 'package:appresort/src/data/model/list_page.dart';
import 'package:appresort/src/data/model/response.dart';
import 'package:appresort/src/data/model/select.dart';
import 'package:appresort/src/data/repository/local/http/http_dio.dart';
import 'package:appresort/src/data/repository/local/storage/storage_repository.dart';

class AccesoUseCase {
  AccesoUseCase({
    required HttpDio http,
    required StorageRepository storage,
  })  : _http = http,
        _storage = storage;
  final HttpDio _http;
  final StorageRepository _storage;

  Future<ListPage<AccesoModel>> getAccesos(int page) async {
    final response = await _http.post(
      route: '/app/acceso',
      data: {
        'id_user': _storage.user.id,
        'page': page,
      },
    );
    final List<AccesoModel> list = <AccesoModel>[];
    String message = "";
    response.status
        ? response.data.forEach((e) => list.add(AccesoModel.fromJson(e)))
        : message = response.message!;

    return ListPage<AccesoModel>(
      itemList: list,
      totalCount: list.length,
      message: message,
    );
  }

  Future<ResponseModel> create(Map<String, dynamic> data) async {
    data["id_user"] = _storage.user.id;
    return _http.post(route: "/app/accesoCreate", data: data);
  }

  Future<List<SelectModel>> unidades() async {
    List<SelectModel> unidades = <SelectModel>[];
    final response = await _http.post(
      route: '/app/unidades',
      data: {
        'id_propietario': _storage.user.idpropietario,
      },
    );
    if (response.status) {
      response.data.forEach((e) => unidades.add(SelectModel.fromJson(e)));
    }
    return unidades;
  }

  Future<ResponseModel> activarAcceso(bool activar, int idAcceso) async {
    return await _http.post(
      route: '/app/accesoActive',
      data: {
        "active": activar,
        "id_acceso": idAcceso,
      },
    );
  }
}
