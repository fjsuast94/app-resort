import 'package:appresort/src/data/model/conekta.dart';
import 'package:appresort/src/data/model/payment.dart';
import 'package:appresort/src/data/model/response.dart';
import 'package:appresort/src/data/repository/local/http/http_dio.dart';
import 'package:appresort/src/data/repository/local/storage/storage_repository.dart';
import 'package:appresort/src/data/repository/remote/conekta/conekta_flutter_token.dart';

class PaymentUseCase {
  PaymentUseCase({
    required HttpDio http,
    required StorageRepository storage,
    required ConektaFlutterTokenizer conekta,
  })  : _http = http,
        _storage = storage,
        _conekta = conekta;

  final HttpDio _http;
  final StorageRepository _storage;
  final ConektaFlutterTokenizer _conekta;

  Future<ResponseModel> oxxoPago({
    required String name,
    required String email,
    required String phone,
    required int idCharge,
    required int idUnidad,
    required int idConcepto,
    required double total,
  }) async {
    return _http.post(
      route: '/conekta/oxxo',
      data: {
        "name": name,
        "email": email,
        "phone": phone,
        "id_propietario": _storage.user.idpropietario,
        "id_charge": idCharge,
        "id_unidad": idUnidad,
        "id_concepto": idConcepto,
        "total": total,
      },
    );
  }

  Future<ResponseModel> creditCard({
    required String name,
    required String cardNumber,
    required String expirationMonth,
    required String expirationYear,
    required String cvv,
    required int idCharge,
    required int idUnidad,
    required int idConcepto,
    required double total,
    required String email,
    required String phone,
  }) async {
    final ResponseModel response = ResponseModel(status: false, message: '');
    final ConektaModel? token = await _conekta.tokenizePaymentMethod(
      PaymentMethod(
        name: name,
        number: cardNumber,
        expirationMonth: expirationMonth,
        expirationYear: expirationYear,
        cvv: cvv,
      ),
    );

    if (token!.object == 'error') {
      response.message = " ${token.messageToPurchaser}";
      return response;
    }

    return _http.post(
      route: '/conekta/creditCard',
      data: {
        "name": name,
        "number": cardNumber,
        "month": expirationMonth,
        "year": expirationYear,
        "cvv": cvv,
        "id_propietario": _storage.user.idpropietario,
        "id_charge": idCharge,
        "id_unidad": idUnidad,
        "id_concepto": idConcepto,
        "total": total,
        "email": email,
        "phone": phone,
        "token": token.id,
      },
    );
  }
}
