import 'package:appresort/src/data/model/list_page.dart';
import 'package:appresort/src/data/model/notification.dart';
import 'package:appresort/src/data/model/response.dart';
import 'package:appresort/src/data/model/user.dart';
import 'package:appresort/src/data/repository/local/http/http_dio.dart';
import 'package:appresort/src/data/repository/local/storage/storage_repository.dart';
import 'package:appresort/src/presentation/core/utils/helper.dart';

class NotificationUseCase {
  NotificationUseCase({
    required HttpDio http,
    required StorageRepository storage,
  })  : _http = http,
        _storage = storage;

  final HttpDio _http;
  final StorageRepository _storage;

  Future<ListPage<NotificationModel>> show({int page = 1}) async {
    final response = await _http.post(
      route: '/notification/show',
      data: {
        'id_user': _storage.user.id,
        'sistema': _storage.user.sistema,
        'page': page,
      },
    );

    final List<NotificationModel> list = <NotificationModel>[];
    String message = "";
    response.status
        ? response.data.forEach((e) => list.add(NotificationModel.fromJson(e)))
        : message = response.message!;

    return ListPage<NotificationModel>(
      itemList: list,
      totalCount: list.length,
      message: message,
    );
  }

  Future<int> total() async {
    int total = 0;
    final response = await _http.post(
      route: '/notification/total',
      data: {
        'id_user': _storage.user.id,
      },
    );
    total = response.status ? Helper.intParse(response.data['total']) : 0;
    return total;
  }

  Future<bool> delete({int id = 0}) async {
    final response = await _http.post(
      route: '/notification/delete',
      data: {
        'id_notification': id,
      },
    );
    return response.status;
  }

  Future<void> read({int idnotification = 0}) async {
    _http.post(
      route: '/notification/read',
      data: {
        'id_notification': idnotification,
      },
    );
  }

  Future<ResponseModel> playerId({String device = '', String playerId = ''}) async {
    return _http.post(
      route: '/notification/playerId',
      data: {
        "player_id": playerId,
        "device": device,
        "id_user": _storage.user.id,
      },
    );
  }

  Future<ResponseModel> store({
    required int modo,
    required String title,
    required String subtitle,
    required String description,
    required int idNotifiactionType,
    int? iduser,
  }) async {
    final datos = {
      'modo': modo,
      'title': title,
      'subtitle': subtitle,
      'description': description,
      'id_user': iduser ?? _storage.user.id,
      'id_notifications_tipo': idNotifiactionType
    };
    return _http.post(
      route: '/notification/store',
      data: datos,
    );
  }

  Future<ResponseModel> storeApp({
    required int modo,
    required String title,
    required String subtitle,
    required String description,
    int? receiver,
  }) async {
    return _http.post(
      route: '/notification/send_notifi_to_app',
      data: {
        'modo': modo,
        'title': title,
        'subtitle': subtitle,
        'description': description,
        'receiver': receiver ?? _storage.user.id,
      },
    );
  }

  UserModel getUser() {
    return _storage.user;
  }
}
