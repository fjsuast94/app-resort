import 'package:appresort/src/data/model/assemblie.dart';
import 'package:appresort/src/data/model/assemblie_assistence.dart';
import 'package:appresort/src/data/model/assemblie_encuesta.dart';
import 'package:appresort/src/data/model/list_page.dart';
import 'package:appresort/src/data/repository/local/http/http_dio.dart';
import 'package:appresort/src/data/repository/local/storage/storage_repository.dart';

class AssemblieUseCase {
  AssemblieUseCase({
    required HttpDio http,
    required StorageRepository storage,
  })  : _http = http,
        _storage = storage;

  final HttpDio _http;
  final StorageRepository _storage;

  Future<ListPage<AssemblieModel>> assemblies(int page) async {
    final response = await _http.post(
      route: '/asambleas/listarAsambleas',
      data: {
        "idDesarrollo": _storage.user.iddesarrollo,
        "page": page,
      },
    );
    final List<AssemblieModel> list = <AssemblieModel>[];
    String message = "";
    response.status
        ? response.data.forEach((e) => list.add(AssemblieModel.fromJson(e)))
        : message = response.message!;

    return ListPage<AssemblieModel>(
      itemList: list,
      totalCount: list.length,
      message: message,
    );
  }

  Future<ListPage<AssemblieAssistenceModel>> listarAsistentes(int page, int idAsamblea) async {
    final response = await _http.post(
      route: '/asambleas/listarAsistentes',
      data: {
        "idAsamblea": idAsamblea,
        "page": page,
      },
    );
    final List<AssemblieAssistenceModel> list = <AssemblieAssistenceModel>[];
    String message = "";
    response.status
        ? response.data.forEach((e) => list.add(AssemblieAssistenceModel.fromJson(e)))
        : message = response.message!;

    return ListPage<AssemblieAssistenceModel>(
      itemList: list,
      totalCount: list.length,
      message: message,
    );
  }

  Future<String> registerAssistence(int idasamblea) async {
    final response = await _http.post(
      route: '/asambleas/Asistencia',
      data: {
        "idAsamblea": idasamblea,
        "idPropietario": _storage.user.idpropietario,
      },
    );

    return response.status ? response.data[0] : "Asstencia registrada";
  }

  Future<ListPage<AssemblieEncuestaModel>> listarEncuesta(int page, int idAsamblea) async {
    final response = await _http.post(
      route: '/asambleas/listarEncuestas',
      data: {
        "idAsamblea": idAsamblea,
        "page": page,
      },
    );
    final List<AssemblieEncuestaModel> list = <AssemblieEncuestaModel>[];
    String message = "";
    response.status
        ? response.data.forEach((e) => list.add(AssemblieEncuestaModel.fromJson(e)))
        : message = response.message!;

    return ListPage<AssemblieEncuestaModel>(
      itemList: list,
      totalCount: list.length,
      message: message,
    );
  }

  Future<String> votarEncuesta(int idEncuesta, int voto) async {
    final response = await _http.post(
      route: '/asambleas/votarEncuesta',
      data: {
        "idEncuesta": idEncuesta,
        "idPropietario": _storage.user.idpropietario,
        "Voto": voto,
      },
    );
    return response.status ? response.data[0] : response.message!;
  }
}
