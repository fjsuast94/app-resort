import 'package:appresort/src/data/model/meeting.dart';
import 'package:appresort/src/data/model/response.dart';
import 'package:appresort/src/data/model/select.dart';
import 'package:appresort/src/data/repository/local/http/http_dio.dart';
import 'package:appresort/src/data/repository/local/storage/storage_repository.dart';

class ReservationUseCase {
  ReservationUseCase({
    required HttpDio http,
    required StorageRepository storage,
  })  : _http = http,
        _storage = storage;

  final HttpDio _http;
  final StorageRepository _storage;

  Future<List<MeetingModel>> reservaciones() async {
    List<MeetingModel> metting = <MeetingModel>[];

    final response = await _http.post(
      route: '/areas/reservaciones',
      data: {
        'idpropietario': _storage.user.idpropietario,
      },
    );

    if (response.status) {
      final list = response.data.map((e) {
        final date = e['date'].split('-');
        final DateTime startTime = DateTime(
            int.parse(date[0]), int.parse(date[1]), int.parse(date[2]), e['from'] as int, 0, 0);

        final diff = e['to'] - e['from'];
        final DateTime endTime = startTime.add(Duration(hours: diff as int));
        e['from'] = startTime;
        e['to'] = endTime;
        return MeetingModel.fromJson(e);
      });

      list.forEach((e) => metting.add(e));
    }

    return metting;
  }

  Future<List<SelectModel>> listAreas({String buscar = ''}) async {
    List<SelectModel> areas = <SelectModel>[];
    final response = await _http.post(
      route: '/areas',
      data: {
        'desarrollo ': _storage.user.iddesarrollo,
        'buscar': buscar,
      },
    );
    if (response.status) {
      response.data.forEach((e) => areas.add(SelectModel.fromJson(e)));
    }
    return areas;
  }

  Future<List<SelectModel>> listUnidades() async {
    List<SelectModel> unidades = <SelectModel>[];
    final response = await _http.post(
      route: '/areas/unidades',
      data: {
        'idpropietario': _storage.user.idpropietario,
      },
    );
    if (response.status) {
      response.data.forEach((e) => unidades.add(SelectModel.fromJson(e)));
    }
    return unidades;
  }

  Future<List<SelectModel>> listHorarios({
    int idarea = 0,
    String buscar = '',
    int finSemana = 0,
  }) async {
    List<SelectModel> horarios = <SelectModel>[];
    final response = await _http.post(
      route: '/areas/horarios',
      data: {
        'idarea': idarea,
        'buscar': buscar,
        'fin_semana': finSemana,
      },
    );
    if (response.status) {
      response.data.forEach((e) => horarios.add(SelectModel.fromJson(e)));
    }
    return horarios;
  }

  Future<int> validarCupo(String fecha, int idhorario) async {
    final response = await _http.post(
      route: '/areas/validarCupo',
      data: {
        'fehca': fecha,
        'idhorario': idhorario,
      },
    );

    return response.status ? response.data['cantidadTotal'] as int : 0;
  }

  Future<ResponseModel> addReservation({
    required int idarea,
    required int idhorario,
    required int unidad,
    required int cantidad,
    required String descripcion,
    required String fecha,
  }) async {
    return await _http.post(
      route: '/areas/agregarReservacion',
      data: {
        'iddesarrollo': _storage.user.iddesarrollo,
        'idarea': idarea,
        'idhorario': idhorario,
        'unidad': unidad,
        'cantidad': cantidad,
        'descripcion': descripcion,
        'fecha': fecha,
      },
    );
  }
}
