class CardColorModel {
  CardColorModel({
    required this.isSelected,
    required this.cardColor,
  });

  bool isSelected;
  final int cardColor;
}
