class RegulationModel {
  RegulationModel({
    required this.id,
    required this.nombre,
    required this.ruta,
  });

  String id;
  String nombre;
  String ruta;

  factory RegulationModel.fromJson(Map<String, dynamic> json) => RegulationModel(
        id: json["id"] ?? '',
        nombre: json["nombre"] ?? '',
        ruta: json["ruta"] ?? '',
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "nombre": nombre,
        "ruta": ruta,
      };
}
