class TicketsModel {
  TicketsModel({
    required this.id,
    required this.folio,
    required this.description,
    required this.message,
    required this.status,
  });

  String id;
  String folio;
  String message = "";
  String description;
  String status;

  factory TicketsModel.fromJson(Map<String, dynamic> json) => TicketsModel(
        id: json["id"] ?? '',
        folio: json["folio"] ?? '',
        description: json["description"] ?? '',
        message: json["message"] ?? '',
        status: json["status"] ?? '',
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "folio": folio,
        "description": description,
        "message": message,
        "status": status,
      };
}

enum status {
  pendiente,
}

class CatalogoTicketModel {
  CatalogoTicketModel({
    required this.id,
    required this.texto,
  });

  String id;
  String texto;

  factory CatalogoTicketModel.fromJson(Map<String, dynamic> json) => CatalogoTicketModel(
        id: json["id"] ?? 0,
        texto: json["nombre"] ?? '',
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "texto": texto,
      };
}
