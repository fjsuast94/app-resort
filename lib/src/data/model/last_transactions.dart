class LastTransations {
  LastTransations({
    required this.id,
    required this.cargo,
    required this.abono,
    required this.unidad,
    required this.nombre,
    required this.fecha,
    required this.concepto,
    required this.esCargo,
    required this.tipoConcepto,
    required this.metodo,
  });

  String id;
  String cargo;
  String abono;
  String unidad;
  String nombre;
  String fecha;
  String concepto;
  bool esCargo;
  String tipoConcepto;
  String metodo;

  factory LastTransations.fromJson(Map<String, dynamic> json) => LastTransations(
        id: json["id"] ?? '',
        cargo: json["cargo"] ?? '',
        abono: json["abono"] ?? '',
        unidad: json["unidad"] ?? '',
        nombre: json["nombre"] ?? '',
        fecha: json["fecha"] ?? '',
        concepto: json["concepto"] ?? '',
        esCargo: json["esCargo"] ?? false,
        tipoConcepto: json["tipoConcepto"] ?? '',
        metodo: json["metodo"] ?? '',
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "cargo": cargo,
        "abono": abono,
        "unidad": unidad,
        "nombre": nombre,
        "fecha": fecha,
        "concepto": concepto,
        "esCargo": esCargo,
        "tipoConcepto": tipoConcepto,
        "metodo": metodo,
      };
}
