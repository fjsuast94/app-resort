class AccesoModel {
  AccesoModel({
    required this.id,
    required this.tipoVisitante,
    required this.nombreVisitante,
    required this.numPersona,
    required this.activarAcceso,
    required this.fechaAcceso,
    required this.formaTraslado,
    required this.tipoVehiculo,
    required this.marca,
    required this.placa,
    required this.color,
    required this.direccion,
    required this.numCasa,
    required this.empresa,
    required this.comentarios,
    required this.tokenQr,
    required this.propiedad,
  });

  int id;
  String tipoVisitante;
  String nombreVisitante;
  int numPersona;
  bool activarAcceso;
  String fechaAcceso;
  String formaTraslado;
  String tipoVehiculo;
  String marca;
  String placa;
  int color;
  String direccion;
  String numCasa;
  String empresa;
  String comentarios;
  String tokenQr;
  String propiedad;

  factory AccesoModel.fromJson(Map<String, dynamic> json) => AccesoModel(
        id: json["id"],
        tipoVisitante: json["tipo_visitante"] ?? "",
        nombreVisitante: json["nombre_visitante"] ?? "",
        numPersona: json["num_persona"] ?? 0,
        activarAcceso: json["activar_acceso"] ?? false,
        fechaAcceso: json["fecha_acceso"] ?? "",
        formaTraslado: json["forma_traslado"] ?? "",
        tipoVehiculo: json["tipo_vehiculo"] ?? "",
        marca: json["marca"] ?? "",
        placa: json["placa"] ?? "",
        color: json["color"] ?? 0,
        direccion: json["direccion"] ?? "",
        numCasa: json["num_casa"] ?? "",
        empresa: json["empresa"] ?? "",
        comentarios: json["comentarios"] ?? "",
        tokenQr: json["tokenQr"] ?? "",
        propiedad: json["propiedad"] ?? "",
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "tipo_visitante": tipoVisitante,
        "nombre_visitante": nombreVisitante,
        "num_persona": numPersona,
        "activar_acceso": activarAcceso,
        "fecha_acceso": fechaAcceso,
        "forma_traslado": formaTraslado,
        "tipo_vehiculo": tipoVehiculo,
        "marca": marca,
        "placa": placa,
        "color": color,
        "direccion": direccion,
        "num_casa": numCasa,
        "empresa": empresa,
        "comentarios": comentarios,
        "tokenQr": tokenQr,
        "propiedad": propiedad,
      };
}
