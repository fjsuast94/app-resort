class AssemblieAssistenceModel {
  AssemblieAssistenceModel({
    required this.id,
    required this.condomino,
  });

  int id;
  String condomino;

  factory AssemblieAssistenceModel.fromJson(Map<String, dynamic> json) => AssemblieAssistenceModel(
        id: json["id"] ?? 0,
        condomino: json["condomino"] ?? '',
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "condomino": condomino,
      };
}
