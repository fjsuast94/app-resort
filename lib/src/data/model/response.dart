class ResponseModel {
  ResponseModel({
    required this.status,
    this.message,
    this.data,
  });

  bool status;
  String? message;
  dynamic data;

  factory ResponseModel.fromJson(Map<String, dynamic> json) => ResponseModel(
        status: json["status"] ?? false,
        message: json["message"] ?? '',
        data: json["data"],
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "message": message,
        "data": data,
      };
}
