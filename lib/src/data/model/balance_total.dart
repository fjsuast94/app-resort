class BalanceTotalModel {
  BalanceTotalModel({
    required this.total,
    required this.fondo,
  });

  double total;
  double fondo;

  factory BalanceTotalModel.fromJson(Map<String, dynamic> json) => BalanceTotalModel(
        total: double.parse(json["total"] ?? "0.00"),
        fondo: double.parse(json["fondo"] ?? "0.00"),
      );

  Map<String, dynamic> toJson() => {
        "total": total,
        "fondo": fondo,
      };
}
