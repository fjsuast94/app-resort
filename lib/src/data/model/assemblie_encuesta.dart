class AssemblieEncuestaModel {
  AssemblieEncuestaModel({
    required this.id,
    required this.pregunta,
    required this.fechaInicio,
    required this.fechaCierre,
    required this.votoNo,
    required this.votoSi,
    required this.votoNeutral,
  });

  int id;
  String pregunta;
  String fechaInicio;
  String fechaCierre;
  String votoNo;
  String votoSi;
  String votoNeutral;

  factory AssemblieEncuestaModel.fromJson(Map<String, dynamic> json) => AssemblieEncuestaModel(
        id: json["id"] ?? 0,
        pregunta: json["pregunta"] ?? '',
        fechaInicio: json["fechaInicio"] ?? '',
        fechaCierre: json["fechaCierre"] ?? '',
        votoNo: json["votos_no"] ?? '',
        votoSi: json["votos_si"] ?? '',
        votoNeutral: json["votos_neutal"] ?? '',
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "pregunta": pregunta,
        "fechaInicio": fechaInicio,
        "fechaCierre": fechaCierre,
        "votoNo": votoNo,
        "votoSi": votoSi,
        "votoNeutral": votoNeutral,
      };
}
