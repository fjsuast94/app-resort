class TimeLineModel {
  TimeLineModel({
    required this.id,
    required this.message,
    required this.date,
    required this.process,
  });

  String id;
  String message = "";
  String date;
  String process;

  factory TimeLineModel.fromJson(Map<String, dynamic> json) => TimeLineModel(
        id: json["id"] ?? '',
        message: json["message"] ?? '',
        date: json["date"] ?? '',
        process: json["process"] ?? '',
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "message": message,
        "date": date,
        "process": process,
      };
}
