class AssemblieModel {
  AssemblieModel({
    required this.id,
    required this.titulo,
    required this.fecha,
    required this.hora,
    required this.estatus,
    required this.ruta,
  });

  int id;
  String titulo;
  String fecha;
  String hora;
  String ruta;
  String estatus;

  factory AssemblieModel.fromJson(Map<String, dynamic> json) => AssemblieModel(
        id: json["id"] ?? 0,
        titulo: json["nombre"] ?? '',
        fecha: json["fecha_inicioFormato"] ?? '',
        hora: json["hora_inicioFormato"] ?? '',
        ruta: json["ruta"] ?? '',
        estatus: json["status"] ?? '',
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "titulo": titulo,
        "fecha": fecha,
        "hora": hora,
        "ruta": ruta,
        "estatus": estatus,
      };
}
