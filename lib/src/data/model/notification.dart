class NotificationModel {
  int id;
  String title;
  String subtitle;
  String description;
  String icon;
  String page;
  String time;
  String sender;
  String senderAvatar;
  bool status;

  NotificationModel({
    required this.id,
    required this.title,
    required this.subtitle,
    required this.description,
    required this.icon,
    required this.page,
    required this.time,
    required this.sender,
    required this.senderAvatar,
    required this.status,
  });

  factory NotificationModel.fromJson(Map<String, dynamic> json) => NotificationModel(
        id: int.parse(json["id"] ?? "0"),
        title: json["title"] ?? '',
        subtitle: json["subtitle"] ?? '',
        description: json["description"] ?? '',
        icon: json["icon"] ?? '',
        page: json["page"] ?? '',
        time: json["date"] ?? '',
        sender: json["sender"] ?? '',
        senderAvatar: json["sender_avatar"] ?? '',
        status: int.parse(json["status"]) == 1 ? true : false,
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "title": title,
        "subtitle": subtitle,
        "description": description,
        "icon": icon,
        "page": page,
        "time": time,
        "sender": sender,
        "senderAvatar": senderAvatar,
        "status": status
      };
}
