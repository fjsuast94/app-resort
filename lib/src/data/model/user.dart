class UserModel {
  String id;
  String name;
  String user;
  String fullname;
  String email;
  String photo;
  String token;
  String rol;
  String sistema;
  String idpropietario;
  String iddesarrollo;

  UserModel({
    required this.id,
    required this.name,
    required this.fullname,
    required this.user,
    required this.email,
    required this.photo,
    required this.token,
    required this.rol,
    required this.sistema,
    required this.idpropietario,
    required this.iddesarrollo,
  });

  factory UserModel.fromJson(Map<String, dynamic> json) => UserModel(
        id: json["id"] ?? '',
        name: json["name"] ?? '',
        user: json["user"] ?? '',
        fullname: json["fullname"] ?? '',
        email: json["email"] ?? '',
        photo: json["photo"] ?? '',
        token: json["token"] ?? '',
        rol: json["rol"] ?? '',
        sistema: json["sistema"] ?? '',
        idpropietario: json["idpropietario"] ?? '',
        iddesarrollo: json["iddesarrollo"] ?? '',
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "fullname": fullname,
        "user": user,
        "email": email,
        "photo": photo,
        "token": token,
        "rol": rol,
        "sistema": sistema,
        "idpropietario": idpropietario,
        "iddesarrollo": iddesarrollo,
      };
}
