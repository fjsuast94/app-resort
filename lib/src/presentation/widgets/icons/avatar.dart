import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class AvatarIcon extends StatelessWidget {
  const AvatarIcon({
    Key? key,
    this.profileImage,
    required this.child,
    required this.height,
    required this.width,
    this.marginColor = const Color(0xfff4f6f8),
    this.marginWidth = 2,
  }) : super(key: key);

  final double height;
  final double width;
  final double marginWidth;
  final Color marginColor;
  final String? profileImage;
  final Widget child;

  @override
  Widget build(BuildContext context) {
    if (profileImage == null) {
      return Container(
        height: height,
        width: width,
        decoration: BoxDecoration(
          color: const Color(0xFFAABBCC),
          border: Border.all(
            color: marginColor,
            width: marginWidth,
          ),
        ),
        child: child,
      );
    }
    return CachedNetworkImage(
      imageUrl: profileImage!,
      imageBuilder: (context, image) => Container(
        height: height,
        width: width,
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          image: DecorationImage(
            image: image,
          ),
          border: Border.all(
            color: marginColor,
            width: marginWidth,
          ),
        ),
        child: child,
      ),
      placeholder: (context, url) {
        return Container(
          height: height,
          width: width,
          decoration: BoxDecoration(
            color: Colors.white,
            border: Border.all(
              color: marginColor,
              width: marginWidth,
            ),
          ),
          child: child,
        );
      },
      errorWidget: (context, url, error) => Container(
        height: height,
        width: width,
        decoration: BoxDecoration(
          color: const Color(0xFFAABBCC),
          border: Border.all(
            color: marginColor,
            width: marginWidth,
          ),
        ),
        child: child,
      ),
      fadeOutDuration: const Duration(milliseconds: 400),
      fadeInDuration: const Duration(milliseconds: 800),
    );
  }
}
