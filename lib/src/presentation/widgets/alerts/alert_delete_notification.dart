import 'package:appresort/src/presentation/core/themes/responsive.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class AlertDeleteNotification extends StatelessWidget {
  const AlertDeleteNotification({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final responsive = Responsive.of(context);
    return CupertinoAlertDialog(
      title: Text(
        "¿Deseas eliminar la notificatión?",
        textAlign: TextAlign.center,
        style: TextStyle(
          fontSize: responsive.inchPercent(1.5),
          color: Colors.black.withOpacity(0.9),
          fontWeight: FontWeight.bold,
        ),
      ),
      content: Text(
        "Se eliminara esta notificatión de tu lista",
        textAlign: TextAlign.center,
        style: TextStyle(
          fontSize: responsive.inchPercent(1.5),
          color: Colors.black.withOpacity(0.9),
        ),
      ),
      actions: <Widget>[
        CupertinoDialogAction(
          child: const Text(
            'Cancelar',
          ),
          onPressed: () => Navigator.of(context).pop(false),
        ),
        CupertinoDialogAction(
          isDefaultAction: true,
          child: const Text(
            'Eliminar',
          ),
          onPressed: () async {
            return Navigator.of(context).pop(true);
          },
        ),
      ],
    );
  }
}
