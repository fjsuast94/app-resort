import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class AlertConfirm extends StatelessWidget {
  const AlertConfirm({
    Key? key,
    required this.title,
    this.content = "",
    this.labelYes = "Aceptar",
    this.labelNo = "Cancelar",
  }) : super(key: key);

  final String title;
  final String content;
  final String labelYes;
  final String labelNo;

  @override
  Widget build(BuildContext context) {
    return CupertinoAlertDialog(
      title: Text(
        title,
        textAlign: TextAlign.center,
        style: TextStyle(
          fontSize: 14,
          color: Colors.black.withOpacity(0.6),
          fontWeight: FontWeight.bold,
        ),
      ),
      content: content.isNotEmpty
          ? Text(
              content,
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: 12,
                color: Colors.black.withOpacity(0.9),
              ),
            )
          : const SizedBox.shrink(),
      actions: <Widget>[
        CupertinoDialogAction(
          child: Text(
            labelNo,
            style: const TextStyle(
              fontSize: 15,
            ),
          ),
          onPressed: () => Navigator.of(context).pop(false),
        ),
        CupertinoDialogAction(
          isDefaultAction: true,
          child: Text(
            labelYes,
            style: const TextStyle(
              fontSize: 15,
            ),
          ),
          onPressed: () => Navigator.of(context).pop(true),
        ),
      ],
    );
  }
}
