import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class AlertDelete extends StatelessWidget {
  const AlertDelete({
    Key? key,
    this.title = "¿Deseas eliminar la notificatión?",
    this.content = "Se eliminara este item de tu lista",
    this.labelAction = "Eliminar",
  }) : super(key: key);

  final String title;
  final String content;
  final String labelAction;

  @override
  Widget build(BuildContext context) {
    return CupertinoAlertDialog(
      title: Text(
        title,
        textAlign: TextAlign.center,
        style: TextStyle(
          fontSize: 16,
          color: Colors.black.withOpacity(0.9),
          fontWeight: FontWeight.bold,
        ),
      ),
      content: Text(
        content,
        textAlign: TextAlign.center,
        style: TextStyle(
          fontSize: 15,
          color: Colors.black.withOpacity(0.9),
        ),
      ),
      actions: <Widget>[
        CupertinoDialogAction(
          child: const Text(
            'Cancelar',
          ),
          onPressed: () => Navigator.of(context).pop(false),
        ),
        CupertinoDialogAction(
          isDefaultAction: true,
          child: Text(
            labelAction,
          ),
          onPressed: () => Navigator.of(context).pop(true),
        ),
      ],
    );
  }
}
