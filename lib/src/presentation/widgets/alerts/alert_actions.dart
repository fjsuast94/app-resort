import 'package:appresort/src/presentation/core/themes/app_colors.dart';
import 'package:flutter/material.dart';

class AlertAction extends StatelessWidget {
  final String title;
  final String buttonLabelYes;
  final String buttonLabelNo;
  final Color buttonColorYes, buttonColorNo, buttonColorTextYes, buttonColorTextNo;
  final VoidCallback? onPressYes;
  final bool activeButton;

  const AlertAction({
    Key? key,
    required this.title,
    this.onPressYes,
    this.buttonLabelYes = "Si",
    this.buttonLabelNo = "No",
    this.buttonColorYes = AppColors.kPrimaryColor,
    this.buttonColorNo = AppColors.kCancel,
    this.buttonColorTextYes = Colors.white,
    this.buttonColorTextNo = Colors.white,
    this.activeButton = true,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.all(
          Radius.circular(20.0),
        ),
      ),
      title: Center(
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Container(
                margin: const EdgeInsets.only(top: 12),
                child: Text(
                  title,
                  textAlign: TextAlign.center,
                  style: const TextStyle(
                    color: Colors.black54,
                    fontSize: 14,
                    fontWeight: FontWeight.w400,
                  ),
                ),
              ),
              Row(
                children: <Widget>[
                  onPressYes != null
                      ? Expanded(
                          child: GestureDetector(
                            onTap: onPressYes,
                            child: Container(
                              padding: const EdgeInsets.all(10),
                              margin: const EdgeInsets.only(left: 0, right: 5, top: 20, bottom: 0),
                              decoration: BoxDecoration(
                                color: buttonColorYes,
                                borderRadius: const BorderRadius.all(
                                  Radius.circular(100),
                                ),
                              ),
                              child: Text(
                                buttonLabelYes,
                                style: TextStyle(
                                  color: buttonColorTextYes,
                                  fontSize: 17,
                                ),
                                textAlign: TextAlign.center,
                              ),
                            ),
                          ),
                        )
                      : const SizedBox.shrink(),
                  Expanded(
                    child: GestureDetector(
                      onTap: () => Navigator.of(context).pop(),
                      child: Container(
                        padding: const EdgeInsets.all(10),
                        margin: const EdgeInsets.only(left: 5, right: 0, top: 20, bottom: 0),
                        decoration: BoxDecoration(
                          color: buttonColorNo,
                          border: Border.all(color: Colors.black54, width: 0.5),
                          borderRadius: const BorderRadius.all(
                            Radius.circular(100),
                          ),
                        ),
                        child: Text(
                          buttonLabelNo,
                          style: TextStyle(
                            color: buttonColorTextNo,
                            fontSize: 17,
                          ),
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ),
                  )
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
