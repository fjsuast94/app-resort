import 'package:appresort/src/presentation/core/themes/app_colors.dart';
import 'package:flutter/material.dart';

class AlertCustom extends StatefulWidget {
  final AlertDialogType type;
  final String text;
  final VoidCallback? action;

  const AlertCustom({
    Key? key,
    required this.text,
    this.action,
    this.type = AlertDialogType.success,
  }) : super(key: key);

  @override
  _AlertCustomState createState() => _AlertCustomState();
}

class _AlertCustomState extends State<AlertCustom> {
  String textButton = 'Aceptar';
  late Icon icono;
  late Color backgroundcolor;

  @override
  void initState() {
    super.initState();
    switch (widget.type) {
      case AlertDialogType.success:
        icono = const Icon(Icons.done, color: AppColors.kPrimaryColor, size: 50);
        backgroundcolor = AppColors.kPrimaryColor;
        break;
      case AlertDialogType.error:
        icono = const Icon(Icons.error, color: Colors.red, size: 50);
        backgroundcolor = const Color(0xFFF14141);
        break;
      case AlertDialogType.warning:
        icono = const Icon(Icons.warning, color: Colors.orange, size: 50);
        backgroundcolor = Colors.orange;
        break;
      default:
        icono = const Icon(Icons.done, color: AppColors.kPrimaryColor, size: 50);
        backgroundcolor = AppColors.kPrimaryColor;
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.all(
          Radius.circular(20.0),
        ),
      ),
      title: Center(
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Container(
                decoration: BoxDecoration(
                  border: Border.all(color: backgroundcolor, width: 1.5),
                  borderRadius: BorderRadius.circular(100),
                  color: Colors.white,
                ),
                child: icono,
              ),
              Container(
                margin: const EdgeInsets.only(top: 12),
                child: Text(
                  widget.text,
                  textAlign: TextAlign.center,
                  style: const TextStyle(
                    color: Colors.black54,
                    fontSize: 14,
                    fontWeight: FontWeight.w400,
                  ),
                ),
              ),
              Row(
                children: <Widget>[
                  Expanded(
                    child: GestureDetector(
                      onTap: widget.action ?? () => Navigator.of(context).pop(),
                      child: Container(
                        padding: const EdgeInsets.all(10),
                        margin: const EdgeInsets.only(
                          top: 20.0,
                        ),
                        decoration: BoxDecoration(
                          color: backgroundcolor,
                          borderRadius: const BorderRadius.all(
                            Radius.circular(100),
                          ),
                        ),
                        child: Text(
                          textButton,
                          style: const TextStyle(
                            color: Colors.white,
                            fontSize: 17,
                          ),
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}

enum AlertDialogType {
  success,
  error,
  warning,
  info,
}
