import 'package:appresort/src/presentation/core/themes/responsive.dart';
import 'package:flutter/material.dart';

class AlertImagePicker extends StatelessWidget {
  final VoidCallback onPressCamara, onPressGalery;

  const AlertImagePicker({
    Key? key,
    required this.onPressCamara,
    required this.onPressGalery,
  }) : super(key: key);

  final TextStyle subtitle = const TextStyle(fontSize: 12.0, color: Colors.grey);
  final TextStyle label = const TextStyle(fontSize: 14.0, color: Colors.grey);

  @override
  Widget build(BuildContext context) {
    final responseive = Responsive.of(context);
    return Center(
      child: SizedBox(
        height: 280,
        child: Dialog(
          child: Padding(
            padding: const EdgeInsets.all(16.0),
            child: Column(
              children: <Widget>[
                //Text("Thank You!",style: TextStyle(color: Colors.green),),
                Text(
                  "¿De donde quieres obtener la foto? ",
                  style: label,
                ),
                const Divider(),
                GestureDetector(
                  behavior: HitTestBehavior.translucent,
                  onTap: onPressGalery,
                  child: SizedBox(
                    width: responseive.width,
                    height: 50.0,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        const Icon(Icons.photo_library),
                        const SizedBox(
                          width: 20.0,
                        ),
                        Text("Galeria", style: label)
                      ],
                    ),
                  ),
                ),
                const Divider(),
                GestureDetector(
                  behavior: HitTestBehavior.translucent,
                  onTap: onPressCamara,
                  child: SizedBox(
                    width: responseive.width,
                    height: 50.0,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        const Icon(Icons.photo_camera),
                        const SizedBox(
                          width: 20.0,
                        ),
                        Text("Camara", style: label)
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
