import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class AlertAskAgainDialog extends StatelessWidget {
  final String title;
  final String subTitle;
  final String positiveButtonText;
  final String negativeButtonText;
  final VoidCallback? onPositiveButtonClicked;
  final String doNotAskAgainText;

  const AlertAskAgainDialog({
    Key? key,
    this.title = 'App Update Available',
    this.subTitle = 'Please update the app to continue',
    this.positiveButtonText = 'Update Now',
    this.negativeButtonText = 'App Update Available',
    this.onPositiveButtonClicked,
    this.doNotAskAgainText = 'Please update the app to continue',
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Platform.isIOS
        ? CupertinoAlertDialog(
            title: Text(
              title,
              textAlign: TextAlign.center,
            ),
            content: Container(
              margin: const EdgeInsets.only(top: 6.0),
              child: Text(
                subTitle,
                textAlign: TextAlign.center,
              ),
            ),
            actions: <Widget>[
              CupertinoDialogAction(
                child: Text(
                  positiveButtonText,
                ),
                onPressed: onPositiveButtonClicked,
              )
            ],
          )
        : AlertDialog(
            title: Text(
              title,
              textAlign: TextAlign.center,
              style: const TextStyle(fontSize: 20.0),
            ),
            content: FittedBox(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    subTitle,
                    textAlign: TextAlign.center,
                    style: const TextStyle(fontSize: 22.0),
                  ),
                ],
              ),
            ),
            actions: <Widget>[
              TextButton(
                child: Text(positiveButtonText),
                onPressed: onPositiveButtonClicked,
              ),
            ],
          );
  }
}
