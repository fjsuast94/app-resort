import 'package:appresort/src/presentation/core/themes/responsive.dart';
import 'package:flutter/material.dart';

class CardRefresh extends StatelessWidget {
  final Widget title;
  final Widget? content;
  final double margin;

  const CardRefresh({
    Key? key,
    required this.title,
    this.margin = 0.0,
    this.content,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final responsive = Responsive.of(context);
    return Container(
      margin: EdgeInsets.symmetric(
        vertical: responsive.inchPercent(0.5),
      ),
      child: Center(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            title,
            content ??
                Column(
                  children: const [
                    Text("Tire para refrescar"),
                    Icon(Icons.arrow_downward),
                  ],
                ),
          ],
        ),
      ),
    );
  }
}
