import 'package:flutter/material.dart';
import 'package:appresort/src/presentation/core/themes/app_colors.dart';

class CardIcon extends StatelessWidget {
  const CardIcon({
    Key? key,
    required this.icon,
    required this.text,
    this.onTap,
    this.colorText = AppColors.kPrimaryColor,
    this.colorIcon = AppColors.kPrimaryColor,
    this.background = Colors.white,
    this.fontWeight = FontWeight.normal,
  }) : super(key: key);

  final VoidCallback? onTap;
  final IconData icon;
  final String text;
  final Color colorText;
  final Color colorIcon;
  final Color background;
  final FontWeight fontWeight;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        width: 60,
        height: 60,
        decoration: BoxDecoration(
          border: Border.all(color: Colors.black12),
          borderRadius: BorderRadius.circular(10),
          color: background,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Icon(
              icon,
              color: colorIcon,
              size: 30.0,
            ),
            Text(
              text,
              style: TextStyle(
                fontSize: 12,
                color: colorText,
                fontWeight: fontWeight,
              ),
            )
          ],
        ),
      ),
    );
  }
}
