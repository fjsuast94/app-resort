import 'dart:io';

import 'package:appresort/src/presentation/shared/logger/logger_utils.dart';
import 'package:bloc/bloc.dart';
import 'package:path_provider/path_provider.dart';
import 'package:screenshot/screenshot.dart';
import 'package:share_extend/share_extend.dart';

part 'qr_shared_state.dart';

class QrSharedCubit extends Cubit<QrSharedState> {
  QrSharedCubit({
    required ScreenshotController screenshotController,
  })  : _screenshotController = screenshotController,
        super(QrSharedState());

  final ScreenshotController _screenshotController;
  ScreenshotController get screenshotController => _screenshotController;

  Future<void> shareTapped(String extraText) async {
    try {
      final file = await _screenshotController.capture(
        pixelRatio: 1.5,
      );

      if (file != null) {
        final directory = await getApplicationDocumentsDirectory();
        final imagePath = await File('${directory.path}/image.png').create();
        await imagePath.writeAsBytes(file);

        ShareExtend.share(
          imagePath.path,
          "image",
          extraText: extraText,
        );
      }
    } catch (e) {
      Logger.write(e.toString());
    }
  }
}
