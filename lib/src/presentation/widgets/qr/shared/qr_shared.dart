import 'package:appresort/src/presentation/core/themes/app_colors.dart';
import 'package:appresort/src/presentation/core/themes/responsive.dart';
import 'package:appresort/src/presentation/widgets/qr/shared/cubit/qr_shared_cubit.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:qr_flutter/qr_flutter.dart';
import 'package:screenshot/screenshot.dart';

class QrShared extends StatelessWidget {
  const QrShared({
    Key? key,
    required this.code,
    this.extraText = "",
  }) : super(key: key);

  final String code;
  final String extraText;

  @override
  Widget build(BuildContext context) {
    final responsive = Responsive.of(context);
    return BlocProvider(
      create: (context) => QrSharedCubit(
        screenshotController: ScreenshotController(),
      ),
      child: BlocBuilder<QrSharedCubit, QrSharedState>(
        builder: (context, state) {
          final cubit = context.read<QrSharedCubit>();
          return Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Screenshot(
                controller: cubit.screenshotController,
                child: QrImage(
                  backgroundColor: Colors.white,
                  data: code,
                  size: responsive.height * 0.25,
                ),
              ),
              SizedBox(
                width: responsive.width * 0.5,
                child: ElevatedButton(
                  onPressed: () => cubit.shareTapped(extraText),
                  style: ButtonStyle(
                    shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                      RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(5.0),
                        side: const BorderSide(
                          color: AppColors.kPrimaryColor,
                        ),
                      ),
                    ),
                  ),
/*                   color: Colors.white,
                  splashColor: Colors.transparent,
                  elevation: 0.0,
                  highlightColor: Colors.transparent,
                  hoverColor: Colors.transparent, */
                  child: SizedBox(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          'Compartir',
                          style: TextStyle(
                            color: Colors.black,
                            fontSize: responsive.inchPercent(1.5),
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        const SizedBox(width: 10),
                        const Icon(Icons.share),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          );
        },
      ),
    );
  }
}
