part of 'qr_scanner_cubit.dart';

class QrScannerState {
  QrScannerState({
    this.barcodeResult,
    this.flashOn = false,
  });
  final Barcode? barcodeResult;
  final bool flashOn;

  QrScannerState copyWith({
    Barcode? barcodeResult,
    bool? flashOn,
  }) =>
      QrScannerState(
        barcodeResult: barcodeResult ?? this.barcodeResult,
        flashOn: flashOn ?? this.flashOn,
      );
}
