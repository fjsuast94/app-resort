import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:qr_code_scanner/qr_code_scanner.dart';

part 'qr_scanner_state.dart';

class QrScannerCubit extends Cubit<QrScannerState> {
  QrScannerCubit() : super(QrScannerState());

  late QRViewController _qrViewController;
  final GlobalKey qrKey = GlobalKey(debugLabel: 'QR');

  @override
  Future<void> close() {
    _qrViewController.dispose();
    return super.close();
  }

  void onQRViewCreated(QRViewController controller) {
    _qrViewController = controller;
    _qrViewController.scannedDataStream.listen((scanData) {
      emit(state.copyWith(barcodeResult: scanData));
    });
  }

  Future<void> toggleFlash() async {
    await _qrViewController.toggleFlash();
    final flashOn = await _qrViewController.getFlashStatus();
    emit(state.copyWith(flashOn: flashOn));
  }

  Future<void> flipCamera() async {
    await _qrViewController.flipCamera();
    await _qrViewController.getCameraInfo();
  }
}
