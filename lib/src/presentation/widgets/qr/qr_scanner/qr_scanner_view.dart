import 'package:appresort/src/presentation/core/themes/app_colors.dart';
import 'package:appresort/src/presentation/core/themes/responsive.dart';
import 'package:appresort/src/presentation/widgets/qr/qr_scanner/cubit/qr_scanner_cubit.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/route_manager.dart';
import 'package:qr_code_scanner/qr_code_scanner.dart';

class QrScannerView extends StatelessWidget {
  static const String routeName = '/qrScanner';
  const QrScannerView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final responsive = Responsive.of(context);
    final scanArea =
        (responsive.width < 400 || responsive.height < 400) ? 300.0 : 300.0;
    return BlocProvider(
      create: (context) => QrScannerCubit(),
      child: BlocConsumer<QrScannerCubit, QrScannerState>(
        listener: (context, state) {
          if (state.barcodeResult != null) {
            Get.back(result: state.barcodeResult!.code);
          }
        },
        builder: (context, state) {
          final qrBloc = context.read<QrScannerCubit>();
          return AnnotatedRegion<SystemUiOverlayStyle>(
            value: SystemUiOverlayStyle.light,
            child: Scaffold(
              body: Stack(
                children: [
                  QRView(
                    key: qrBloc.qrKey,
                    onQRViewCreated: qrBloc.onQRViewCreated,
                    overlay: QrScannerOverlayShape(
                      borderColor: AppColors.kPrimaryColor,
                      borderRadius: 10,
                      borderLength: 30,
                      borderWidth: 10,
                      cutOutSize: scanArea,
                    ),
                  ),
                  Positioned(
                    left: responsive.heightPercent(3),
                    top: responsive.heightPercent(6),
                    child: GestureDetector(
                      behavior: HitTestBehavior.translucent,
                      onTap: () => Get.back(result: null),
                      child: const Icon(
                        Icons.close,
                        color: Colors.white,
                      ),
                    ),
                  ),
                  Positioned(
                    right: responsive.heightPercent(3),
                    top: responsive.heightPercent(6),
                    child: GestureDetector(
                      behavior: HitTestBehavior.translucent,
                      onTap: () async => await qrBloc.toggleFlash(),
                      child: Icon(
                        state.flashOn
                            ? Icons.flash_on_outlined
                            : Icons.flash_off_outlined,
                        color: Colors.white,
                      ),
                    ),
                  ),
                  Positioned(
                    right: responsive.heightPercent(3),
                    bottom: responsive.heightPercent(6),
                    child: GestureDetector(
                      behavior: HitTestBehavior.translucent,
                      onTap: () async => await qrBloc.flipCamera(),
                      child: const Icon(
                        Icons.camera_enhance_outlined,
                        color: Colors.white,
                      ),
                    ),
                  )
                ],
              ),
            ),
          );
        },
      ),
    );
  }
}
