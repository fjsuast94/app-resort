import 'package:appresort/src/presentation/core/themes/responsive.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class ButtonSocialApleeks extends StatelessWidget {
  final String? text;
  final TextStyle? textStyle;
  final VoidCallback? onPressed;
  final ButtonsApleeks button;

  const ButtonSocialApleeks({
    Key? key,
    this.text,
    required this.button,
    this.onPressed,
    this.textStyle,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final responsive = Responsive.of(context);
    Widget widget;
    switch (button) {
      case ButtonsApleeks.google:
        widget = OutlinedButton(
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Icon(
                FontAwesomeIcons.google,
                color: Colors.white,
                size: responsive.inchPercent(2),
              ),
              Text(
                text ?? 'Sign up with Google',
                textAlign: TextAlign.left,
                style: textStyle ??
                    TextStyle(
                      color: Colors.white,
                      fontSize: responsive.inchPercent(1.5),
                    ),
              ),
            ],
          ),
          /* color: Colors.transparent,
          splashColor: Colors.white30,
          highlightedBorderColor: Colors.white,
          borderSide: const BorderSide(color: Colors.white), */
          onPressed: onPressed ?? () {},
        );
        break;
      case ButtonsApleeks.facebook:
        widget = OutlinedButton(
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Icon(
                FontAwesomeIcons.facebook,
                color: Colors.white,
                size: responsive.inchPercent(2),
              ),
              Text(
                text ?? 'Sign up with Facebook',
                textAlign: TextAlign.left,
                style: textStyle ??
                    TextStyle(
                      color: Colors.white,
                      fontSize: responsive.inchPercent(1.5),
                    ),
              ),
            ],
          ),
          /* color: Colors.transparent,
          splashColor: Colors.white30,
          highlightedBorderColor: Colors.white,
          borderSide: const BorderSide(color: Colors.white), */
          onPressed: onPressed ?? () {},
        );
        break;
      case ButtonsApleeks.apple:
        widget = OutlinedButton(
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Icon(
                FontAwesomeIcons.apple,
                color: Colors.white,
                size: responsive.inchPercent(2),
              ),
              Text(
                text ?? 'Sign up with Apple',
                textAlign: TextAlign.left,
                style: textStyle ??
                    TextStyle(
                      color: Colors.white,
                      fontSize: responsive.inchPercent(1.5),
                    ),
              ),
            ],
          ),
          /* color: Colors.transparent,
          splashColor: Colors.white30,
          highlightedBorderColor: Colors.white,
          borderSide: const BorderSide(color: Colors.white), */
          onPressed: onPressed ?? () {},
        );
        break;
    }
    return widget;
  }
}

enum ButtonsApleeks {
  google,
  facebook,
  apple,
}
