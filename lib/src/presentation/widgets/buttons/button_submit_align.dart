import 'package:appresort/src/presentation/core/themes/app_colors.dart';
import 'package:flutter/material.dart';

class ButtonSubmitAlign extends StatelessWidget {
  final String text;
  final VoidCallback? submit;
  final Color color;

  const ButtonSubmitAlign({
    Key? key,
    required this.text,
    this.submit,
    this.color = AppColors.kPrimaryColor,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(top: 40.0, bottom: 20.0),
      child: Align(
        alignment: Alignment.bottomRight,
        child: ClipRRect(
          borderRadius: BorderRadius.circular(5.0),
          child: ElevatedButton(
            onPressed: submit,
            child: Text(text),
            /* color: Colors.white,
            padding: const EdgeInsets.all(10.0),
            elevation: 0.0,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(5.0),
              side: BorderSide(
                color: color,
              ),
            ), */
          ),
        ),
      ),
    );
  }
}
