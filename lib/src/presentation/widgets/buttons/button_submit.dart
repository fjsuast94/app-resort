import 'package:appresort/src/presentation/core/themes/app_colors.dart';
import 'package:flutter/material.dart';

class ButtonSubmit extends StatelessWidget {
  final VoidCallback? submit;
  final String text;
  final Color color, textColor;
  final double radiusCircular;
  final TextAlign textAlign;

  const ButtonSubmit({
    Key? key,
    this.submit,
    required this.text,
    this.color = AppColors.kPrimaryColor,
    this.textColor = Colors.white,
    this.radiusCircular = 0,
    this.textAlign = TextAlign.left,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(
        radiusCircular,
      ),
      child: TextButton(
        style: TextButton.styleFrom(
          backgroundColor: color,
          textStyle: TextStyle(color: textColor),
        ),
        child: SizedBox(
          width: MediaQuery.of(context).size.width * 0.5,
          child: Center(
            child: Text(
              text,
              textAlign: textAlign,
            ),
          ),
        ),
        onPressed: submit,
      ),
    );
  }
}
