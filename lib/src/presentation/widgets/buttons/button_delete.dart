import 'package:flutter/material.dart';

class ButtonDelete extends StatelessWidget {
  const ButtonDelete({
    Key? key,
    this.onTap,
  }) : super(key: key);

  final VoidCallback? onTap;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      behavior: HitTestBehavior.translucent,
      onTap: onTap,
      child: Container(
        decoration: BoxDecoration(
          color: Colors.red,
          borderRadius: BorderRadius.circular(10),
        ),
        child: const Icon(
          Icons.clear,
          size: 20.0,
          color: Colors.white,
        ),
      ),
    );
  }
}
