import 'package:appresort/src/presentation/core/themes/responsive.dart';
import 'package:flutter/material.dart';

class CircleButton extends StatelessWidget {
  const CircleButton({
    Key? key,
    required this.callback,
    required this.icon,
    required this.backgroundColor,
    this.height,
    this.width,
  }) : super(key: key);

  final VoidCallback callback;
  final Widget icon;
  final Color backgroundColor;
  final double? height;
  final double? width;

  @override
  Widget build(BuildContext context) {
    final responsive = Responsive.of(context);
    final heightResponfive = height ?? responsive.heightPercent(4.5);
    final widthResponfive = width ?? responsive.heightPercent(4.5);
    return ClipOval(
      child: Container(
        height: heightResponfive,
        width: widthResponfive,
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          color: backgroundColor,
        ),
        child: Material(
          color: Colors.transparent,
          child: InkWell(
            onTap: callback,
            child: Center(child: icon),
          ),
        ),
      ),
    );
  }
}
