import 'package:flutter/material.dart';

class BackButtonWidget extends StatelessWidget {
  final IconData icon;
  final Color color;

  const BackButtonWidget({
    Key? key,
    this.color = Colors.white,
    this.icon = Icons.keyboard_backspace,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: Icon(icon, color: color),
      onTap: () => Navigator.of(context).pop(),
    );
  }
}
