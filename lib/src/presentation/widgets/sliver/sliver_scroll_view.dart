import 'package:flutter/material.dart';

class SliverScrollView extends StatelessWidget {
  final Widget child;
  final ScrollController? controller;
  final bool reverse;
  final ScrollPhysics? physics;

  const SliverScrollView({
    Key? key,
    required this.child,
    this.controller,
    this.reverse = false,
    this.physics,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CustomScrollView(
      physics: physics,
      reverse: reverse,
      controller: controller,
      slivers: [
        SliverToBoxAdapter(child: child),
      ],
    );
  }
}
