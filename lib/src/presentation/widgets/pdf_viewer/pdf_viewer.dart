import 'package:advance_pdf_viewer/advance_pdf_viewer.dart';
import 'package:appresort/src/presentation/widgets/pdf_viewer/cubit/pdf_view_cubit.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class PdfViewer extends StatelessWidget {
  const PdfViewer({
    Key? key,
    required this.path,
    this.title = '',
    this.network = true,
  }) : super(key: key);

  final String path;
  final String title;
  final bool network;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(title),
      ),
      body: BlocProvider(
        create: (context) => PdfViewCubit(
          url: path,
          network: network,
        )..loadedPdf(),
        child: BlocBuilder<PdfViewCubit, PdfViewState>(
          builder: (context, state) {
            return state.loading
                ? const Center(
                    child: CircularProgressIndicator(
                      strokeWidth: 3,
                    ),
                  )
                : state.document != null
                    ? PDFViewer(
                        document: state.document!,
                        showPicker: false,
                        maxScale: 200,
                      )
                    : Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: Center(
                          child: Text(
                            state.message,
                            textAlign: TextAlign.center,
                          ),
                        ),
                      );
          },
        ),
      ),
    );
  }
}
