import 'package:advance_pdf_viewer/advance_pdf_viewer.dart';
import 'package:bloc/bloc.dart';

part 'pdf_view_state.dart';

class PdfViewCubit extends Cubit<PdfViewState> {
  PdfViewCubit({
    required this.url,
    this.network = false,
  }) : super(PdfViewState());

  final String url;
  final bool network;

  void loadedPdf() async {
    try {
      emit(PdfViewState(loading: true));
      final document = network ? await PDFDocument.fromURL(url) : await PDFDocument.fromAsset(url);
      emit(PdfViewState(document: document, loading: false));
    } catch (e) {
      emit(PdfViewState(loading: false, message: e.toString()));
    }
  }
}
