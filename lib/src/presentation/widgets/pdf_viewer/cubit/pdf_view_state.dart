part of 'pdf_view_cubit.dart';

class PdfViewState {
  PdfViewState({
    this.loading = false,
    this.document,
    this.message = '',
  });

  final bool loading;
  final PDFDocument? document;
  final String message;

  PdfViewState copyWith({
    bool? loading,
    PDFDocument? document,
    String? message,
  }) =>
      PdfViewState(
        loading: loading ?? this.loading,
        document: document ?? this.document,
        message: message ?? this.message,
      );
}
