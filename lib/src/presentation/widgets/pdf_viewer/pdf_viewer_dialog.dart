import 'package:appresort/src/presentation/widgets/pdf_viewer/cubit/pdf_view_cubit.dart';
import 'package:flutter/material.dart';
import 'package:advance_pdf_viewer/advance_pdf_viewer.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class PdfViewerDialog extends StatelessWidget {
  const PdfViewerDialog({
    Key? key,
    required this.path,
    this.network = false,
  }) : super(key: key);

  final String path;
  final bool network;

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => PdfViewCubit(
        url: path,
        network: network,
      )..loadedPdf(),
      child: BlocBuilder<PdfViewCubit, PdfViewState>(
        builder: (context, state) {
          return state.loading
              ? const Center(
                  child: CircularProgressIndicator(
                    strokeWidth: 3,
                    valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
                  ),
                )
              : Stack(
                  children: [
                    Container(
                      padding: const EdgeInsets.all(10.0),
                      child: state.document != null
                          ? PDFViewer(
                              document: state.document!,
                              showPicker: false,
                              maxScale: 100,
                              zoomSteps: 20,
                            )
                          : Scaffold(
                              body: Padding(
                                padding: const EdgeInsets.all(10.0),
                                child: Center(
                                  child: Text(
                                    state.message,
                                    textAlign: TextAlign.center,
                                  ),
                                ),
                              ),
                            ),
                    ),
                    Positioned(
                      right: 10.0,
                      top: 10.0,
                      child: GestureDetector(
                        behavior: HitTestBehavior.translucent,
                        onTap: () => Navigator.pop(context),
                        child: const Icon(
                          Icons.clear,
                          color: Colors.white,
                          size: 25.0,
                        ),
                      ),
                    )
                  ],
                );
        },
      ),
    );
  }
}
