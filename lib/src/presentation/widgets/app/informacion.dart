import 'package:appresort/src/presentation/core/themes/app_colors.dart';
import 'package:flutter/material.dart';

class AppInformation {
  const AppInformation({
    required double width,
    required double height,
    required BuildContext context,
  })  : _width = width,
        _height = height;

  factory AppInformation.of(BuildContext context) {
    final data = MediaQuery.of(context);
    final size = data.size;
    return AppInformation(
      height: size.height,
      width: size.width,
      context: context,
    );
  }

  final double _width, _height;

  Widget logo() {
    return ClipRRect(
      borderRadius: BorderRadius.circular(10.0),
      child: Container(
        height: 60.0,
        width: 60.0,
        decoration: BoxDecoration(
          image: DecorationImage(
            image: Image.asset(
              "assets/images/app_logo.png",
              fit: BoxFit.contain,
            ).image,
          ),
        ),
      ),
    );
  }

  Widget footer() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        const Text(
          "Powered by",
          style: TextStyle(color: Colors.grey),
        ),
        Image.asset(
          "assets/images/apleeks_logo.png",
          width: 90.0,
          height: 90.0,
        )
      ],
    );
  }

  Widget terminos() {
    return Container(
      margin: const EdgeInsets.only(top: 20.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[
          GestureDetector(
            child: const Text(
              "Términos y Condiciones",
              style: TextStyle(
                color: AppColors.kTextColor,
                decoration: TextDecoration.underline,
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget banner() {
    return Container(
      height: _height * 0.30,
      width: _width,
      decoration: const BoxDecoration(
        color: Colors.white,
        image: DecorationImage(
          image: AssetImage("assets/images/app_banner.png"),
          fit: BoxFit.fill,
        ),
      ),
    );
  }
}
