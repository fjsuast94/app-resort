import 'package:flutter/material.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
// ignore: implementation_imports
import 'package:flutter_form_bloc/src/utils/utils.dart';

class SelectBlocBuilder<Value> extends StatefulWidget {
  final String label;
  final SelectFieldBloc<Value, Object> selectFieldBloc;
  final FocusNode? nextFocusNode;
  final FieldBlocStringItemBuilder<Value> itemBuilder;
  final Function(Value) onChanged;
  final bool animateWhenCanShow;
  final InputDecoration decoration;
  final FieldBlocErrorBuilder? errorBuilder;

  const SelectBlocBuilder({
    Key? key,
    this.label = '',
    required this.selectFieldBloc,
    required this.itemBuilder,
    required this.onChanged,
    this.nextFocusNode,
    this.decoration = const InputDecoration(),
    this.errorBuilder,
    this.animateWhenCanShow = true,
  }) : super(key: key);

  @override
  _SelectBlocBuilderState<Value> createState() => _SelectBlocBuilderState<Value>();
}

class _SelectBlocBuilderState<Value> extends State<SelectBlocBuilder<Value>> {
  @override
  Widget build(BuildContext context) {
    return CanShowFieldBlocBuilder(
      fieldBloc: widget.selectFieldBloc,
      animate: widget.animateWhenCanShow,
      builder: (_, __) {
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(widget.label),
            const SizedBox(
              height: 10,
            ),
            BlocBuilder<SelectFieldBloc<Value, dynamic>, SelectFieldBlocState<Value, dynamic>>(
              bloc: widget.selectFieldBloc,
              builder: (context, fieldState) {
                final isEnabled = fieldBlocIsEnabled(
                  isEnabled: true,
                  enableOnlyWhenFormBlocCanSubmit: false,
                  fieldBlocState: fieldState,
                );
                final errorText = Style.getErrorText(
                  context: context,
                  errorBuilder: widget.errorBuilder,
                  fieldBlocState: fieldState,
                  fieldBloc: widget.selectFieldBloc,
                );
                return Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      decoration: BoxDecoration(
                        border: Border.all(color: Colors.black12),
                        borderRadius: BorderRadius.circular(5),
                      ),
                      padding: const EdgeInsets.symmetric(
                        horizontal: 5,
                        vertical: 5,
                      ),
                      child: Row(
                        children: [
                          Expanded(
                            child: DropdownButton<Value>(
                              value: fieldState.value,
                              style: TextStyle(
                                color: Colors.black.withOpacity(0.6),
                                fontSize: 12,
                              ),
                              icon: const SizedBox.shrink(),
                              isDense: true,
                              underline: const SizedBox.shrink(),
                              onChanged: fieldBlocBuilderOnChange<Value?>(
                                isEnabled: isEnabled,
                                nextFocusNode: widget.nextFocusNode,
                                onChanged: (value) {
                                  widget.selectFieldBloc.updateValue(value);
                                  widget.onChanged(value!);
                                  FocusScope.of(context).requestFocus(FocusNode());
                                },
                              ),
                              items:
                                  fieldState.items!.isEmpty ? null : _buildItems(fieldState.items!),
                            ),
                          ),
                          Icon(
                            Icons.arrow_downward,
                            color: Colors.black.withOpacity(0.4),
                            size: 20,
                          ),
                        ],
                      ),
                    ),
                    errorText == null || errorText.isEmpty
                        ? const SizedBox.shrink()
                        : Container(
                            padding: const EdgeInsets.symmetric(
                              horizontal: 0.0,
                              vertical: 10.0,
                            ),
                            child: Text(
                              errorText,
                              style: const TextStyle(
                                color: Colors.red,
                                fontSize: 10,
                              ),
                            ),
                          )
                  ],
                );
              },
            ),
          ],
        );
      },
    );
  }

  List<DropdownMenuItem<Value>> _buildItems(Iterable<Value> items) {
    List<DropdownMenuItem<Value>> menuItems;
    menuItems = items.map<DropdownMenuItem<Value>>(
      (Value value) {
        return DropdownMenuItem<Value>(
          value: value,
          child: Text(widget.itemBuilder(context, value)),
        );
      },
    ).toList();
    return menuItems;
  }
}
