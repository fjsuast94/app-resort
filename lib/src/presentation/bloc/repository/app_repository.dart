import 'package:appresort/src/data/usecase/ticket.dart';
import 'package:appresort/src/presentation/views/tickets/cubit/ticket_cubit.dart';
import 'package:get_storage/get_storage.dart';
import 'package:appresort/src/data/repository/local/auth/local_auth.dart';
import 'package:appresort/src/data/repository/local/http/http_dio.dart';
import 'package:appresort/src/data/repository/local/storage/storage_repository.dart';
import 'package:appresort/src/data/repository/remote/conekta/conekta_flutter_token.dart';
import 'package:appresort/src/data/repository/remote/socket/socket_controller.dart';
import 'package:appresort/src/data/usecase/acceso.dart';
import 'package:appresort/src/data/usecase/auth.dart';
import 'package:appresort/src/data/usecase/notification.dart';
import 'package:device_info/device_info.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';
import 'package:local_auth/local_auth.dart';

class AppRepository extends StatelessWidget {
  const AppRepository({
    Key? key,
    required this.child,
  }) : super(key: key);

  final Widget child;

  @override
  Widget build(BuildContext context) {
    return MultiRepositoryProvider(
      providers: [
        RepositoryProvider<StorageRepository>(
          create: (context) => StorageRepository(
            storage: GetStorage(),
          ),
        ),
        RepositoryProvider<HttpDio>(
          create: (context) => HttpDio(
            baseUrl: context.read<StorageRepository>().api,
            dio: Dio(),
          ),
        ),
        RepositoryProvider<LocalAuthRepository>(
          create: (context) => LocalAuthRepository(
            localAuth: LocalAuthentication(),
            deviceInfo: DeviceInfoPlugin(),
            storage: context.read(),
          ),
        ),
        RepositoryProvider<ConektaFlutterTokenizer>(
          create: (context) => ConektaFlutterTokenizer(
            http: context.read(),
          ),
        ),
        RepositoryProvider<NotificationUseCase>(
          create: (context) => NotificationUseCase(
            http: context.read(),
            storage: context.read(),
          ),
        ),
        RepositoryProvider<SocketController>(
          create: (context) => SocketController(
            baseUrl: context.read<StorageRepository>().serverSocket,
          ),
        ),
        RepositoryProvider<AuthUseCase>(
          create: (context) => AuthUseCase(
            http: context.read(),
            storage: context.read(),
            localAuth: context.read(),
          ),
        ),
        RepositoryProvider<AccesoUseCase>(
          create: (context) => AccesoUseCase(
            http: context.read(),
            storage: context.read(),
          ),
        ),
      ],
      // child: child,
      child: MultiBlocProvider(
        providers: [
          // BlocProvider<OneSignalController>(
          //   create: (BuildContext context) => OneSignalController(
          //     notificationUse: context.read(),
          //   )..initPlatformState(),
          // ),
          BlocProvider<TicketCubit>(
            create: (context) => TicketCubit(
              pagination: PagingController(firstPageKey: 1),
              ticketUse: TicketUseCase(
                http: context.read(),
                storage: context.read(),
              ),
            )..init(),
          )
        ],
        child: child,
      ),
    );
  }
}
