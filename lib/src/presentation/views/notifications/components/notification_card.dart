import 'package:appresort/src/data/model/notification.dart';
import 'package:appresort/src/presentation/core/themes/app_colors.dart';
import 'package:appresort/src/presentation/core/themes/responsive.dart';
import 'package:appresort/src/presentation/widgets/alerts/alert_delete_notification.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class NotificationCard extends StatelessWidget {
  const NotificationCard({
    Key? key,
    required this.notification,
    this.onTap,
    this.onDelete,
  }) : super(key: key);

  final VoidCallback? onTap;
  final VoidCallback? onDelete;

  final NotificationModel notification;

  @override
  Widget build(BuildContext context) {
    final responsive = Responsive.of(context);
    return Dismissible(
      key: UniqueKey(),
      direction: DismissDirection.endToStart,
      confirmDismiss: (DismissDirection direction) async {
        if (direction == DismissDirection.endToStart) {
          final bool response = await showDialog(
            barrierColor: Colors.transparent.withOpacity(0.2),
            barrierDismissible: false,
            context: context,
            builder: (BuildContext context) => const AlertDeleteNotification(),
          );
          if (response) {
            if (onDelete != null) {
              onDelete!.call();
              return true;
            }
          }
        }
        return false;
      },
      child: GestureDetector(
        onTap: onTap,
        child: Container(
          width: double.infinity,
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(5),
          ),
          padding: const EdgeInsets.symmetric(
            horizontal: 15,
            vertical: 10,
          ),
          margin: const EdgeInsets.symmetric(
            horizontal: 20,
            vertical: 8,
          ),
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  CachedNetworkImage(
                    imageUrl: notification.senderAvatar,
                    imageBuilder: (context, image) => CircleAvatar(
                      radius: 15,
                      backgroundColor: Colors.white,
                      backgroundImage: image,
                    ),
                    placeholder: (context, url) => const CircleAvatar(
                      radius: 15,
                      backgroundColor: Color(0xFFAABBCC),
                      child: Text("AR"),
                    ),
                    errorWidget: (context, url, error) => const CircleAvatar(
                      radius: 15,
                      backgroundColor: Color(0xFFAABBCC),
                      child: Text("AR"),
                    ),
                    fadeOutDuration: const Duration(milliseconds: 400),
                    fadeInDuration: const Duration(milliseconds: 800),
                  ),
                  const SizedBox(width: 20),
                  Expanded(
                    child: Text(
                      notification.sender,
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Colors.black,
                        fontSize: responsive.inchPercent(1.2),
                      ),
                    ),
                  ),
                  Expanded(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        CircleAvatar(
                          radius: 5,
                          backgroundColor: notification.status ? Colors.green : Colors.white,
                        ),
                      ],
                    ),
                  ),
                ],
              ),
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const SizedBox(height: 5),
                        notification.title.isEmpty
                            ? const SizedBox.shrink()
                            : Text(
                                notification.title,
                                style: TextStyle(
                                  color: Colors.black.withOpacity(0.8),
                                ),
                              ),
                        SizedBox(
                          height: notification.title.isEmpty ? 0 : 5,
                        ),
                        notification.description.isEmpty
                            ? const SizedBox.shrink()
                            : Text(
                                notification.description,
                                overflow: TextOverflow.ellipsis,
                                style: TextStyle(
                                  color: Colors.black.withOpacity(0.5),
                                ),
                              )
                      ],
                    ),
                  ),
                  Text(
                    notification.time,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                      color: AppColors.kPrimaryColor.withOpacity(0.9),
                      fontWeight: FontWeight.bold,
                      fontSize: 10,
                    ),
                  )
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
