part of 'notification_cubit.dart';

class NotificationState {
  NotificationState({
    this.notification,
    this.user,
  });

  final NotificationModel? notification;
  final UserModel? user;

  NotificationState copyWith({
    NotificationModel? notification,
    UserModel? user,
  }) =>
      NotificationState(
        notification: notification ?? this.notification,
        user: user ?? this.user,
      );
}
