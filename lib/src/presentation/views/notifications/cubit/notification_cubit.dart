import 'package:appresort/src/data/model/notification.dart';
import 'package:appresort/src/data/model/user.dart';
import 'package:appresort/src/data/usecase/notification.dart';
import 'package:bloc/bloc.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';

part 'notification_state.dart';

class NotificationCubit extends Cubit<NotificationState> {
  NotificationCubit({
    required PagingController<int, NotificationModel> pagination,
    required NotificationUseCase notificationUse,
  })  : _notificationUse = notificationUse,
        _pagination = pagination,
        super(NotificationState());

  final PagingController<int, NotificationModel> _pagination;
  PagingController<int, NotificationModel> get pagination => _pagination;
  static const _pageSize = 10;
  final NotificationUseCase _notificationUse;

  void init() {
    pagination.addPageRequestListener(getRegulations);
  }

  Future<void> getRegulations(int pageKey) async {
    final list = await _notificationUse.show(page: pageKey);
    if (list.message.isNotEmpty) {
      _pagination.error = list.message;
      return;
    }
    list.itemList.length <= _pageSize
        ? _pagination.appendLastPage(list.itemList)
        : _pagination.appendPage(list.itemList, pageKey + 1);
  }

  Future<void> selectNotification(NotificationModel v) async {
    final user = _notificationUse.getUser();
    await _notificationUse.read(idnotification: v.id);
    pagination.refresh();
    emit(state.copyWith(notification: v, user: user));
  }

  Future<bool> delete(int id) async {
    return await _notificationUse.delete(id: id);
  }
}
