import 'package:appresort/src/data/model/notification.dart';
import 'package:appresort/src/presentation/core/themes/app_colors.dart';
import 'package:appresort/src/presentation/core/utils/my_icons.dart';
import 'package:appresort/src/presentation/shared/logger/logger_utils.dart';
import 'package:appresort/src/presentation/views/notifications/cubit/notification_cubit.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class NotificationContentView extends StatelessWidget {
  static const String routeName = '/notification-content';
  const NotificationContentView({
    Key? key,
    required this.notification,
  }) : super(key: key);

  final NotificationModel notification;

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<NotificationCubit, NotificationState>(
      builder: (context, state) {
        final user = state.user;
        return Scaffold(
          backgroundColor: Colors.white,
          appBar: AppBar(
            title: Text(notification.title),
          ),
          body: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                  margin: const EdgeInsets.only(bottom: 10.0),
                  child: Column(
                    children: <Widget>[
                      Container(
                        child: MyIcons.name(
                          name: notification.icon,
                          size: 100.0,
                          color: AppColors.kSecondColor,
                        ),
                      ),
                      const SizedBox(height: 30.0),
                      Text(
                        "Hola ${user!.name}",
                        style: const TextStyle(fontSize: 15.0),
                      )
                    ],
                  ),
                ),
                Container(
                  padding: const EdgeInsets.all(20.0),
                  margin: const EdgeInsets.only(bottom: 50.0),
                  child: Text(
                    notification.description,
                    textAlign: TextAlign.center,
                  ),
                ),
                notification.page.isNotEmpty
                    ? ClipRRect(
                        borderRadius: BorderRadius.circular(5.0),
                        child: ElevatedButton(
                          onPressed: () {
                            Logger.write("Ir la pagina");
                          },
                          child: Container(
                            child: const Text('Ir a la página'),
                            padding: const EdgeInsets.symmetric(
                              horizontal: 80.0,
                              vertical: 15.0,
                            ),
                          ),
                        ),
                      )
                    : const SizedBox.shrink()
              ],
            ),
          ),
        );
      },
    );
  }
}
