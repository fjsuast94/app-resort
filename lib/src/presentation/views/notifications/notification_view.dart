import 'package:appresort/src/data/model/notification.dart';
import 'package:appresort/src/presentation/core/navigator/routes.dart';
import 'package:appresort/src/presentation/views/notifications/components/notification_card.dart';
import 'package:appresort/src/presentation/views/notifications/cubit/notification_cubit.dart';
import 'package:appresort/src/presentation/views/notifications/notification_content_view.dart';
import 'package:appresort/src/presentation/widgets/cards/card_refresh.dart';
import 'package:appresort/src/presentation/widgets/sliver/refresh_sliver_scroll.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';

class NotificationView extends StatelessWidget {
  static const String routeName = '/notification';
  const NotificationView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => NotificationCubit(
        pagination: PagingController(firstPageKey: 1),
        notificationUse: context.read(),
      )..init(),
      child: Scaffold(
        appBar: AppBar(
          title: const Text("Notificaciones"),
        ),
        body: BlocBuilder<NotificationCubit, NotificationState>(
          builder: (context, state) {
            final cubit = context.read<NotificationCubit>();
            final paginationController = cubit.pagination;
            return RefreshSliverScrollView(
              onRefresh: () => paginationController.refresh(),
              slivers: [
                const SliverToBoxAdapter(child: SizedBox(height: 0)),
                PagedSliverList.separated(
                  pagingController: paginationController,
                  builderDelegate: PagedChildBuilderDelegate<NotificationModel>(
                    itemBuilder: (context, notification, index) => NotificationCard(
                      notification: notification,
                      onDelete: () => cubit.delete(notification.id),
                      onTap: () {
                        cubit.selectNotification(notification);
                        Routes.to(
                          () => BlocProvider.value(
                            value: cubit,
                            child: NotificationContentView(notification: notification),
                          ),
                        );
                      },
                    ),
                    firstPageErrorIndicatorBuilder: (context) => CardRefresh(
                      title: Text("${paginationController.error}"),
                    ),
                    noItemsFoundIndicatorBuilder: (context) => const CardRefresh(
                      title: Text("No hay notificaciones disponibles"),
                    ),
                  ),
                  separatorBuilder: (context, index) => const SizedBox(
                    height: 0,
                  ),
                ),
              ],
            );
          },
        ),
      ),
    );
  }
}
