import 'package:appresort/src/data/model/ticket.dart';
import 'package:appresort/src/data/usecase/ticket.dart';
import 'package:bloc/bloc.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';

class TicketCubit extends Cubit<TicketState> {
  TicketCubit({
    required PagingController<int, TicketsModel> pagination,
    required TicketUseCase ticketUse,
  })  : _ticketUse = ticketUse,
        _pagination = pagination,
        super(TicketState());

  final PagingController<int, TicketsModel> _pagination;
  PagingController<int, TicketsModel> get pagination => _pagination;

  static const _pageSize = 10;
  final TicketUseCase _ticketUse;

  void init() {
    _pagination.addPageRequestListener(getTickets);
  }

  Future<void> getTickets(int pageKey) async {
    final list = await _ticketUse.tickets(page: pageKey);
    if (list.message.isNotEmpty) {
      _pagination.error = list.message;
      return;
    }
    list.itemList.length <= _pageSize
        ? _pagination.appendLastPage(list.itemList)
        : _pagination.appendPage(list.itemList, pageKey + 1);
  }

  emitState(TicketsModel ticket) => emit(TicketState(ticket: ticket));

  @override
  Future<void> close() {
    return super.close();
  }
}

class TicketState {
  TicketState({
    this.ticket,
  });

  final TicketsModel? ticket;
}
