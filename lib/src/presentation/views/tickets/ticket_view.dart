import 'package:appresort/src/data/model/ticket.dart';
import 'package:appresort/src/presentation/core/navigator/routes.dart';
import 'package:appresort/src/presentation/views/tickets/cubit/ticket_cubit.dart';
import 'package:appresort/src/presentation/views/tickets/pages/ticket_add/ticket_add_view.dart';
import 'package:appresort/src/presentation/views/tickets/pages/ticket_content/ticket_content_view.dart';
import 'package:appresort/src/presentation/widgets/cards/card_refresh.dart';
import 'package:appresort/src/presentation/widgets/sliver/refresh_sliver_scroll.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';

class TicketView extends StatelessWidget {
  static const String routeName = '/ticket';
  const TicketView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final cubit = context.read<TicketCubit>();
    return Scaffold(
      appBar: AppBar(
        title: const Text("Tickets"),
        actions: [
          IconButton(
            icon: const Icon(Icons.add),
            onPressed: () => Routes.to(
              () => const TicketAddView(),
            ),
          )
        ],
      ),
      body: BlocBuilder<TicketCubit, TicketState>(
        builder: (context, state) {
          return RefreshSliverScrollView(
            onRefresh: () => cubit.pagination.refresh(),
            slivers: [
              const SliverToBoxAdapter(child: SizedBox(height: 0)),
              PagedSliverList.separated(
                pagingController: cubit.pagination,
                builderDelegate: PagedChildBuilderDelegate<TicketsModel>(
                  itemBuilder: (context, ticket, index) => Padding(
                    padding: const EdgeInsets.symmetric(
                      horizontal: 10,
                      vertical: 10,
                    ),
                    child: ListTile(
                      tileColor: Colors.white,
                      title: Text("Folio: ${ticket.folio}"),
                      subtitle: Text(ticket.description),
                      onTap: () {
                        cubit.emitState(ticket);
                        Routes.to(
                          () => const TicketContentView(),
                        );
                      },
                    ),
                  ),
                  firstPageErrorIndicatorBuilder: (context) => CardRefresh(
                    title: Text("${cubit.pagination.error}"),
                  ),
                  noItemsFoundIndicatorBuilder: (context) => const CardRefresh(
                    title: Text("No hay tickets disponibles"),
                  ),
                ),
                separatorBuilder: (context, index) => const SizedBox(
                  height: 0,
                ),
              ),
            ],
          );
        },
      ),
    );
  }
}
