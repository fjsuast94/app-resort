import 'dart:io';

import 'package:appresort/src/data/model/response.dart';
import 'package:appresort/src/data/model/ticket.dart';
import 'package:appresort/src/data/repository/remote/socket/socket_controller.dart';
import 'package:appresort/src/data/usecase/notification.dart';
import 'package:appresort/src/data/usecase/ticket.dart';
import 'package:appresort/src/presentation/core/utils/validator_string.dart';
import 'package:dio/dio.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:image_picker/image_picker.dart';
import 'package:rxdart/subjects.dart';

import 'package:path/path.dart' as path;

class TicketBloc extends FormBloc<String, String> {
  final TicketUseCase ticketUse;
  final NotificationUseCase notificationUse;
  final SocketController socket;
  final ImagePicker picker;

  final BehaviorSubject<File?> _image = BehaviorSubject<File?>();
  Function(File?) get addImage => _image.sink.add;
  Stream<File?> get getImage => _image.stream;
  File? get image => _image.value;

  final descripcion = TextFieldBloc<Object>(
    validators: [ValidatorString.required],
  );

  final service = SelectFieldBloc<String, Object>(
    validators: [ValidatorString.required],
    items: [],
  );

  List<CatalogoTicketModel> _catalogoTicket = <CatalogoTicketModel>[];

  TicketBloc({
    required this.ticketUse,
    required this.notificationUse,
    required this.socket,
    required this.picker,
  }) {
    socket.connect();
    addFieldBlocs(
      fieldBlocs: [
        descripcion,
        service,
      ],
    );
    obtenerCatalogoTicket();
  }

  @override
  Future<void> close() {
    socket.disconnect();
    descripcion.close();
    service.close();
    return super.close();
  }

  @override
  void onSubmitting() async {
    final response = await addTicket();
    notificationUse.store(
      modo: 1,
      title: 'Nueva ticket',
      subtitle: 'Se ha generado un nuevo ticket',
      description: response.message!,
      iduser: 0,
      idNotifiactionType: 3,
    );
    notificationUse.store(
      modo: 1,
      title: 'Nueva ticket',
      subtitle: 'Se ha generado un nuevo ticket',
      description: response.message!,
      idNotifiactionType: 3,
    );

    notificationUse.storeApp(
      modo: 3,
      title: 'Nueva ticket',
      subtitle: 'Se ha generado un nuevo ticket',
      description: response.message!,
    );

    socket.emitEvent("new message");
    response.status
        ? emitSuccess(successResponse: response.message)
        : emitFailure(failureResponse: response.message);
  }

  Future<ResponseModel> addTicket() async {
    final int serviceId =
        int.parse(_catalogoTicket.where((element) => element.texto == service.value).first.id);

    return await ticketUse.addTicket(
      idCatTicket: serviceId,
      message: descripcion.value!,
      image: image != null
          ? await MultipartFile.fromFile(image!.path, filename: path.basename(image!.path))
          : null,
    );
  }

  Future<void> obtenerCatalogoTicket() async {
    final lists = await ticketUse.catalogoTicket();
    _catalogoTicket = lists;
    final items = lists.map((e) => e.texto).toList();
    service.updateItems(items);
    if (items.isNotEmpty) {
      service.updateInitialValue(items.first);
    }
  }

  Future<bool> openGallery() async {
    final picture = await picker.pickImage(
      source: ImageSource.gallery,
      maxHeight: 500,
      maxWidth: 640,
    );

    addImage(picture != null ? File(picture.path) : null);

    return true;
  }

  Future<bool> openCamara() async {
    final picture = await picker.pickImage(
      source: ImageSource.camera,
      maxHeight: 500,
      maxWidth: 640,
    );
    addImage(picture != null ? File(picture.path) : null);
    return true;
  }

  void deleteImage() {
    addImage(null);
  }
}
