import 'dart:io';

import 'package:appresort/src/presentation/core/navigator/routes.dart';
import 'package:appresort/src/presentation/views/tickets/pages/ticket_add/bloc/ticket_bloc.dart';
import 'package:appresort/src/presentation/widgets/alerts/alert_image_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class TicketImage extends StatelessWidget {
  const TicketImage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final bloc = BlocProvider.of<TicketBloc>(context);
    return Center(
      child: StreamBuilder<File?>(
          stream: bloc.getImage,
          initialData: null,
          builder: (context, snapshot) {
            final image = snapshot.data;
            return Stack(
              children: [
                SizedBox(
                  height: 200,
                  width: double.infinity,
                  child: image == null
                      ? GestureDetector(
                          onTap: () => showSelectDialog(context),
                          child: Container(
                            decoration: BoxDecoration(
                              color: Colors.grey.withOpacity(0.4),
                            ),
                            child: const Center(
                              child: Icon(
                                Icons.add,
                                color: Colors.black54,
                              ),
                            ),
                          ),
                        )
                      : Container(
                          decoration: BoxDecoration(
                            image: DecorationImage(
                              image: Image.file(image).image,
                              fit: BoxFit.cover,
                            ),
                          ),
                        ),
                ),
                image != null
                    ? Positioned(
                        right: 10,
                        top: 10,
                        child: GestureDetector(
                          behavior: HitTestBehavior.translucent,
                          onTap: () => bloc.deleteImage(),
                          child: CircleAvatar(
                            radius: 10,
                            backgroundColor: Colors.white.withOpacity(0.5),
                            child: Icon(
                              Icons.clear,
                              size: 20,
                              color: Colors.black.withOpacity(0.9),
                            ),
                          ),
                        ),
                      )
                    : const SizedBox.shrink()
              ],
            );
          }),
    );
  }

  Future<void> showSelectDialog(BuildContext context) async {
    final bloc = context.read<TicketBloc>();
    Routes.dialog(
      AlertImagePicker(
        onPressCamara: () async {
          await bloc.openCamara();
          Routes.back();
        },
        onPressGalery: () async {
          await bloc.openGallery();
          Routes.back();
        },
      ),
    );
  }
}
