import 'package:appresort/src/data/usecase/ticket.dart';
import 'package:appresort/src/presentation/core/navigator/routes.dart';
import 'package:appresort/src/presentation/core/utils/helper.dart';
import 'package:appresort/src/presentation/views/tickets/cubit/ticket_cubit.dart';
import 'package:appresort/src/presentation/views/tickets/pages/ticket_add/bloc/ticket_bloc.dart';
import 'package:appresort/src/presentation/views/tickets/pages/ticket_add/components/ticket_image.dart';
import 'package:appresort/src/presentation/widgets/alerts/alert_custom.dart';
import 'package:appresort/src/presentation/widgets/buttons/button_submit_align.dart';
import 'package:appresort/src/presentation/widgets/forms/dropdown_bloc.dart';
import 'package:appresort/src/presentation/widgets/forms/input_text_cupertino.dart';
import 'package:appresort/src/presentation/widgets/loading/loading_apleeks.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:image_picker/image_picker.dart';

class TicketAddForm extends StatelessWidget {
  const TicketAddForm({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final cubit = context.read<TicketCubit>();
    return BlocProvider(
      create: (context) => TicketBloc(
        picker: ImagePicker(),
        notificationUse: context.read(),
        socket: context.read(),
        ticketUse: TicketUseCase(
          http: context.read(),
          storage: context.read(),
        ),
      ),
      child: Builder(
        builder: (context) {
          final ticket = BlocProvider.of<TicketBloc>(context, listen: false);
          return FormBlocListener<TicketBloc, String, String>(
            onSubmitting: (context, state) => LoadingApleeks.show(context),
            onSuccess: (context, state) async {
              LoadingApleeks.hide(context);
              await Routes.dialog(
                AlertCustom(
                  type: AlertDialogType.success,
                  text: state.successResponse!,
                  action: () => Routes.back(),
                ),
              );
              cubit.pagination.refresh();
              Routes.back();
            },
            onFailure: (context, state) async {
              LoadingApleeks.hide(context);
              Helper.error(message: state.failureResponse!);
            },
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                DropdownBloc(
                  label: 'Seleccione una opción',
                  selectFieldBloc: ticket.service,
                  itemBuilder: (context, value) => value,
                  onChanged: (s) {},
                  mode: Mode.BOTTOM_SHEET,
                  margin: const EdgeInsets.symmetric(
                    vertical: 10,
                  ),
                ),
                const SizedBox(
                  height: 10,
                ),
                const Text(
                  "*Favor de describir a detalle el caso.",
                  style: TextStyle(fontSize: 17.0),
                  textAlign: TextAlign.start,
                ),
                InputTextCupertino(
                  textFieldBloc: ticket.descripcion,
                  placeholder: "Descripción",
                ),
                const SizedBox(
                  height: 10,
                ),
                const Text(
                  "Seleccione una imagen(opcional).",
                  style: TextStyle(fontSize: 17.0),
                  textAlign: TextAlign.start,
                ),
                const SizedBox(
                  height: 20,
                ),
                const TicketImage(),
                ButtonSubmitAlign(
                  submit: ticket.submit,
                  text: "Agregar",
                )
              ],
            ),
          );
        },
      ),
    );
  }
}
