import 'package:appresort/src/presentation/views/tickets/pages/ticket_add/components/ticket_add_form.dart';
import 'package:appresort/src/presentation/widgets/forms/un_focus_form.dart';
import 'package:flutter/material.dart';

class TicketAddView extends StatelessWidget {
  static const String routeName = '/ticket-add';
  const TicketAddView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Nuevo ticket"),
      ),
      body: SingleChildScrollView(
        child: Container(
          margin: const EdgeInsets.symmetric(
            vertical: 10,
            horizontal: 10,
          ),
          child: const UnFocusForm(
            child: TicketAddForm(),
          ),
        ),
      ),
    );
  }
}
