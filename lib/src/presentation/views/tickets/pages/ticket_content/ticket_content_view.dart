import 'package:appresort/src/data/model/time_line.dart';
import 'package:appresort/src/data/usecase/ticket.dart';
import 'package:appresort/src/presentation/core/navigator/routes.dart';
import 'package:appresort/src/presentation/core/themes/app_colors.dart';
import 'package:appresort/src/presentation/views/tickets/cubit/ticket_cubit.dart';
import 'package:appresort/src/presentation/views/tickets/pages/chat/ticket_chat_view.dart';
import 'package:appresort/src/presentation/views/tickets/pages/ticket_content/cubit/ticket_content_cubit.dart';
import 'package:appresort/src/presentation/widgets/cards/card_refresh.dart';
import 'package:appresort/src/presentation/widgets/sliver/refresh_sliver_scroll.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';
import 'package:timeline_tile/timeline_tile.dart';

class TicketContentView extends StatelessWidget {
  static const String routeName = '/ticket-content';
  const TicketContentView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final cubit = context.read<TicketCubit>();
    final ticket = cubit.state.ticket;

    return Scaffold(
      appBar: AppBar(
        title: Text("Folio: ${ticket!.folio}"),
      ),
      body: BlocProvider<TicketContentCubit>(
        create: (context) => TicketContentCubit(
          idTicket: int.parse(ticket.id),
          timeLineController: PagingController(firstPageKey: 1),
          ticketUse: TicketUseCase(
            http: context.read(),
            storage: context.read(),
          ),
        )..init(),
        child: BlocBuilder<TicketContentCubit, TicketContentState>(
          builder: (context, state) {
            final timeLineController = context.read<TicketContentCubit>().timeLineController;
            return RefreshSliverScrollView(
              onRefresh: () => timeLineController.refresh(),
              slivers: [
                const SliverToBoxAdapter(child: SizedBox(height: 0)),
                PagedSliverList.separated(
                  pagingController: timeLineController,
                  builderDelegate: PagedChildBuilderDelegate<TimeLineModel>(
                    itemBuilder: (context, timeline, index) {
                      final bool par = index % 2 == 0;
                      final bool first = index == 0;
                      final last = timeLineController.itemList!.length == index + 1 &&
                          timeLineController.itemList!.length > 1;

                      return TimelineTile(
                        alignment: TimelineAlign.center,
                        isFirst: first,
                        isLast: last,
                        indicatorStyle: IndicatorStyle(
                          width: 20,
                          color: last ? Colors.green : AppColors.kPrimaryColor,
                          indicatorXY: 0.2,
                          padding: const EdgeInsets.all(8),
                        ),
                        endChild: par
                            ? Column(
                                children: [
                                  getIcon(index),
                                  const SizedBox(
                                    height: 5,
                                  ),
                                  Text(
                                    timeline.message,
                                    textAlign: TextAlign.center,
                                    style: const TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 15,
                                      color: Colors.black,
                                    ),
                                  ),
                                  const SizedBox(
                                    height: 5,
                                  ),
                                  Text(
                                    timeline.date,
                                    textAlign: TextAlign.center,
                                    style: const TextStyle(
                                      fontSize: 12,
                                      color: Colors.black,
                                    ),
                                  )
                                ],
                              )
                            : const SizedBox.shrink(),
                        startChild: !par
                            ? Column(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  getIcon(index),
                                  const SizedBox(
                                    height: 5,
                                  ),
                                  Text(
                                    timeline.message,
                                    textAlign: TextAlign.center,
                                    style: const TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 15,
                                      color: Colors.black,
                                    ),
                                  ),
                                  const SizedBox(
                                    height: 5,
                                  ),
                                  Text(
                                    timeline.date,
                                    textAlign: TextAlign.center,
                                    style: const TextStyle(
                                      fontSize: 12,
                                      color: Colors.black,
                                    ),
                                  )
                                ],
                              )
                            : const SizedBox.shrink(),
                      );
                    },
                    firstPageErrorIndicatorBuilder: (context) => CardRefresh(
                      title: Text("${timeLineController.error}"),
                    ),
                    noItemsFoundIndicatorBuilder: (context) => const CardRefresh(
                      title: Text("No hay tickets disponibles"),
                    ),
                  ),
                  separatorBuilder: (context, index) => const SizedBox(
                    height: 0,
                  ),
                ),
              ],
            );
          },
        ),
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: const Color(0xFF00C962),
        child: const Icon(
          Icons.chat,
          color: Colors.white,
        ),
        onPressed:
            ticket.status == 'Cerrado' ? () {} : () => Routes.to(() => const TicketChatView()),
      ),
    );
  }

  Widget getIcon(int index) {
    final List<Icon> icons = [
      const Icon(
        FontAwesomeIcons.ticketAlt,
        size: 30,
        color: AppColors.kPrimaryColor,
      ),
      const Icon(
        FontAwesomeIcons.signInAlt,
        size: 30,
        color: AppColors.kPrimaryColor,
      ),
      const Icon(
        FontAwesomeIcons.user,
        size: 30,
        color: AppColors.kPrimaryColor,
      ),
      const Icon(
        FontAwesomeIcons.checkCircle,
        color: Colors.green,
        size: 30,
      ),
    ];
    int indexAux = index + 1;
    return icons.length >= indexAux
        ? icons[index]
        : const Icon(
            FontAwesomeIcons.questionCircle,
            size: 30,
          );
  }
}
