import 'package:appresort/src/data/model/time_line.dart';
import 'package:appresort/src/data/usecase/ticket.dart';
import 'package:bloc/bloc.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';

part 'ticket_content_state.dart';

class TicketContentCubit extends Cubit<TicketContentState> {
  TicketContentCubit({
    required this.idTicket,
    required PagingController<int, TimeLineModel> timeLineController,
    required TicketUseCase ticketUse,
  })  : _ticketUse = ticketUse,
        _timeLineController = timeLineController,
        super(TicketContentInitial());

  final int idTicket;
  final PagingController<int, TimeLineModel> _timeLineController;
  PagingController<int, TimeLineModel> get timeLineController => _timeLineController;
  final TicketUseCase _ticketUse;
  static const _pageSize = 10;

  void init() {
    _timeLineController.addPageRequestListener(getTickets);
  }

  Future<void> getTickets(int pageKey) async {
    final list = await _ticketUse.ticketTimeLine(page: pageKey, idTicket: idTicket);
    if (list.message.isNotEmpty) {
      _timeLineController.error = list.message;
      return;
    }
    list.itemList.length <= _pageSize
        ? _timeLineController.appendLastPage(list.itemList)
        : _timeLineController.appendPage(list.itemList, pageKey + 1);
  }
}
