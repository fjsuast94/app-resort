import 'package:appresort/src/data/model/chat.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

const accentColor = Color(0xffc2185b);

class MessageItem extends StatelessWidget {
  final ChatModel message;
  const MessageItem({
    Key? key,
    required this.message,
  }) : super(key: key);

  Widget getUsernameView() {
    return Text(
      message.username,
      style: const TextStyle(
        fontSize: 12,
        color: Color(0xff718792),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return sender(message);
  }

  Widget sender(ChatModel message) {
    return Wrap(
      alignment: message.sender ? WrapAlignment.end : WrapAlignment.start,
      children: [
        Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: message.sender ? MainAxisAlignment.end : MainAxisAlignment.start,
          children: [
            !message.sender
                ? CircleAvatar(
                    backgroundImage: CachedNetworkImageProvider(
                      message.photo,
                    ),
                  )
                : const SizedBox.shrink(),
            const SizedBox(
              width: 10,
            ),
            Expanded(
              child: Column(
                crossAxisAlignment:
                    message.sender ? CrossAxisAlignment.end : CrossAxisAlignment.start,
                children: [
                  if (!message.sender) getUsernameView(),
                  Container(
                    constraints: const BoxConstraints(
                      maxWidth: 300,
                    ),
                    padding: const EdgeInsets.symmetric(
                      horizontal: 20,
                      vertical: 10,
                    ),
                    decoration: BoxDecoration(
                      color:
                          message.sender ? const Color(0xFF2BA873) : Colors.grey.withOpacity(0.1),
                      borderRadius: BorderRadius.only(
                        bottomLeft: const Radius.circular(30),
                        topRight: const Radius.circular(30),
                        topLeft: Radius.circular(message.sender ? 30 : 0),
                        bottomRight: Radius.circular(!message.sender ? 30 : 0),
                      ),
                    ),
                    child: Text(
                      message.message,
                      style: TextStyle(
                        color: message.sender ? Colors.white : Colors.black45,
                      ),
                    ),
                  ),
                  if (message.sender) getUsernameView(),
                  const SizedBox(height: 20),
                ],
              ),
            ),
          ],
        ),
      ],
    );
  }
}
