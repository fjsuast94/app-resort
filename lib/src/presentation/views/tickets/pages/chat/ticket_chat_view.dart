import 'package:appresort/src/data/model/chat.dart';
import 'package:appresort/src/data/usecase/ticket.dart';
import 'package:appresort/src/presentation/core/utils/helper.dart';
import 'package:appresort/src/presentation/views/tickets/cubit/ticket_cubit.dart';
import 'package:appresort/src/presentation/views/tickets/pages/chat/components/input_chat.dart';
import 'package:appresort/src/presentation/views/tickets/pages/chat/components/message_item.dart';
import 'package:appresort/src/presentation/views/tickets/pages/chat/cubit/ticket_chat_cubit.dart';
import 'package:appresort/src/presentation/widgets/cards/card_refresh.dart';
import 'package:appresort/src/presentation/widgets/forms/un_focus_form.dart';
import 'package:appresort/src/presentation/widgets/sliver/refresh_sliver_scroll.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';

class TicketChatView extends StatelessWidget {
  static const String routeName = '/ticket-chat';
  const TicketChatView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final ticket = context.read<TicketCubit>().state.ticket;
    return Scaffold(
      appBar: AppBar(
        title: const Text("Suporte"),
      ),
      body: BlocProvider<TicketChatCubit>(
        create: (context) => TicketChatCubit(
          idTicket: int.parse(ticket!.id),
          chatController: PagingController(firstPageKey: 1),
          ticketUse: TicketUseCase(
            http: context.read(),
            storage: context.read(),
          ),
          socket: context.read(),
          notificationUse: context.read(),
        )..init(),
        child: BlocBuilder<TicketChatCubit, TicketChatState>(
          builder: (context, state) {
            final cubit = context.read<TicketChatCubit>();
            return UnFocusForm(
              child: Container(
                width: double.infinity,
                height: double.infinity,
                padding: const EdgeInsets.all(10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Expanded(
                      child: RefreshSliverScrollView(
                        reverse: true,
                        slivers: [
                          const SliverToBoxAdapter(child: SizedBox(height: 0)),
                          PagedSliverList.separated(
                            pagingController: cubit.chatController,
                            builderDelegate: PagedChildBuilderDelegate<ChatModel>(
                              itemBuilder: (context, chat, index) => MessageItem(message: chat),
                              firstPageErrorIndicatorBuilder: (context) => CardRefresh(
                                title: Text(
                                  "${cubit.chatController.error}",
                                ),
                              ),
                              noItemsFoundIndicatorBuilder: (context) => const CardRefresh(
                                title: Text("No hay comentarios disponibles"),
                              ),
                            ),
                            separatorBuilder: (context, index) => const SizedBox(
                              height: 0,
                            ),
                          ),
                        ],
                      ),
                    ),
                    InputChat(
                      onChanged: (String s) async {
                        if (s.isNotEmpty) {
                          final res = await cubit.chatAdd(s);
                          if (res.isNotEmpty) {
                            Helper.error(message: res);
                          }
                        }
                      },
                    ),
                  ],
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}
