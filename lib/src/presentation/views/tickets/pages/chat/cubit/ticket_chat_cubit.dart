import 'package:appresort/src/data/model/chat.dart';
import 'package:appresort/src/data/repository/remote/socket/socket_controller.dart';
import 'package:appresort/src/data/usecase/notification.dart';
import 'package:appresort/src/data/usecase/ticket.dart';
import 'package:bloc/bloc.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';

part 'ticket_chat_state.dart';

class TicketChatCubit extends Cubit<TicketChatState> {
  TicketChatCubit({
    required this.idTicket,
    required PagingController<int, ChatModel> chatController,
    required TicketUseCase ticketUse,
    required this.socket,
    required NotificationUseCase notificationUse,
  })  : _ticketUse = ticketUse,
        _notificationUse = notificationUse,
        _chatController = chatController,
        super(TicketChatInitial());

  final PagingController<int, ChatModel> _chatController;
  PagingController<int, ChatModel> get chatController => _chatController;

  final TicketUseCase _ticketUse;
  final int idTicket;
  final SocketController socket;
  final NotificationUseCase _notificationUse;

  static const _pageSize = 10;

  void init() {
    _chatController.addPageRequestListener(getTicketChat);
    socket.connect();
  }

  @override
  Future<void> close() {
    socket.disconnect();
    return super.close();
  }

  Future<void> getTicketChat(int pageKey) async {
    final list = await _ticketUse.ticketChat(page: pageKey, idTicket: idTicket);
    if (list.message.isNotEmpty) {
      _chatController.error = list.message;
      return;
    }
    list.itemList.length <= _pageSize
        ? _chatController.appendLastPage(list.itemList)
        : _chatController.appendPage(list.itemList, pageKey + 1);
  }

  Future<String> chatAdd(String s) async {
    String error = '';

    final resp = await _ticketUse.ticketChatAdd(
      idTicket: idTicket,
      message: s,
    );

    _notificationUse.store(
      modo: 1,
      title: 'Nuevo mensaje ticket',
      subtitle: 'Se ha agregado un nuevo mensaje.',
      description: 'Se ha agregado un nuevo mensaje.',
      iduser: 0,
      idNotifiactionType: 4,
    );
    _notificationUse.store(
      modo: 1,
      title: 'Nuevo mensaje ticket',
      subtitle: 'Se ha agregado un nuevo mensaje.',
      description: 'Se ha agregado un nuevo mensaje.',
      idNotifiactionType: 4,
    );

    _notificationUse.storeApp(
      modo: 3,
      title: 'Nuevo mensaje ticket',
      subtitle: 'Se ha agregado un nuevo mensaje.',
      description: 'Se ha agregado un nuevo mensaje.',
    );

    socket.emitEvent("new notification");
    socket.emitEvent("new message");

    resp.status ? chatController.refresh() : error = resp.message!;

    return error;
  }
}
