import 'package:appresort/src/presentation/core/navigator/routes.dart';
import 'package:appresort/src/presentation/core/themes/app_colors.dart';
import 'package:appresort/src/presentation/views/password/password_view.dart';
import 'package:appresort/src/presentation/views/settings/cubit/setting_cubit.dart';
import 'package:appresort/src/presentation/widgets/alerts/alert_actions.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class SettingBody extends StatelessWidget {
  const SettingBody({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<SettingCubit, SettingState>(
      builder: (context, state) {
        final cubit = context.read<SettingCubit>();
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            ListTile(
              contentPadding: EdgeInsets.zero,
              title: Text(
                "Cambiar contraseña",
                style: TextStyle(
                  fontSize: 15.0,
                  fontWeight: FontWeight.w400,
                  color: Colors.black.withOpacity(0.5),
                ),
              ),
              trailing: const Icon(Icons.arrow_forward_outlined),
              onTap: () => Routes.fullDialog(
                () => const PassWordView(),
              ),
              tileColor: Colors.transparent,
              selectedTileColor: Colors.transparent,
              dense: true,
            ),
            state.isAndroid8OrInferiror
                ? SwitchListTile(
                    activeColor: AppColors.kPrimaryColor,
                    contentPadding: EdgeInsets.zero,
                    value: state.biometric,
                    title: const Text(
                      "Habilitar datos biométricos",
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 15,
                      ),
                    ),
                    subtitle: const Text(
                      "Sera tu clave de seguridad y contraseña",
                      style: TextStyle(
                        fontSize: 12,
                      ),
                    ),
                    onChanged: cubit.toggleTheme,
                  )
                : const SizedBox.shrink(),
            ListTile(
              contentPadding: EdgeInsets.zero,
              title: Text(
                "Cerrar Sessión",
                style: TextStyle(
                  fontSize: 15.0,
                  fontWeight: FontWeight.w400,
                  color: Colors.black.withOpacity(0.5),
                ),
              ),
              trailing: const Icon(Icons.logout),
              onTap: () {
                Routes.dialog(
                  AlertAction(
                    title: "¿Deseas cerrar sesión?",
                    onPressYes: () async {
                      await cubit.logout();
                      Routes.offAllNamed('/login');
                    },
                  ),
                );
              },
              tileColor: Colors.transparent,
              selectedTileColor: Colors.transparent,
              dense: true,
            ),
          ],
        );
      },
    );
  }
}
