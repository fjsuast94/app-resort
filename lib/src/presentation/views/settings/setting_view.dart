import 'package:appresort/src/presentation/views/settings/components/setting_body.dart';
import 'package:appresort/src/presentation/views/settings/components/setting_header.dart';
import 'package:appresort/src/presentation/views/settings/cubit/setting_cubit.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class SettingView extends StatelessWidget {
  static const String routeName = '/settings';
  const SettingView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Configuración"),
      ),
      body: Container(
        margin: const EdgeInsets.symmetric(
          horizontal: 20,
          vertical: 10,
        ),
        child: BlocProvider(
          create: (context) => SettingCubit(
            auth: context.read(),
          )..init(),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: const [
              SettingHeader(),
              Divider(),
              SettingBody(),
            ],
          ),
        ),
      ),
    );
  }
}
