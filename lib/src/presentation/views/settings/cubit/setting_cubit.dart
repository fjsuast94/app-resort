import 'package:appresort/src/data/model/user.dart';
import 'package:appresort/src/data/usecase/auth.dart';
import 'package:bloc/bloc.dart';

part 'setting_state.dart';

class SettingCubit extends Cubit<SettingState> {
  SettingCubit({
    required AuthUseCase auth,
  })  : _auth = auth,
        super(SettingState());

  final AuthUseCase _auth;

  Future<void> init() async {
    final user = _auth.getUser();
    emit(state.copyWith(user: user));

    final isAndroid8OrInferiror = await _auth.isAndroid8OrInferiror();
    emit(state.copyWith(
      biometric: await _auth.getBiometric(),
      isAndroid8OrInferiror: isAndroid8OrInferiror,
    ));
  }

  Future<void> logout() async {
    await _auth.logout();
  }

  void toggleTheme(bool biometric) {
    _auth.setBiometric(biometric);
    emit(state.copyWith(biometric: biometric));
  }
}
