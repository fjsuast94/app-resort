import 'package:appresort/src/presentation/core/navigator/routes.dart';
import 'package:appresort/src/presentation/core/utils/helper.dart';
import 'package:appresort/src/presentation/views/password/bloc/password.dart';
import 'package:appresort/src/presentation/widgets/alerts/alert_custom.dart';
import 'package:appresort/src/presentation/widgets/buttons/button_submit.dart';
import 'package:appresort/src/presentation/widgets/forms/input_text_cupertino.dart';
import 'package:appresort/src/presentation/widgets/loading/loading_apleeks.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';

class PassWordView extends StatelessWidget {
  static const String routeName = '/password';
  const PassWordView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => PasswordBloc(
        auth: context.read(),
      ),
      child: Builder(
        builder: (context) {
          final bloc = context.read<PasswordBloc>();
          return FormBlocListener<PasswordBloc, String, String>(
            onSubmitting: (context, state) => LoadingApleeks.show(context),
            onSuccess: (context, state) {
              LoadingApleeks.hide(context);
              Routes.dialog(
                AlertCustom(
                  type: AlertDialogType.success,
                  text: state.successResponse!,
                  action: () => Routes.offAndToNamed('/login'),
                ),
              );
            },
            onFailure: (context, state) {
              LoadingApleeks.hide(context);
              Helper.error(message: state.failureResponse!);
            },
            child: Scaffold(
              appBar: AppBar(
                title: const Text("Cambiar contraseña"),
              ),
              body: Container(
                margin: const EdgeInsets.symmetric(
                  vertical: 20,
                  horizontal: 10,
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    InputTextCupertino(
                      textFieldBloc: bloc.password,
                      placeholder: 'Nueva contraseña',
                      suffixButton: SuffixButton.obscureText,
                      keyboardType: TextInputType.text,
                    ),
                    InputTextCupertino(
                      textFieldBloc: bloc.confirm,
                      placeholder: 'Repetir contraseña',
                      suffixButton: SuffixButton.obscureText,
                      keyboardType: TextInputType.text,
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                    Center(
                      child: ButtonSubmit(
                        submit: bloc.submit,
                        text: "Acceder",
                      ),
                    ),
                  ],
                ),
              ),
            ),
          );
        },
      ),
    );
  }
}
