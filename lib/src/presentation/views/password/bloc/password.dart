import 'package:appresort/src/data/usecase/auth.dart';
import 'package:appresort/src/presentation/core/utils/validator_string.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';

class PasswordBloc extends FormBloc<String, String> {
  final password = TextFieldBloc<Object>(
    validators: [ValidatorString.required],
  );
  final confirm = TextFieldBloc<Object>(
    validators: [ValidatorString.required],
  );

  final AuthUseCase _auth;

  PasswordBloc({
    required AuthUseCase auth,
  }) : _auth = auth {
    addFieldBlocs(fieldBlocs: [password, confirm]);
    confirm
      ..addValidators([ValidatorString.confirmPassword(password)])
      ..subscribeToFieldBlocs([password]);
  }

  @override
  void onSubmitting() async {
    final response = await _auth.changePassWord(password.value!);
    response.status
        ? emitSuccess(successResponse: response.message)
        : emitFailure(failureResponse: response.message);
  }
}
