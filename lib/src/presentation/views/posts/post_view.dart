import 'package:appresort/src/data/model/post.dart';
import 'package:appresort/src/data/usecase/post.dart';
import 'package:appresort/src/presentation/core/utils/helper.dart';
import 'package:appresort/src/presentation/views/posts/components/post_card.dart';
import 'package:appresort/src/presentation/views/posts/cubit/post_cubit.dart';
import 'package:appresort/src/presentation/widgets/cards/card_refresh.dart';
import 'package:appresort/src/presentation/widgets/sliver/refresh_sliver_scroll.dart';
import 'package:esys_flutter_share_plus/esys_flutter_share_plus.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';

class PostView extends StatelessWidget {
  static const String routeName = '/post';
  const PostView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Publicaciones"),
      ),
      body: BlocProvider(
        create: (context) => PostCubit(
          pagination: PagingController(firstPageKey: 1),
          postUse: PostUseCase(
            http: context.read(),
            storage: context.read(),
          ),
        )..init(),
        child: BlocConsumer<PostCubit, PostState>(
          listener: (context, state) async {
            if (state.pngBytes != null) {
              await Share.file(
                'Publicaciones',
                'share_publicaciones.png',
                state.pngBytes!.buffer.asUint8List(),
                'image/png',
              );
            } else {
              Helper.error(message: state.message);
            }
          },
          builder: (context, state) {
            final paginationController = context.read<PostCubit>().pagination;
            return RefreshSliverScrollView(
              onRefresh: () => paginationController.refresh(),
              slivers: [
                const SliverToBoxAdapter(child: SizedBox(height: 0)),
                PagedSliverList.separated(
                  pagingController: paginationController,
                  builderDelegate: PagedChildBuilderDelegate<PostModel>(
                    itemBuilder: (context, post, index) {
                      GlobalKey postKey = GlobalKey(
                        debugLabel: index.toString(),
                      );
                      return Padding(
                        padding: const EdgeInsets.symmetric(
                          horizontal: 10,
                          vertical: 10,
                        ),
                        child: PostItem(
                          post: post,
                          postKey: postKey,
                          onShared: () =>
                              context.read<PostCubit>().captureAndSharePng(key: postKey),
                        ),
                      );
                    },
                    firstPageErrorIndicatorBuilder: (context) => CardRefresh(
                      title: Text("${paginationController.error}"),
                    ),
                    noItemsFoundIndicatorBuilder: (context) => const CardRefresh(
                      title: Text("No hay publicaciones disponibles"),
                    ),
                  ),
                  separatorBuilder: (context, index) => const SizedBox(
                    height: 10,
                  ),
                ),
              ],
            );
          },
        ),
      ),
    );
  }
}
