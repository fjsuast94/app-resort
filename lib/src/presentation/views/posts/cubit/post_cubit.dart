import 'dart:ui';

import 'package:appresort/src/data/model/post.dart';
import 'package:appresort/src/data/usecase/post.dart';
import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';

import 'dart:typed_data';

part 'post_state.dart';

class PostCubit extends Cubit<PostState> {
  PostCubit({
    required PagingController<int, PostModel> pagination,
    required PostUseCase postUse,
  })  : _postUse = postUse,
        _pagination = pagination,
        super(PostState());

  final PagingController<int, PostModel> _pagination;
  PagingController<int, PostModel> get pagination => _pagination;
  final PostUseCase _postUse;
  static const _pageSize = 10;

  void init() {
    _pagination.addPageRequestListener(getPosts);
  }

  Future<void> getPosts(int pageKey) async {
    final list = await _postUse.posts(pageKey);
    if (list.message.isNotEmpty) {
      _pagination.error = list.message;
      return;
    }
    list.itemList.length <= _pageSize
        ? _pagination.appendLastPage(list.itemList)
        : _pagination.appendPage(list.itemList, pageKey + 1);
  }

  Future<void> captureAndSharePng({required GlobalKey key}) async {
    try {
      RenderRepaintBoundary boundary =
          key.currentContext!.findRenderObject() as RenderRepaintBoundary;
      var image = await boundary.toImage();
      ByteData? byteData = await image.toByteData(format: ImageByteFormat.png);
      Uint8List pngBytes = byteData!.buffer.asUint8List();
      emit(PostState(pngBytes: pngBytes));
    } catch (e) {
      emit(PostState(message: e.toString()));
    }
  }
}
