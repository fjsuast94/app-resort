part of 'post_cubit.dart';

class PostState {
  PostState({
    this.pngBytes,
    this.message = '',
  });

  final Uint8List? pngBytes;
  final String message;
}
