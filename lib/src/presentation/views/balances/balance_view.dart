import 'package:appresort/src/data/model/balance_saldo.dart';
import 'package:appresort/src/data/model/last_transactions.dart';
import 'package:appresort/src/data/usecase/balance.dart';
import 'package:appresort/src/presentation/core/navigator/routes.dart';
import 'package:appresort/src/presentation/core/utils/my_icons.dart';
import 'package:appresort/src/presentation/views/balances/components/card_balance.dart';
import 'package:appresort/src/presentation/views/balances/components/card_last_transactions.dart';
import 'package:appresort/src/presentation/views/balances/cubit/balance_cubit.dart';
import 'package:appresort/src/presentation/widgets/cards/card_refresh.dart';
import 'package:appresort/src/presentation/widgets/sliver/refresh_sliver_scroll.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';

class BalanceView extends StatelessWidget {
  static const String routeName = '/balance';
  const BalanceView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => BalanceCubit(
        balanceUse: BalanceUseCase(
          http: context.read(),
          storage: context.read(),
        ),
        paginationSummary: PagingController<int, BalanceSaldoModel>(
          firstPageKey: 1,
        ),
        paginationCharges: PagingController<int, LastTransations>(
          firstPageKey: 1,
        ),
      )..init(),
      child: Scaffold(
        appBar: AppBar(
          title: const Text("Saldos"),
        ),
        body: BlocBuilder<BalanceCubit, BalanceState>(
          builder: (context, state) {
            final cubit = context.read<BalanceCubit>();
            final paginationSummary = cubit.paginationSummary;
            final paginationCharges = cubit.paginationCharges;
            return Padding(
              padding: const EdgeInsets.all(8.0),
              child: RefreshSliverScrollView(
                onRefresh: () => cubit.refresh(),
                slivers: [
                  const SliverToBoxAdapter(child: SizedBox(height: 0)),
                  PagedSliverList.separated(
                    pagingController: paginationSummary,
                    builderDelegate: PagedChildBuilderDelegate<BalanceSaldoModel>(
                      itemBuilder: (context, balance, index) => CardBalance(
                        balance: balance,
                      ),
                      firstPageErrorIndicatorBuilder: (context) => CardRefresh(
                        title: Text("Hola mundo ${paginationSummary.error}"),
                      ),
                      noItemsFoundIndicatorBuilder: (context) => const CardRefresh(
                        title: Text("No hay información disponibles"),
                      ),
                    ),
                    separatorBuilder: (context, index) => const SizedBox(
                      height: 0,
                    ),
                  ),
                  SliverToBoxAdapter(
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Expanded(
                          flex: 2,
                          child: GestureDetector(
                            onTap: () => Routes.toNamed("/estado-cuenta"),
                            child: Container(
                              decoration: BoxDecoration(
                                color: Colors.white,
                                boxShadow: [
                                  BoxShadow(
                                    blurRadius: 2.0,
                                    color: Colors.grey.withOpacity(0.3),
                                  ),
                                ],
                              ),
                              padding: const EdgeInsets.all(5.0),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  MyIcons.name(name: "assignment"),
                                  const SizedBox(width: 5),
                                  Text(
                                    'Estado de cuenta',
                                    overflow: TextOverflow.ellipsis,
                                    style: TextStyle(
                                      fontSize: 15,
                                      fontWeight: FontWeight.bold,
                                      color: Colors.black.withOpacity(0.7),
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ),
                        ),
                        const SizedBox(width: 10),
                        Expanded(
                          flex: 2,
                          child: GestureDetector(
                            onTap: () => Routes.toNamed("/charge"),
                            child: Container(
                              decoration: BoxDecoration(
                                color: Colors.white,
                                boxShadow: [
                                  BoxShadow(
                                    blurRadius: 2.0,
                                    color: Colors.grey.withOpacity(0.3),
                                  ),
                                ],
                              ),
                              padding: const EdgeInsets.all(5.0),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  MyIcons.name(name: "payment"),
                                  const SizedBox(width: 5),
                                  Text(
                                    'Pago en línea',
                                    style: TextStyle(
                                      fontSize: 15,
                                      fontWeight: FontWeight.bold,
                                      color: Colors.black.withOpacity(0.7),
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  const SliverToBoxAdapter(
                    child: SizedBox(
                      height: 10,
                    ),
                  ),
                  SliverToBoxAdapter(
                    child: Text(
                      "Movimientos de los últimos 10 días".toUpperCase(),
                      style: TextStyle(
                        color: Colors.black.withOpacity(0.4),
                        fontWeight: FontWeight.bold,
                        fontSize: 16,
                      ),
                    ),
                  ),
                  PagedSliverList.separated(
                    pagingController: paginationCharges,
                    builderDelegate: PagedChildBuilderDelegate<LastTransations>(
                      itemBuilder: (context, transaction, index) => CardLastTransactions(
                        transation: transaction,
                      ),
                      firstPageErrorIndicatorBuilder: (context) => CardRefresh(
                        title: Text("${paginationCharges.error}"),
                      ),
                      noItemsFoundIndicatorBuilder: (context) => const CardRefresh(
                        title: Text("No hay información disponibles"),
                      ),
                    ),
                    separatorBuilder: (context, index) => const SizedBox(
                      height: 0,
                    ),
                  ),
                ],
              ),
            );
          },
        ),
      ),
    );
  }
}
