import 'package:appresort/src/data/model/last_transactions.dart';
import 'package:appresort/src/presentation/core/utils/my_icons.dart';
import 'package:flutter/material.dart';

class CardLastTransactions extends StatelessWidget {
  const CardLastTransactions({
    Key? key,
    required this.transation,
  }) : super(key: key);

  final LastTransations transation;
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(
        vertical: 10,
      ),
      padding: const EdgeInsets.symmetric(
        horizontal: 10,
        vertical: 10,
      ),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(5),
      ),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                "Departamento",
                style: TextStyle(
                  color: Colors.black.withOpacity(0.8),
                  fontSize: 18,
                  fontWeight: FontWeight.bold,
                ),
              ),
              const SizedBox(width: 10),
              Text(
                transation.unidad,
                style: TextStyle(
                  color: Colors.black.withOpacity(0.8),
                  fontSize: 18,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ],
          ),
          const SizedBox(height: 10),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                transation.concepto,
                style: TextStyle(
                  color: Colors.black.withOpacity(0.8),
                  fontSize: 15,
                ),
              ),
              Text(
                transation.fecha,
                style: TextStyle(
                  color: Colors.black.withOpacity(0.8),
                  fontSize: 15,
                ),
              ),
              Container(
                child: transation.esCargo
                    ? Text(
                        "\$ - ${transation.cargo}",
                        style: const TextStyle(
                          color: Colors.red,
                          fontWeight: FontWeight.bold,
                          fontSize: 15,
                        ),
                      )
                    : Text(
                        "\$ ${transation.abono}",
                        style: const TextStyle(
                          color: Colors.green,
                          fontWeight: FontWeight.bold,
                          fontSize: 15,
                        ),
                      ),
              )
            ],
          ),
          const SizedBox(height: 5),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                transation.tipoConcepto,
                style: TextStyle(
                  color: Colors.black.withOpacity(0.5),
                ),
              ),
              Text(
                transation.metodo.isNotEmpty ? "(${transation.metodo})" : '',
                style: TextStyle(
                  color: Colors.black.withOpacity(0.8),
                  fontSize: 15,
                ),
              ),
              Container(
                child: transation.esCargo
                    ? MyIcons.name(name: 'trending_down', color: Colors.red)
                    : MyIcons.name(name: 'trending_up', color: Colors.green),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
