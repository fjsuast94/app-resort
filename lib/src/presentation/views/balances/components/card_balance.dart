import 'package:appresort/src/data/model/balance_saldo.dart';
import 'package:appresort/src/presentation/core/themes/app_colors.dart';
import 'package:appresort/src/presentation/core/utils/helper.dart';
import 'package:appresort/src/presentation/core/utils/my_icons.dart';
import 'package:flutter/material.dart';

class CardBalance extends StatelessWidget {
  const CardBalance({
    Key? key,
    required this.balance,
  }) : super(key: key);

  final BalanceSaldoModel balance;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Stack(
          children: <Widget>[
            Container(
              padding: const EdgeInsets.all(10.0),
              margin: const EdgeInsets.symmetric(
                vertical: 20,
                horizontal: 1,
              ),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(5.0),
                boxShadow: [
                  BoxShadow(
                    blurRadius: 3.0,
                    color: Colors.grey[400]!,
                  ),
                ],
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Align(
                    alignment: Alignment.bottomRight,
                    child: RichText(
                      text: TextSpan(
                        text: 'Saldo actual: ',
                        style: TextStyle(
                          color: Colors.black.withOpacity(0.5),
                          fontWeight: FontWeight.bold,
                        ),
                        children: <TextSpan>[
                          TextSpan(
                            text: Helper.moneyFormat(balance.total),
                            style: const TextStyle(
                              fontWeight: FontWeight.bold,
                              color: Colors.green,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  const SizedBox(height: 30),
                  Text(
                    "Saldos",
                    style: TextStyle(
                      color: Colors.black.withOpacity(0.5),
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  const SizedBox(height: 10),
                  Container(
                    width: double.infinity,
                    margin: const EdgeInsets.symmetric(
                      horizontal: 10,
                    ),
                    child: balance.saldos.isNotEmpty
                        ? Column(
                            children: List.generate(balance.saldos.length, (index) {
                              final saldo = balance.saldos[index];
                              return Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Expanded(
                                    child: Text("º ${saldo.concepto}"),
                                  ),
                                  Text(
                                    "\$ ${saldo.saldo}",
                                    style: const TextStyle(fontWeight: FontWeight.bold),
                                  ),
                                ],
                              );
                            }),
                          )
                        : const Center(child: Text("No hay saldo disponible")),
                  ),
                  const SizedBox(height: 10),
                  Text(
                    "Fondos",
                    style: TextStyle(
                      color: Colors.black.withOpacity(0.5),
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  Container(
                    width: double.infinity,
                    margin: const EdgeInsets.symmetric(
                      horizontal: 10,
                    ),
                    child: balance.fondos.isNotEmpty
                        ? Column(
                            children: List.generate(balance.fondos.length, (index) {
                              final fondo = balance.fondos[index];
                              return Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Expanded(
                                    child: Text("º ${fondo.fondo}"),
                                  ),
                                  Text(
                                    "\$ ${fondo.saldo}",
                                    style: const TextStyle(fontWeight: FontWeight.bold),
                                  ),
                                ],
                              );
                            }),
                          )
                        : const Center(child: Text("No hay fondos disponibles")),
                  ),
                  const SizedBox(height: 10),
                ],
              ),
            ),
            CircleAvatar(
              radius: 30,
              backgroundColor: AppColors.kPrimaryColor,
              child: MyIcons.name(
                name: 'balance_scale',
                color: Colors.white,
              ),
            ),
          ],
        ),
      ],
    );
  }
}
