import 'package:appresort/src/data/model/balance_saldo.dart';
import 'package:appresort/src/data/model/last_transactions.dart';
import 'package:appresort/src/data/usecase/balance.dart';
import 'package:bloc/bloc.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';

part 'balance_state.dart';

class BalanceCubit extends Cubit<BalanceState> {
  BalanceCubit({
    required BalanceUseCase balanceUse,
    required PagingController<int, BalanceSaldoModel> paginationSummary,
    required PagingController<int, LastTransations> paginationCharges,
  })  : _balanceUse = balanceUse,
        _paginationSummary = paginationSummary,
        _paginationCharges = paginationCharges,
        super(BalanceInitial());

  final BalanceUseCase _balanceUse;

  final PagingController<int, BalanceSaldoModel> _paginationSummary;
  PagingController<int, BalanceSaldoModel> get paginationSummary => _paginationSummary;

  final PagingController<int, LastTransations> _paginationCharges;
  PagingController<int, LastTransations> get paginationCharges => _paginationCharges;

  static const _pageSize = 10;

  void init() {
    _paginationSummary.addPageRequestListener(getSummary);
    _paginationCharges.addPageRequestListener(getCharges);
  }

  Future<void> getSummary(int pageKey) async {
    final list = await _balanceUse.balanceReport();
    if (list.message.isNotEmpty) {
      _paginationSummary.error = list.message;
      return;
    }
    list.itemList.length <= _pageSize
        ? _paginationSummary.appendLastPage(list.itemList)
        : _paginationSummary.appendPage(list.itemList, pageKey + 1);
  }

  Future<void> getCharges(int pageKey) async {
    final list = await _balanceUse.lastTransactions();
    if (list.message.isNotEmpty) {
      _paginationCharges.error = list.message;
      return;
    }
    list.itemList.length <= _pageSize
        ? _paginationCharges.appendLastPage(list.itemList)
        : _paginationCharges.appendPage(list.itemList, pageKey + 1);
  }

  Future<void> refresh() async {
    _paginationSummary.refresh();
    _paginationCharges.refresh();
  }
}
