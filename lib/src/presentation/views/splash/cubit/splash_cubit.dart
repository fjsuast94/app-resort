import 'package:appresort/src/data/usecase/auth.dart';
import 'package:bloc/bloc.dart';

part 'splash_state.dart';

class SplashCubit extends Cubit<SplashState> {
  SplashCubit({
    required AuthUseCase auth,
  })  : _auth = auth,
        super(SplashState.none);

  final AuthUseCase _auth;

  Future<void> init() async {
    final showQr = await _auth.showQr();
    if (!showQr) {
      emit(SplashState.none);
      return;
    }
    final qrInitial = await _auth.qrInitial();
    if (!qrInitial) {
      final token = await _auth.tokenIsExpired();
      token ? emit(SplashState.none) : emit(SplashState.none);
      return;
    }
    emit(SplashState.qr);
  }
}
