import 'package:appresort/src/presentation/core/navigator/routes.dart';
import 'package:appresort/src/presentation/core/themes/responsive.dart';
import 'package:appresort/src/presentation/views/splash/cubit/splash_cubit.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';

class SplashView extends StatelessWidget {
  static const String routeName = '/';
  const SplashView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final responsive = Responsive.of(context);
    return BlocProvider(
      create: (context) => SplashCubit(
        auth: context.read(),
      )..init(),
      child: BlocConsumer<SplashCubit, SplashState>(
        listener: (context, state) {
          switch (state) {
            case SplashState.auth:
              Routes.offAndToNamed('/home');
              break;
            case SplashState.qr:
              Routes.offAndToNamed('/initial');
              break;
            case SplashState.none:
              Routes.offAndToNamed('/login');
              break;
          }
        },
        builder: (context, state) {
          return Scaffold(
            body: Column(
              children: [
                Container(
                  height: responsive.height,
                  decoration: const BoxDecoration(
                    image: DecorationImage(
                      fit: BoxFit.cover,
                      image: AssetImage("assets/images/splash.png"),
                    ),
                  ),
                ),
              ],
            ),
          );
        },
      ),
    );
  }
}
