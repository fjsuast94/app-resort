import 'package:appresort/src/data/model/meeting.dart';
import 'package:appresort/src/data/usecase/reservation.dart';
import 'package:appresort/src/presentation/core/themes/app_colors.dart';
import 'package:appresort/src/presentation/core/utils/helper.dart';
import 'package:appresort/src/presentation/views/reservation/bloc/reservation_bloc.dart';
import 'package:appresort/src/presentation/views/reservation/components/meeting_data_source.dart';
import 'package:appresort/src/presentation/views/reservation/components/reservation_modal_add.dart';
import 'package:appresort/src/presentation/views/reservation/components/reservation_modal_view.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import 'package:syncfusion_flutter_calendar/calendar.dart';

class ReservationView extends StatelessWidget {
  static const String routeName = '/reservation';
  const ReservationView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Reservaciones"),
      ),
      body: BlocProvider<ReservationBloc>(
        create: (context) => ReservationBloc(
            reservationUse: ReservationUseCase(
          http: context.read(),
          storage: context.read(),
        ))
          ..add(ReservationInitial()),
        child: BlocBuilder<ReservationBloc, ReservationState>(
          builder: (context, state) {
            return Stack(
              children: [
                SfCalendar(
                  view: CalendarView.month,
                  dataSource: MeetingDataSource(state.mettings),
                  monthViewSettings: const MonthViewSettings(
                    appointmentDisplayMode: MonthAppointmentDisplayMode.indicator,
                    showAgenda: true,
                  ),
                  todayHighlightColor: AppColors.kPrimaryColor,
                  showNavigationArrow: true,
                  cellBorderColor: AppColors.kPrimaryColor.withOpacity(0.3),
                  headerStyle: const CalendarHeaderStyle(
                    textAlign: TextAlign.center,
                    backgroundColor: AppColors.kPrimaryColor,
                    textStyle: TextStyle(
                      fontSize: 25,
                      decorationColor: Colors.black,
                      fontStyle: FontStyle.normal,
                      letterSpacing: 5,
                      color: Colors.white,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                  onTap: (CalendarTapDetails c) async {
                    final response = await calendarTapped(context, c);
                    if (response) {
                      context.read<ReservationBloc>().add(ReservationInitial());
                      Helper.success(message: "Exito");
                    }
                  },
                  onViewChanged: (e) {},
                  showDatePickerButton: true,
                  appointmentTextStyle: const TextStyle(
                    fontSize: 25,
                    decorationColor: Colors.black,
                    fontStyle: FontStyle.normal,
                    letterSpacing: 5,
                    color: Colors.white,
                    fontWeight: FontWeight.w500,
                  ),
                ),
                state.loading
                    ? Container(
                        width: double.infinity,
                        height: MediaQuery.of(context).size.height,
                        color: Colors.black.withOpacity(0.1),
                        child: Center(
                          child: Card(
                            shadowColor: Colors.transparent,
                            elevation: 0.0,
                            color: Colors.transparent,
                            child: Container(
                              width: 80,
                              height: 80,
                              padding: const EdgeInsets.all(12.0),
                              child: const CircularProgressIndicator(
                                strokeWidth: 3,
                                valueColor: AlwaysStoppedAnimation<Color>(Colors.black),
                              ),
                            ),
                          ),
                        ),
                      )
                    : const SizedBox.shrink(),
              ],
            );
          },
        ),
      ),
    );
  }

  Future<bool> calendarTapped(BuildContext context, CalendarTapDetails details) async {
    final now = DateTime.now();
    final dateCell = details.date;

    if (details.targetElement == CalendarElement.calendarCell) {
      if (now.difference(dateCell!).inDays > 0) return false;

      final bool response = await showDialog(
        context: context,
        barrierDismissible: false,
        barrierColor: Colors.transparent.withOpacity(0.2),
        builder: (BuildContext context) => CupertinoAlertDialog(
          title: const Text("Reservación"),
          content: const Text("¿Deseas hacer una nueva reservación?"),
          actions: <Widget>[
            CupertinoDialogAction(
              isDefaultAction: true,
              child: const Text("Si"),
              onPressed: () async {
                final response = await calendarNew(context, details);
                Navigator.of(context).pop(response);
              },
            ),
            CupertinoDialogAction(
              child: const Text("No"),
              onPressed: () => Navigator.of(context).pop(false),
            )
          ],
        ),
      );
      return response;
    } else {
      await calendarEdit(context, details);
      return false;
    }
  }

  Future<bool> calendarNew(BuildContext context, CalendarTapDetails details) async {
    String dateText = DateFormat('yy-MM-dd').format(details.date!).toString();
    int weekend = getWeekend(DateFormat('EEE').format(details.date!).toString());
    final String exito = await showCupertinoModalBottomSheet(
      context: context,
      backgroundColor: Colors.transparent,
      isDismissible: false,
      builder: (context) => ReservacionesModalAdd(
        title: "Nueva reservación",
        date: dateText,
        weekend: weekend,
      ),
    );

    return exito.isNotEmpty;
  }

  Future<bool> calendarEdit(BuildContext context, CalendarTapDetails details) async {
    if (details.appointments!.isEmpty) return false;
    final MeetingModel appointmentDetails = details.appointments![0];

    String exito = '';
    final now = DateTime.now();
    final dateCell = details.date!;

    if (now.difference(dateCell).inDays > 0) {
      exito = await showCupertinoModalBottomSheet(
        context: context,
        backgroundColor: Colors.white,
        builder: (context) => ReservationModalView(
          reservation: appointmentDetails,
        ),
      );
    } else {
      exito = await showCupertinoModalBottomSheet(
        context: context,
        backgroundColor: Colors.white,
        /* builder: (context) => ReservationModalEdit( */
        builder: (context) => ReservationModalView(
          reservation: appointmentDetails,
        ),
      );
    }

    return exito.isNotEmpty;
  }

  int getWeekend(String day) => ['Sat', 'Sun'].contains(day) ? 1 : 0;
}
