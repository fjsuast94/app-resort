import 'package:appresort/src/data/model/meeting.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ReservationModalEdit extends StatelessWidget {
  const ReservationModalEdit({
    Key? key,
    required this.reservation,
  }) : super(key: key);

  final MeetingModel reservation;

  @override
  Widget build(BuildContext context) {
    return Material(
      child: CupertinoPageScaffold(
        navigationBar: CupertinoNavigationBar(
          leading: const SizedBox.shrink(),
          middle: Text(
            reservation.name,
            style: const TextStyle(
              fontSize: 18,
            ),
          ),
          trailing: IconButton(
            icon: const Icon(Icons.clear),
            onPressed: () => Navigator.of(context).pop(),
          ),
        ),
        child: Container(
          margin: const EdgeInsets.symmetric(
            horizontal: 10,
            vertical: 10,
          ),
          child: ListView(
            shrinkWrap: true,
            children: const <Widget>[],
          ),
        ),
      ),
    );
  }
}
