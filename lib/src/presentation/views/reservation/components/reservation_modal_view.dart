import 'package:appresort/src/data/model/meeting.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ReservationModalView extends StatelessWidget {
  const ReservationModalView({
    Key? key,
    required this.reservation,
  }) : super(key: key);
  final MeetingModel reservation;

  @override
  Widget build(BuildContext context) {
    final String to = reservation.to.toString().split(' ')[1];
    final String from = reservation.from.toString().split(' ')[1];

    return Material(
      child: CupertinoPageScaffold(
        navigationBar: CupertinoNavigationBar(
          leading: const SizedBox.shrink(),
          middle: Text(
            reservation.name,
            style: const TextStyle(
              fontSize: 15,
            ),
          ),
          trailing: IconButton(
            icon: const Icon(Icons.clear),
            onPressed: () => Navigator.of(context).pop(),
          ),
        ),
        child: Container(
          margin: const EdgeInsets.symmetric(
            horizontal: 10,
            vertical: 10,
          ),
          child: ListView(
            shrinkWrap: true,
            physics: const NeverScrollableScrollPhysics(),
            children: <Widget>[
              Text(
                "Reservación ${reservation.name}",
                style: const TextStyle(
                  fontSize: 16,
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              const Text(
                "Horario de reservación.",
                style: TextStyle(
                  fontSize: 16,
                ),
              ),
              Text(
                "Desde $from hasta $to hrs.",
                style: const TextStyle(
                  fontSize: 15,
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              const Text(
                "Comentarios:",
                style: TextStyle(
                  fontSize: 17,
                ),
              ),
              Text(
                reservation.descripcion,
                style: const TextStyle(
                  fontSize: 15,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
