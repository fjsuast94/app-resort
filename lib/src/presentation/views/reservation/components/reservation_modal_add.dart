import 'package:appresort/src/data/usecase/reservation.dart';
import 'package:appresort/src/presentation/core/utils/helper.dart';
import 'package:appresort/src/presentation/views/reservation/bloc/reservation_add_bloc.dart';
import 'package:appresort/src/presentation/widgets/buttons/button_submit.dart';
import 'package:appresort/src/presentation/widgets/forms/input_cupertino_bloc.dart';
import 'package:appresort/src/presentation/widgets/forms/select_bloc_builder.dart';
import 'package:appresort/src/presentation/widgets/forms/un_focus_form.dart';
import 'package:appresort/src/presentation/widgets/loading/loading_apleeks.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';

class ReservacionesModalAdd extends StatelessWidget {
  const ReservacionesModalAdd({
    Key? key,
    this.title = '',
    this.date = '',
    this.weekend = 0,
  }) : super(key: key);

  final String title;
  final String date;
  final int weekend;

  @override
  Widget build(BuildContext context) {
    return UnFocusForm(
      child: Material(
        child: CupertinoPageScaffold(
          navigationBar: CupertinoNavigationBar(
            leading: const SizedBox.shrink(),
            middle: Text(title),
            trailing: IconButton(
              icon: const Icon(Icons.clear),
              onPressed: () => Navigator.of(context).pop(),
            ),
          ),
          child: BlocProvider(
            create: (context) => ReservationAddBloc(
              date: date,
              weekend: weekend,
              socket: context.read(),
              notificationUse: context.read(),
              reservationUse: ReservationUseCase(
                http: context.read(),
                storage: context.read(),
              ),
            ),
            child: Builder(
              builder: (context) {
                final bloc = BlocProvider.of<ReservationAddBloc>(context);
                return FormBlocListener<ReservationAddBloc, String, String>(
                  onSubmitting: (context, state) => LoadingApleeks.show(context),
                  onSuccess: (context, state) {
                    LoadingApleeks.hide(context);
                    Navigator.of(context).pop(state.successResponse);
                    bloc.close();
                  },
                  onFailure: (context, state) async {
                    LoadingApleeks.hide(context);
                    Helper.error(message: state.failureResponse!);
                  },
                  child: Container(
                    margin: const EdgeInsets.symmetric(
                      horizontal: 10,
                      vertical: 10,
                    ),
                    child: ListView(
                      shrinkWrap: true,
                      children: <Widget>[
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Expanded(
                              child: SelectBlocBuilder<String>(
                                label: 'Selecciones una áreas',
                                selectFieldBloc: bloc.areas,
                                itemBuilder: (context, value) => value,
                                onChanged: (s) {
                                  bloc.getHorarios(s);
                                },
                              ),
                            ),
                            const SizedBox(width: 10),
                            Expanded(
                              child: SelectBlocBuilder<String>(
                                label: 'Selecciones un horario',
                                selectFieldBloc: bloc.horarios,
                                itemBuilder: (context, value) => value,
                                onChanged: (s) {
                                  bloc.cupoDisponible(s);
                                },
                              ),
                            ),
                          ],
                        ),
                        SelectBlocBuilder<String>(
                          label: 'Seleccione una unidad',
                          selectFieldBloc: bloc.unidades,
                          itemBuilder: (context, value) => value,
                          onChanged: (s) {},
                        ),
                        StreamBuilder<int>(
                          stream: bloc.getOcupacionMax,
                          initialData: 0,
                          builder: (context, data) {
                            final ocupacion = data.data;
                            return ocupacion! > 0
                                ? RichText(
                                    text: TextSpan(
                                      text: 'Cupo disponible ',
                                      style: DefaultTextStyle.of(context).style,
                                      children: <TextSpan>[
                                        TextSpan(
                                          text: '$ocupacion',
                                          style: const TextStyle(
                                            fontWeight: FontWeight.bold,
                                          ),
                                        ),
                                      ],
                                    ),
                                  )
                                : const SizedBox.shrink();
                          },
                        ),
                        InputCupertinoBloc(
                          label: const Text("Núm. de personas"),
                          textFieldBloc: bloc.numeroPersonas,
                          inputColor: Colors.transparent,
                        ),
                        InputCupertinoBloc(
                          label: const Text("Comentarios adicionales"),
                          textFieldBloc: bloc.comentarios,
                          inputColor: Colors.transparent,
                        ),
                        ButtonSubmit(
                          submit: bloc.submit,
                          text: "Aceptar",
                        )
                      ],
                    ),
                  ),
                );
              },
            ),
          ),
        ),
      ),
    );
  }
}
