import 'dart:async';

import 'package:appresort/src/data/model/meeting.dart';
import 'package:appresort/src/data/usecase/reservation.dart';
import 'package:bloc/bloc.dart';

part 'reservation_event.dart';
part 'reservation_state.dart';

class ReservationBloc extends Bloc<ReservationEvent, ReservationState> {
  ReservationBloc({
    required this.reservationUse,
  }) : super(
          ReservationState(
            loading: true,
            mettings: List<MeetingModel>.empty(),
          ),
        );

  final ReservationUseCase reservationUse;

  @override
  Stream<ReservationState> mapEventToState(
    ReservationEvent event,
  ) async* {
    if (event is ReservationInitial) {
      final metting = await reservationUse.reservaciones();
      yield state.withCopy(mettings: metting, loading: false);
    }
  }
}
