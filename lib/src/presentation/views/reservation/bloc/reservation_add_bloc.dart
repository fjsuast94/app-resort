import 'package:appresort/src/data/model/select.dart';
import 'package:appresort/src/data/repository/remote/socket/socket_controller.dart';
import 'package:appresort/src/data/usecase/notification.dart';
import 'package:appresort/src/data/usecase/reservation.dart';
import 'package:appresort/src/presentation/core/utils/validator_string.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:rxdart/subjects.dart';

class ReservationAddBloc extends FormBloc<String, String> {
  final String date;
  final int weekend;
  final NotificationUseCase notificationUse;
  final ReservationUseCase reservationUse;
  final SocketController socket;

  // Elementos fuera del form
  final BehaviorSubject<int> _ocupacionMax = BehaviorSubject<int>();

  Function(int) get addOcupacionMax => _ocupacionMax.sink.add;
  Stream<int> get getOcupacionMax => _ocupacionMax.stream;

  List<SelectModel> listAreas = <SelectModel>[];
  List<SelectModel> listHorarios = <SelectModel>[];
  List<SelectModel> listUnidades = <SelectModel>[];

  // Elementos del form
  final numeroPersonas = TextFieldBloc<Object>(
    initialValue: '1',
    validators: [ValidatorString.required],
  );

  final comentarios = TextFieldBloc<Object>();

  final areas = SelectFieldBloc<String, Object>(
    validators: [ValidatorString.required],
    items: [],
  );

  final horarios = SelectFieldBloc<String, Object>(
    validators: [ValidatorString.required],
    items: [],
  );

  final unidades = SelectFieldBloc<String, Object>(
    validators: [ValidatorString.required],
    items: [],
  );

  ReservationAddBloc({
    required this.date,
    required this.weekend,
    required this.notificationUse,
    required this.reservationUse,
    required this.socket,
  }) {
    socket.connect();
    getAreas();
    getUnidades();
    addFieldBlocs(
      fieldBlocs: [
        numeroPersonas,
        areas,
        horarios,
        comentarios,
        unidades,
      ],
    );
  }

  @override
  Future<void> close() {
    socket.disconnect();
    return super.close();
  }

  void failure(String message) {
    emitFailure(
      failureResponse: message,
    );
  }

  @override
  void onSubmitting() async {
    final unidad = listUnidades.where((element) => element.name == unidades.value).first.id;

    final response = await reservationUse.addReservation(
      idarea: listAreas.where((element) => element.name == areas.value).first.id,
      idhorario: listHorarios.where((element) => element.name == horarios.value).first.id,
      unidad: unidad,
      cantidad: numeroPersonas.valueToInt!,
      descripcion: comentarios.value!,
      fecha: date,
    );

    notificationUse.store(
      modo: 1,
      title: 'Nueva reservación',
      subtitle: 'Se realizó una reservación',
      description:
          'Se ha resalizado una nueva reservación de ${numeroPersonas.valueToInt} persona(s) en el area ${areas.value} el $date',
      iduser: 0,
      idNotifiactionType: 1,
    );
    notificationUse.store(
      modo: 1,
      title: 'Nueva reservación',
      subtitle: 'Se realizó una reservación',
      description:
          'Se ha resalizado una nueva reservación de ${numeroPersonas.valueToInt} persona(s) en el area ${areas.value} el $date',
      iduser: unidad,
      idNotifiactionType: 1,
    );

    notificationUse.storeApp(
      modo: 3,
      title: 'Nueva reservación',
      subtitle: 'Se realizó una reservación',
      description:
          'Se ha resalizado una nueva reservación de ${numeroPersonas.valueToInt} persona(s) en el area ${areas.value} el $date',
      receiver: unidad,
    );

    socket.emitEvent("");

    response.status
        ? emitSuccess(successResponse: response.message)
        : emitFailure(failureResponse: response.message);
  }

  Future<void> getAreas() async {
    listAreas = await reservationUse.listAreas();
    final items = listAreas.map((e) => e.name).toList();
    areas.updateItems(items);
    if (items.isNotEmpty) {
      areas.updateInitialValue(items.first);
      getHorarios(items.first);
    }
    addOcupacionMax(0);
  }

  Future<void> getUnidades() async {
    listUnidades = await reservationUse.listUnidades();
    final items = listUnidades.map((e) => e.name).toList();
    unidades.updateItems(items);
    if (items.isNotEmpty) {
      unidades.updateInitialValue(items.first);
    }
  }

  Future<void> getHorarios(String name) async {
    addOcupacionMax(0);
    int idArea = listAreas.where((element) => element.name == name).first.id;
    listHorarios = await reservationUse.listHorarios(
      idarea: idArea,
      finSemana: weekend,
    );
    final items = listHorarios.map((e) => e.name).toList();
    horarios.updateItems(items);
    if (items.isNotEmpty) {
      horarios.updateInitialValue(items.first);
      cupoDisponible(items.first);
    }
  }

  Future<void> cupoDisponible(String name) async {
    int idHorario = listHorarios.where((element) => element.name == name).first.id;
    final ocupacion = await reservationUse.validarCupo(date, idHorario);
    addOcupacionMax(ocupacion);
  }
}
