part of 'reservation_bloc.dart';

class ReservationState {
  ReservationState({
    this.mettings = const [],
    this.loading = false,
  });

  final bool loading;
  final List<MeetingModel> mettings;

  ReservationState withCopy({
    bool? loading,
    List<MeetingModel>? mettings,
  }) =>
      ReservationState(
        loading: loading ?? this.loading,
        mettings: mettings ?? this.mettings,
      );
}
