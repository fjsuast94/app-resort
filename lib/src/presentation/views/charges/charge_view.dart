import 'package:appresort/src/data/model/charges_model.dart';
import 'package:appresort/src/data/usecase/balance.dart';
import 'package:appresort/src/presentation/views/charges/components/card_charge.dart';
import 'package:appresort/src/presentation/views/charges/cubit/charge_cubit.dart';
import 'package:appresort/src/presentation/widgets/cards/card_refresh.dart';
import 'package:appresort/src/presentation/widgets/sliver/refresh_sliver_scroll.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';

class ChargeView extends StatelessWidget {
  static const String routeName = '/charge';
  const ChargeView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Mis cargos"),
      ),
      body: BlocProvider(
        create: (context) => ChargeCubit(
          pagingCharges: PagingController<int, ChargesModel>(firstPageKey: 1),
          balanceUse: BalanceUseCase(
            http: context.read(),
            storage: context.read(),
          ),
        )..init(),
        child: BlocBuilder<ChargeCubit, ChargeState>(
          builder: (context, state) {
            final paginationController = context.read<ChargeCubit>().pagingCharges;
            return RefreshSliverScrollView(
              onRefresh: () => paginationController.refresh(),
              slivers: [
                const SliverToBoxAdapter(child: SizedBox(height: 0)),
                PagedSliverList.separated(
                  pagingController: paginationController,
                  builderDelegate: PagedChildBuilderDelegate<ChargesModel>(
                    itemBuilder: (context, charge, index) => CardCharge(
                      charge: charge,
                    ),
                    firstPageErrorIndicatorBuilder: (context) => CardRefresh(
                      title: Text("${paginationController.error}"),
                    ),
                    noItemsFoundIndicatorBuilder: (context) => const CardRefresh(
                      title: Text("No hay cargos pendientes"),
                    ),
                  ),
                  separatorBuilder: (context, index) => const SizedBox(
                    height: 0,
                  ),
                ),
              ],
            );
          },
        ),
      ),
    );
  }
}
