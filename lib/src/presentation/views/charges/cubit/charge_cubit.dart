import 'package:appresort/src/data/model/charges_model.dart';
import 'package:appresort/src/data/usecase/balance.dart';
import 'package:bloc/bloc.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';

part 'charge_state.dart';

class ChargeCubit extends Cubit<ChargeState> {
  ChargeCubit({
    required this.pagingCharges,
    required BalanceUseCase balanceUse,
  })  : _balanceUse = balanceUse,
        super(ChargeState());

  final PagingController<int, ChargesModel> pagingCharges;
  final BalanceUseCase _balanceUse;
  static const _pageSize = 10;

  void init() {
    pagingCharges.addPageRequestListener(getCharges);
  }

  Future<void> getCharges(int pageKey) async {
    final list = await _balanceUse.getCharges(
      page: pageKey,
    );
    if (list.message.isNotEmpty) {
      pagingCharges.error = list.message;
      return;
    }
    list.itemList.length <= _pageSize
        ? pagingCharges.appendLastPage(list.itemList)
        : pagingCharges.appendPage(list.itemList, pageKey + 1);
  }
}
