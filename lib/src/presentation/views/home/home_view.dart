import 'package:appresort/src/data/usecase/balance.dart';
import 'package:appresort/src/presentation/views/home/components/home_app_bar.dart';
import 'package:appresort/src/presentation/views/home/components/home_body.dart';
import 'package:appresort/src/presentation/views/home/cubit/home_cubit.dart';
import 'package:appresort/src/presentation/widgets/sliver/refresh_scroll_view.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class HomeView extends StatelessWidget {
  static const String routeName = '/home';
  const HomeView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => HomeCubit(
        balance: BalanceUseCase(
          http: context.read(),
          storage: context.read(),
        ),
      )..init(),
      child: Scaffold(
        body: SafeArea(
          child: BlocBuilder<HomeCubit, HomeState>(
            builder: (context, state) {
              if (state.loading) {
                return const Center(
                  child: Text("Cargando datos"),
                );
              }
              return RefreshScrollView(
                onRefresh: () => context.read<HomeCubit>().reloadCharges(),
                child: Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: const [
                      HomeAppBar(),
                      HomeBody(),
                    ],
                  ),
                ),
              );
            },
          ),
        ),
      ),
    );
  }
}
