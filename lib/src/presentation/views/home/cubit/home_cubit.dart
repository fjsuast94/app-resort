import 'package:appresort/src/data/model/actions.dart';
import 'package:appresort/src/data/model/balance_total.dart';
import 'package:appresort/src/data/model/user.dart';
import 'package:appresort/src/data/usecase/balance.dart';
import 'package:bloc/bloc.dart';

part 'home_state.dart';

class HomeCubit extends Cubit<HomeState> {
  HomeCubit({
    required BalanceUseCase balance,
  })  : _balance = balance,
        super(HomeState());

  final BalanceUseCase _balance;

  Future<void> init() async {
    final user = _balance.getUser();
    emit(state.copyWith(user: user));
    final balance = await _balance.balances();
    final actions = await _balance.getActions();

    emit(state.copyWith(
      loading: false,
      loadingBalance: false,
      saldo: balance,
      actions: actions,
    ));
  }

  Future<void> reloadCharges() async {
    emit(state.copyWith(loadingBalance: true));
    final balance = await _balance.balances();
    final actions = await _balance.getActions();
    emit(state.copyWith(loadingBalance: false, saldo: balance, actions: actions));
  }
}
