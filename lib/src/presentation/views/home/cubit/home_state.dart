part of 'home_cubit.dart';

class HomeState {
  HomeState({
    this.loading = true,
    this.loadingBalance = true,
    this.actions = const [],
    this.user,
    this.saldo,
  });
  final bool loading;
  final bool loadingBalance;
  final UserModel? user;
  final BalanceTotalModel? saldo;
  final List<ActionsModel> actions;

  HomeState copyWith({
    bool? loading,
    bool? loadingBalance,
    UserModel? user,
    BalanceTotalModel? saldo,
    List<ActionsModel>? actions,
  }) =>
      HomeState(
        loading: loading ?? this.loading,
        loadingBalance: loadingBalance ?? this.loadingBalance,
        user: user ?? this.user,
        saldo: saldo ?? this.saldo,
        actions: actions ?? this.actions,
      );
}
