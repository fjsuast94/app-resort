import 'package:appresort/src/presentation/core/navigator/routes.dart';
import 'package:appresort/src/presentation/core/themes/responsive.dart';
import 'package:appresort/src/presentation/views/home/cubit/home_cubit.dart';
import 'package:appresort/src/presentation/widgets/buttons/circle_button.dart';
import 'package:appresort/src/presentation/widgets/icons/avatar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class HomeAppBar extends StatelessWidget {
  const HomeAppBar({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final responsive = Responsive.of(context);

    return BlocBuilder<HomeCubit, HomeState>(
      builder: (context, state) {
        final user = state.user;
        return Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  "Bienvenido de nuevo",
                  style: TextStyle(
                    fontSize: responsive.inchPercent(2.2),
                    color: Colors.black45,
                  ),
                ),
                const SizedBox(height: 5),
                Text(
                  user?.name ?? '',
                  style: TextStyle(
                    fontSize: responsive.inchPercent(2),
                    color: Colors.black.withOpacity(0.7),
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ],
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Stack(
                  children: [
                    CircleButton(
                      callback: () => Routes.toNamed('/notification'),
                      icon: FaIcon(
                        FontAwesomeIcons.solidBell,
                        size: responsive.widthPercent(4.2),
                      ),
                      backgroundColor: Colors.white,
                      height: responsive.widthPercent(8.5),
                      width: responsive.widthPercent(8.5),
                    ),
                    Positioned(
                      right: 6,
                      top: 4,
                      child: Container(
                        height: responsive.heightPercent(0.9),
                        width: responsive.heightPercent(0.9),
                        decoration: const BoxDecoration(
                          color: Color(0xffee305e),
                          shape: BoxShape.circle,
                        ),
                      ),
                    )
                  ],
                ),
                const SizedBox(width: 20),
                ClipOval(
                  child: Hero(
                    tag: user!.photo,
                    child: AvatarIcon(
                      profileImage: user.photo,
                      marginColor: Colors.transparent,
                      height: responsive.widthPercent(8.5),
                      width: responsive.widthPercent(8.5),
                      child: Material(
                        color: Colors.transparent,
                        child: InkWell(
                          onTap: () => Routes.toNamed('/settings'),
                          child: const SizedBox(),
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            )
          ],
        );
      },
    );
  }
}
