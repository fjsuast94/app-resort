import 'package:appresort/src/presentation/core/navigator/routes.dart';
import 'package:appresort/src/presentation/core/themes/app_colors.dart';
import 'package:appresort/src/presentation/core/themes/responsive.dart';
import 'package:appresort/src/presentation/core/utils/helper.dart';
import 'package:appresort/src/presentation/views/home/cubit/home_cubit.dart';
import 'package:appresort/src/presentation/widgets/cards/card_action.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class HomeBody extends StatelessWidget {
  const HomeBody({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final bloc = context.watch<HomeCubit>();
    final responsive = Responsive.of(context);
    final spacing = responsive.width > 380 ? 25.0 : 10.0;
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          const _CardBalance(),
          Container(
            margin: const EdgeInsets.symmetric(
              vertical: 20,
            ),
            child: const Text(
              "Todo los servicios",
              style: TextStyle(
                fontWeight: FontWeight.bold,
                color: Colors.black45,
              ),
            ),
          ),
          Wrap(
            alignment: WrapAlignment.start,
            spacing: spacing,
            runSpacing: 10.0,
            children: List.generate(bloc.state.actions.length, (index) {
              final action = bloc.state.actions[index];
              return CardAction(
                action: action,
                onTap: () => Routes.toNamed(
                  action.page,
                  message: "Página en construcción",
                ),
              );
            }),
          ),
        ],
      ),
    );
  }
}

class _CardBalance extends StatelessWidget {
  const _CardBalance({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final bloc = context.watch<HomeCubit>();
    final responsive = Responsive.of(context);
    return Container(
      width: double.infinity,
      decoration: BoxDecoration(
        borderRadius: const BorderRadius.all(
          Radius.circular(10.0),
        ),
        gradient: LinearGradient(
          colors: [
            AppColors.kPrimaryColor,
            AppColors.kPrimaryColor.withOpacity(0.9),
          ],
          begin: Alignment.centerLeft,
          end: Alignment.centerRight,
          tileMode: TileMode.clamp,
        ),
        boxShadow: [
          BoxShadow(
            blurRadius: 10.0,
            color: const Color(0xFF3C60F8).withOpacity(.2),
          ),
        ],
      ),
      child: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      "Saldos",
                      style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                        fontSize: responsive.inchPercent(2.5),
                      ),
                    ),
                    const SizedBox(
                      width: 60,
                    ),
                    Text(
                      "Fondo actual",
                      style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                        fontSize: responsive.inchPercent(2.5),
                      ),
                    ),
                  ],
                ),
                bloc.state.loadingBalance
                    ? const Center(
                        child: CircularProgressIndicator(
                          valueColor:
                              AlwaysStoppedAnimation<Color>(Colors.white),
                          strokeWidth: 2,
                        ),
                      )
                    : Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            Helper.moneyFormat(bloc.state.saldo!.total),
                            style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                              fontSize: responsive.inchPercent(2.5),
                            ),
                          ),
                          const SizedBox(
                            width: 30,
                          ),
                          Text(
                            Helper.moneyFormat(bloc.state.saldo!.fondo),
                            style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                              fontSize: responsive.inchPercent(2.5),
                            ),
                          ),
                        ],
                      )
              ],
            ),
            GestureDetector(
              onTap: () => bloc.reloadCharges(),
              child: const Icon(
                Icons.replay,
                color: Colors.white,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
