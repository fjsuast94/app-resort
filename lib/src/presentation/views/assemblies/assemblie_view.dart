import 'package:appresort/src/data/model/assemblie.dart';
import 'package:appresort/src/data/usecase/assemblie.dart';
import 'package:appresort/src/presentation/core/navigator/routes.dart';
import 'package:appresort/src/presentation/views/assemblies/cubit/assemblie_cubit.dart';
import 'package:appresort/src/presentation/views/assemblies/pages/assemblie_content_view.dart';
import 'package:appresort/src/presentation/widgets/cards/card_refresh.dart';
import 'package:appresort/src/presentation/widgets/sliver/refresh_sliver_scroll.dart';
import 'package:flutter/material.dart';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';

class AssemblieView extends StatelessWidget {
  static const String routeName = '/assemblie';
  const AssemblieView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Asambleas"),
      ),
      body: BlocProvider(
        create: (_) => AssemblieCubit(
          socket: context.read(),
          pagination: PagingController(firstPageKey: 1),
          assemblieUse: AssemblieUseCase(
            http: context.read(),
            storage: context.read(),
          ),
        )..init(),
        child: BlocBuilder<AssemblieCubit, AssemblieState>(
          builder: (context, state) {
            final cubit = context.read<AssemblieCubit>();
            final pagination = cubit.pagination;
            return RefreshSliverScrollView(
              onRefresh: () => pagination.refresh(),
              slivers: [
                PagedSliverList.separated(
                  pagingController: pagination,
                  builderDelegate: PagedChildBuilderDelegate<AssemblieModel>(
                    itemBuilder: (context, asamblea, index) => Padding(
                      padding: const EdgeInsets.symmetric(
                        horizontal: 10,
                        vertical: 10,
                      ),
                      child: ListTile(
                        tileColor: Theme.of(context).cardColor,
                        title: Text(asamblea.titulo),
                        subtitle: Text("${asamblea.fecha} ${asamblea.hora}"),
                        onTap: () {
                          cubit.emmitState(asamblea);
                          Routes.to(
                            () => BlocProvider.value(
                              value: BlocProvider.of<AssemblieCubit>(context),
                              child: AssemblieContentView(
                                assemblie: asamblea,
                              ),
                            ),
                          );
                        },
                      ),
                    ),
                    firstPageErrorIndicatorBuilder: (context) => CardRefresh(
                      title: Text("${pagination.error}"),
                    ),
                    noItemsFoundIndicatorBuilder: (context) => const CardRefresh(
                      title: Text("No hay asambleas disponibles"),
                    ),
                  ),
                  separatorBuilder: (context, index) => const SizedBox(
                    height: 0,
                  ),
                ),
              ],
            );
          },
        ),
      ),
    );
  }
}
