import 'package:appresort/src/data/model/assemblie_encuesta.dart';
import 'package:appresort/src/data/usecase/assemblie.dart';
import 'package:appresort/src/presentation/core/utils/helper.dart';
import 'package:appresort/src/presentation/views/assemblies/cubit/assemblie_cubit.dart';
import 'package:appresort/src/presentation/views/assemblies/cubit/assemblie_encuesta_cubit.dart';
import 'package:appresort/src/presentation/widgets/cards/card_refresh.dart';
import 'package:appresort/src/presentation/widgets/loading/loading_apleeks.dart';
import 'package:appresort/src/presentation/widgets/sliver/refresh_sliver_scroll.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';

class AssemblieEncuesta extends StatelessWidget {
  const AssemblieEncuesta({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Encuestas"),
      ),
      body: BlocBuilder<AssemblieCubit, AssemblieState>(
        builder: (_, state) {
          return BlocProvider(
            create: (context) => AssemblieEncuestaCubit(
              assemblieUse: AssemblieUseCase(
                http: context.read(),
                storage: context.read(),
              ),
              assemblieCubit: context.read(),
              pagination: PagingController(firstPageKey: 1),
            )..init(),
            child: BlocBuilder<AssemblieEncuestaCubit, int>(
              builder: (context, state) {
                final cubit = context.read<AssemblieEncuestaCubit>();
                return RefreshSliverScrollView(
                  onRefresh: () => cubit.pagination.refresh(),
                  slivers: [
                    PagedSliverList.separated(
                      pagingController: cubit.pagination,
                      builderDelegate: PagedChildBuilderDelegate<AssemblieEncuestaModel>(
                        itemBuilder: (context, encuesta, index) => Padding(
                          padding: const EdgeInsets.symmetric(
                            horizontal: 20,
                            vertical: 20,
                          ),
                          child: ListTile(
                            tileColor: Theme.of(context).cardColor,
                            title: Text(
                              encuesta.pregunta,
                              textAlign: TextAlign.center,
                            ),
                            subtitle: Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                IconButton(
                                  splashColor: Colors.transparent,
                                  icon: const Icon(
                                    Icons.sentiment_dissatisfied,
                                    size: 25,
                                    color: Colors.redAccent,
                                  ),
                                  onPressed: () async {
                                    LoadingApleeks.show(context);
                                    final res = await cubit.votarEncuesta(encuesta.id, 1);
                                    LoadingApleeks.hide(context);
                                    Helper.success(message: res);
                                  },
                                ),
                                IconButton(
                                  splashColor: Colors.transparent,
                                  icon: const Icon(
                                    Icons.sentiment_neutral,
                                    size: 25,
                                    color: Colors.amber,
                                  ),
                                  onPressed: () async {
                                    LoadingApleeks.show(context);
                                    final res = await cubit.votarEncuesta(encuesta.id, 2);
                                    LoadingApleeks.hide(context);
                                    Helper.success(message: res);
                                  },
                                ),
                                IconButton(
                                  splashColor: Colors.transparent,
                                  icon: const Icon(
                                    Icons.sentiment_very_satisfied,
                                    size: 25,
                                    color: Colors.green,
                                  ),
                                  onPressed: () async {
                                    LoadingApleeks.show(context);
                                    final res = await cubit.votarEncuesta(encuesta.id, 3);
                                    LoadingApleeks.hide(context);
                                    Helper.success(message: res);
                                  },
                                )
                              ],
                            ),
                          ),
                        ),
                        firstPageErrorIndicatorBuilder: (context) => CardRefresh(
                          title: Text("${cubit.pagination.error}"),
                        ),
                        noItemsFoundIndicatorBuilder: (context) => const CardRefresh(
                          title: Text("No hay asistencias disponibles"),
                        ),
                      ),
                      separatorBuilder: (context, index) => const SizedBox(
                        height: 20,
                      ),
                    ),
                  ],
                );
              },
            ),
          );
        },
      ),
    );
  }
}
