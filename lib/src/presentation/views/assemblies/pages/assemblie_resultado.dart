import 'package:appresort/src/data/model/assemblie_encuesta.dart';
import 'package:appresort/src/data/usecase/assemblie.dart';
import 'package:appresort/src/presentation/views/assemblies/cubit/assemblie_cubit.dart';
import 'package:appresort/src/presentation/views/assemblies/cubit/assemblie_resultados_cubit.dart';
import 'package:appresort/src/presentation/widgets/cards/card_refresh.dart';
import 'package:appresort/src/presentation/widgets/sliver/refresh_sliver_scroll.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';

class AssemblieResultado extends StatelessWidget {
  const AssemblieResultado({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Resultados"),
      ),
      body: BlocBuilder<AssemblieCubit, AssemblieState>(
        builder: (_, state) {
          return BlocProvider(
            create: (context) => AssemblieResultadoCubit(
              assemblieUse: AssemblieUseCase(
                http: context.read(),
                storage: context.read(),
              ),
              idAsamblea: state.assemblie!.id,
              pagination: PagingController(firstPageKey: 1),
            )..init(),
            child: BlocBuilder<AssemblieResultadoCubit, int>(
              builder: (context, state) {
                final cubit = context.read<AssemblieResultadoCubit>();
                return RefreshSliverScrollView(
                  onRefresh: () => cubit.pagination.refresh(),
                  slivers: [
                    PagedSliverList.separated(
                      pagingController: cubit.pagination,
                      builderDelegate: PagedChildBuilderDelegate<AssemblieEncuestaModel>(
                        itemBuilder: (context, encuesta, index) => Padding(
                          padding: const EdgeInsets.symmetric(
                            horizontal: 20,
                            vertical: 20,
                          ),
                          child: ListTile(
                            tileColor: Theme.of(context).cardColor,
                            title: Text(
                              "Pregunta: ${encuesta.pregunta}",
                              textAlign: TextAlign.left,
                            ),
                            subtitle: Container(
                              margin: const EdgeInsets.symmetric(vertical: 8),
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  const Text("Resultados"),
                                  const SizedBox(
                                    width: 20,
                                  ),
                                  RichText(
                                    textAlign: TextAlign.center,
                                    text: TextSpan(
                                      style: DefaultTextStyle.of(context).style,
                                      children: <TextSpan>[
                                        TextSpan(
                                          text: '${encuesta.votoSi}%',
                                          style: const TextStyle(
                                            fontWeight: FontWeight.bold,
                                            color: Colors.green,
                                          ),
                                        ),
                                        const TextSpan(text: ' / '),
                                        TextSpan(
                                          text: '${encuesta.votoNeutral}%',
                                          style: const TextStyle(
                                            fontWeight: FontWeight.bold,
                                            color: Colors.blue,
                                          ),
                                        ),
                                        const TextSpan(text: ' / '),
                                        TextSpan(
                                          text: '${encuesta.votoNo}%',
                                          style: const TextStyle(
                                            fontWeight: FontWeight.bold,
                                            color: Colors.red,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                        firstPageErrorIndicatorBuilder: (context) => CardRefresh(
                          title: Text("${cubit.pagination.error}"),
                        ),
                        noItemsFoundIndicatorBuilder: (context) => const CardRefresh(
                          title: Text("No hay asistencias disponibles"),
                        ),
                      ),
                      separatorBuilder: (context, index) => const SizedBox(
                        height: 0,
                      ),
                    ),
                  ],
                );
              },
            ),
          );
        },
      ),
    );
  }
}
