import 'package:appresort/src/data/model/assemblie.dart';
import 'package:appresort/src/data/repository/local/storage/storage_repository.dart';
import 'package:appresort/src/presentation/core/navigator/routes.dart';
import 'package:appresort/src/presentation/core/utils/helper.dart';
import 'package:appresort/src/presentation/views/assemblies/cubit/assemblie_cubit.dart';
import 'package:appresort/src/presentation/views/assemblies/pages/assemblie_assistance.dart';
import 'package:appresort/src/presentation/views/assemblies/pages/assemblie_encuesta.dart';
import 'package:appresort/src/presentation/views/assemblies/pages/assemblie_resultado.dart';
import 'package:appresort/src/presentation/widgets/pdf_viewer/pdf_viewer.dart';
import 'package:appresort/src/presentation/widgets/sliver/refresh_scroll_view.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class AssemblieContentView extends StatelessWidget {
  static const String routeName = '/assemblie-content';
  const AssemblieContentView({
    Key? key,
    required this.assemblie,
  }) : super(key: key);

  final AssemblieModel assemblie;

  @override
  Widget build(BuildContext context) {
    final server = context.read<StorageRepository>().server;
    return Scaffold(
      appBar: AppBar(
        title: Text(assemblie.titulo),
      ),
      body: RefreshScrollView(
        child: BlocBuilder<AssemblieCubit, AssemblieState>(
          builder: (context, state) {
            return Container(
              margin: const EdgeInsets.symmetric(
                horizontal: 10,
              ),
              child: Column(
                children: [
                  ListTile(
                    tileColor: Theme.of(context).cardColor,
                    title: const Text("Covocatoria"),
                    onTap: state.assemblie!.ruta.isEmpty
                        ? () => Helper.error(message: "No hay archivo de convocaria")
                        : () => Routes.fullDialog(
                              () => PdfViewer(path: "$server/${state.assemblie!.ruta}"),
                            ),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  ListTile(
                    title: const Text("Asistencia"),
                    tileColor: Theme.of(context).cardColor,
                    onTap: () => Routes.to(
                      BlocProvider.value(
                        value: context.read<AssemblieCubit>(),
                        child: const AssemblieAssistance(),
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  ListTile(
                    title: const Text("Encuesta"),
                    tileColor: Theme.of(context).cardColor,
                    onTap: () => Routes.to(
                      BlocProvider.value(
                        value: context.read<AssemblieCubit>(),
                        child: const AssemblieEncuesta(),
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  ListTile(
                    title: const Text("Resultados"),
                    tileColor: Theme.of(context).cardColor,
                    onTap: () => Routes.to(
                      BlocProvider.value(
                        value: context.read<AssemblieCubit>(),
                        child: const AssemblieResultado(),
                      ),
                    ),
                  ),
                ],
              ),
            );
          },
        ),
      ),
    );
  }
}
