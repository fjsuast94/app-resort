import 'package:appresort/src/data/model/assemblie_assistence.dart';
import 'package:appresort/src/data/usecase/assemblie.dart';
import 'package:appresort/src/presentation/core/utils/helper.dart';
import 'package:appresort/src/presentation/views/assemblies/cubit/assemblie_assistance_cubit.dart';
import 'package:appresort/src/presentation/views/assemblies/cubit/assemblie_cubit.dart';
import 'package:appresort/src/presentation/widgets/cards/card_refresh.dart';
import 'package:appresort/src/presentation/widgets/loading/loading_apleeks.dart';
import 'package:appresort/src/presentation/widgets/sliver/refresh_sliver_scroll.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';

class AssemblieAssistance extends StatelessWidget {
  static const String routeName = '/assemblie-assistance';
  const AssemblieAssistance({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AssemblieCubit, AssemblieState>(
      builder: (_, state) {
        return BlocProvider(
          create: (context) => AssemblieAssistenceCubit(
            assemblieUse: AssemblieUseCase(
              http: context.read(),
              storage: context.read(),
            ),
            assemblieCubit: context.read<AssemblieCubit>(),
            pagination: PagingController(firstPageKey: 1),
          )..init(),
          child: BlocBuilder<AssemblieAssistenceCubit, int>(
            builder: (context, state) {
              final cubitAssemblie = context.read<AssemblieAssistenceCubit>();
              return Scaffold(
                appBar: AppBar(
                  centerTitle: false,
                  title: const Text("Asistencia"),
                  actions: [
                    TextButton(
                      onPressed: () async {
                        LoadingApleeks.show(context);
                        final message = await cubitAssemblie.marcarAssistencia();
                        LoadingApleeks.hide(context);
                        Helper.success(message: message);
                      },
                      child: const Text("Marcar asistencia"),
                    )
                  ],
                ),
                body: RefreshSliverScrollView(
                  onRefresh: () => cubitAssemblie.pagination.refresh(),
                  slivers: [
                    PagedSliverList.separated(
                      pagingController: cubitAssemblie.pagination,
                      builderDelegate: PagedChildBuilderDelegate<AssemblieAssistenceModel>(
                        itemBuilder: (context, asistencia, index) => Padding(
                          padding: const EdgeInsets.symmetric(
                            horizontal: 10,
                            vertical: 10,
                          ),
                          child: ListTile(
                            tileColor: Theme.of(context).cardColor,
                            title: Text('${index + 1}  ${asistencia.condomino}'),
                          ),
                        ),
                        firstPageErrorIndicatorBuilder: (context) => CardRefresh(
                          title: Text("${cubitAssemblie.pagination.error}"),
                        ),
                        noItemsFoundIndicatorBuilder: (context) => const CardRefresh(
                          title: Text("No hay asistencias disponibles"),
                        ),
                      ),
                      separatorBuilder: (context, index) => const SizedBox(
                        height: 0,
                      ),
                    ),
                  ],
                ),
              );
            },
          ),
        );
      },
    );
  }
}
