import 'package:appresort/src/data/model/assemblie_encuesta.dart';
import 'package:appresort/src/data/usecase/assemblie.dart';
import 'package:appresort/src/presentation/views/assemblies/cubit/assemblie_cubit.dart';
import 'package:bloc/bloc.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';

class AssemblieEncuestaCubit extends Cubit<int> {
  AssemblieEncuestaCubit({
    required AssemblieCubit assemblieCubit,
    required PagingController<int, AssemblieEncuestaModel> pagination,
    required AssemblieUseCase assemblieUse,
  })  : _assemblieUse = assemblieUse,
        _pagination = pagination,
        _assemblieCubit = assemblieCubit,
        super(0);

  final AssemblieCubit _assemblieCubit;
  final PagingController<int, AssemblieEncuestaModel> _pagination;
  PagingController<int, AssemblieEncuestaModel> get pagination => _pagination;
  final AssemblieUseCase _assemblieUse;

  static const _pageSize = 100;

  void init() {
    _assemblieCubit.socket.connect();
    pagination.addPageRequestListener(getListAssitence);
  }

  @override
  Future<void> close() {
    _assemblieCubit.socket.disconnect();
    return super.close();
  }

  Future<void> getListAssitence(int pageKey) async {
    final list = await _assemblieUse.listarEncuesta(pageKey, _assemblieCubit.state.assemblie!.id);
    if (list.message.isNotEmpty) {
      _pagination.error = list.message;
      return;
    }
    list.itemList.length <= _pageSize
        ? _pagination.appendLastPage(list.itemList)
        : _pagination.appendPage(list.itemList, pageKey + 1);
  }

  Future<String> votarEncuesta(int idencuesta, int voto) async {
    _assemblieCubit.socket.emitEvent("new vote");
    return _assemblieUse.votarEncuesta(idencuesta, voto);
  }
}
