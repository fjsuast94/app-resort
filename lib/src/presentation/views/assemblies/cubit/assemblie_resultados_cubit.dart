import 'package:appresort/src/data/model/assemblie_encuesta.dart';
import 'package:appresort/src/data/usecase/assemblie.dart';
import 'package:bloc/bloc.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';

class AssemblieResultadoCubit extends Cubit<int> {
  AssemblieResultadoCubit({
    required this.idAsamblea,
    required PagingController<int, AssemblieEncuestaModel> pagination,
    required AssemblieUseCase assemblieUse,
  })  : _assemblieUse = assemblieUse,
        _pagination = pagination,
        super(0);

  final int idAsamblea;
  final PagingController<int, AssemblieEncuestaModel> _pagination;
  PagingController<int, AssemblieEncuestaModel> get pagination => _pagination;
  final AssemblieUseCase _assemblieUse;

  static const _pageSize = 100;

  void init() {
    pagination.addPageRequestListener(getListAssitence);
  }

  Future<void> getListAssitence(int pageKey) async {
    final list = await _assemblieUse.listarEncuesta(pageKey, idAsamblea);
    if (list.message.isNotEmpty) {
      _pagination.error = list.message;
      return;
    }
    list.itemList.length <= _pageSize
        ? _pagination.appendLastPage(list.itemList)
        : _pagination.appendPage(list.itemList, pageKey + 1);
  }

  Future<String> votarEncuesta(int idencuesta, int voto) async {
    return _assemblieUse.votarEncuesta(idencuesta, voto);
  }
}
