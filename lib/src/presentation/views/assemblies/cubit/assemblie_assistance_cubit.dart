import 'package:appresort/src/data/model/assemblie_assistence.dart';
import 'package:appresort/src/data/usecase/assemblie.dart';
import 'package:appresort/src/presentation/views/assemblies/cubit/assemblie_cubit.dart';
import 'package:bloc/bloc.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';

class AssemblieAssistenceCubit extends Cubit<int> {
  AssemblieAssistenceCubit({
    required AssemblieCubit assemblieCubit,
    required PagingController<int, AssemblieAssistenceModel> pagination,
    required AssemblieUseCase assemblieUse,
  })  : _assemblieCubit = assemblieCubit,
        _assemblieUse = assemblieUse,
        _pagination = pagination,
        super(0);

  final AssemblieCubit _assemblieCubit;
  final PagingController<int, AssemblieAssistenceModel> _pagination;
  PagingController<int, AssemblieAssistenceModel> get pagination => _pagination;
  final AssemblieUseCase _assemblieUse;

  static const _pageSize = 100;

  void init() {
    _assemblieCubit.socket.connect();
    _pagination.addPageRequestListener(getListAssitence);
  }

  @override
  Future<void> close() {
    _assemblieCubit.socket.disconnect();
    return super.close();
  }

  Future<void> getListAssitence(int pageKey) async {
    final list = await _assemblieUse.listarAsistentes(pageKey, _assemblieCubit.state.assemblie!.id);
    if (list.message.isNotEmpty) {
      _pagination.error = list.message;
      return;
    }
    list.itemList.length <= _pageSize
        ? _pagination.appendLastPage(list.itemList)
        : _pagination.appendPage(list.itemList, pageKey + 1);
  }

  Future<String> marcarAssistencia() async {
    _assemblieCubit.socket.emitEvent("new assistance");
    return _assemblieUse.registerAssistence(_assemblieCubit.state.assemblie!.id);
  }
}
