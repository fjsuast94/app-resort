import 'package:appresort/src/data/model/assemblie.dart';
import 'package:appresort/src/data/repository/remote/socket/socket_controller.dart';
import 'package:appresort/src/data/usecase/assemblie.dart';
import 'package:bloc/bloc.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';

part 'assemblie_state.dart';

class AssemblieCubit extends Cubit<AssemblieState> {
  AssemblieCubit({
    required PagingController<int, AssemblieModel> pagination,
    required AssemblieUseCase assemblieUse,
    required SocketController socket,
  })  : _assemblieUse = assemblieUse,
        _pagination = pagination,
        _socket = socket,
        super(AssemblieState());

  final PagingController<int, AssemblieModel> _pagination;
  PagingController<int, AssemblieModel> get pagination => _pagination;

  final AssemblieUseCase _assemblieUse;
  final SocketController _socket;
  SocketController get socket => _socket;

  static const _pageSize = 100;

  void init() {
    _socket.connect();
    pagination.addPageRequestListener(getAssemblies);
  }

  @override
  Future<void> close() {
    _socket.disconnect();
    return super.close();
  }

  Future<void> getAssemblies(int pageKey) async {
    final list = await _assemblieUse.assemblies(pageKey);
    if (list.message.isNotEmpty) {
      _pagination.error = list.message;
      return;
    }
    list.itemList.length <= _pageSize
        ? _pagination.appendLastPage(list.itemList)
        : _pagination.appendPage(list.itemList, pageKey + 1);
  }

  void emmitState(AssemblieModel assemblie) {
    emit(AssemblieState(assemblie: assemblie));
  }
}
