import 'package:appresort/src/data/usecase/balance.dart';
import 'package:appresort/src/presentation/core/navigator/routes.dart';
import 'package:appresort/src/presentation/core/utils/helper.dart';
import 'package:appresort/src/presentation/views/estado_cuenta/bloc/estado_cuenta_bloc.dart';
import 'package:appresort/src/presentation/widgets/buttons/button_submit.dart';
import 'package:appresort/src/presentation/widgets/forms/dropdown_bloc.dart';
import 'package:appresort/src/presentation/widgets/loading/loading_apleeks.dart';
import 'package:appresort/src/presentation/widgets/pdf_viewer/pdf_viewer.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';

class EstadoCuentaView extends StatelessWidget {
  static const String routeName = '/estado-cuenta';
  const EstadoCuentaView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Estado de cuenta"),
      ),
      body: SingleChildScrollView(
        child: Container(
          margin: const EdgeInsets.symmetric(
            horizontal: 10,
            vertical: 10,
          ),
          child: BlocProvider(
            create: (context) => EstadoCuentaBloc(
              balanceUse: BalanceUseCase(
                http: context.read(),
                storage: context.read(),
              ),
            ),
            child: Builder(builder: (context) {
              // ignore: close_sinks
              final cuenta = BlocProvider.of<EstadoCuentaBloc>(context);
              return FormBlocListener<EstadoCuentaBloc, String, String>(
                onSubmitting: (context, state) => LoadingApleeks.show(context),
                onSuccess: (context, state) async {
                  LoadingApleeks.hide(context);
                  Routes.fullDialog(
                    () => PdfViewer(
                      path: state.successResponse!,
                      title: cuenta.selectOpcion.value!,
                    ),
                  );
                },
                onFailure: (context, state) async {
                  LoadingApleeks.hide(context);
                  Helper.error(message: state.failureResponse!);
                },
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    DropdownBloc(
                      label: 'Seleccione una opción',
                      selectFieldBloc: cuenta.selectOpcion,
                      itemBuilder: (context, value) => value,
                      onChanged: (s) {},
                      mode: Mode.BOTTOM_SHEET,
                    ),
                    DropdownBloc(
                      label: 'Movimientos de los últimos',
                      selectFieldBloc: cuenta.lastDays,
                      itemBuilder: (context, value) => value,
                      onChanged: (s) {},
                      mode: Mode.BOTTOM_SHEET,
                      margin: const EdgeInsets.symmetric(
                        vertical: 10,
                      ),
                    ),
                    DropdownBloc(
                      label: 'Mes',
                      selectFieldBloc: cuenta.months,
                      itemBuilder: (context, value) => value,
                      onChanged: (s) {},
                      mode: Mode.BOTTOM_SHEET,
                      margin: const EdgeInsets.symmetric(
                        vertical: 10,
                      ),
                    ),
                    DropdownBloc(
                      label: 'Año',
                      selectFieldBloc: cuenta.years,
                      itemBuilder: (context, value) => value,
                      onChanged: (s) {},
                      mode: Mode.BOTTOM_SHEET,
                      margin: const EdgeInsets.symmetric(
                        vertical: 10,
                      ),
                    ),
                    ButtonSubmit(
                      submit: cuenta.submit,
                      text: 'Aceptar',
                    ),
                  ],
                ),
              );
            }),
          ),
        ),
      ),
    );
  }
}
