import 'package:appresort/src/data/model/regulation.dart';
import 'package:appresort/src/data/usecase/regulation.dart';
import 'package:appresort/src/presentation/core/navigator/routes.dart';
import 'package:appresort/src/presentation/views/regulations/cubit/regulation_cubit.dart';
import 'package:appresort/src/presentation/widgets/cards/card_refresh.dart';
import 'package:appresort/src/presentation/widgets/pdf_viewer/pdf_viewer.dart';
import 'package:appresort/src/presentation/widgets/sliver/refresh_sliver_scroll.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';

class RegulationView extends StatelessWidget {
  static const String routeName = '/regulation';
  const RegulationView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Regalmentos"),
      ),
      body: BlocProvider(
        create: (context) => RegulationCubit(
          pagination: PagingController(firstPageKey: 1),
          regulationUse: RegulationUseCase(
            http: context.read(),
            storage: context.read(),
          ),
        )..init(),
        child: BlocBuilder<RegulationCubit, int>(
          builder: (context, state) {
            final paginationController = context.read<RegulationCubit>().pagination;
            return RefreshSliverScrollView(
              onRefresh: () => context.read<RegulationCubit>().pagination.refresh(),
              slivers: [
                const SliverToBoxAdapter(child: SizedBox(height: 0)),
                PagedSliverList.separated(
                  pagingController: paginationController,
                  builderDelegate: PagedChildBuilderDelegate<RegulationModel>(
                    itemBuilder: (context, regulation, index) => Padding(
                      padding: const EdgeInsets.symmetric(
                        horizontal: 10,
                        vertical: 10,
                      ),
                      child: ListTile(
                        tileColor: Colors.white,
                        title: Text(regulation.nombre),
                        shape: const RoundedRectangleBorder(
                          borderRadius: BorderRadius.all(
                            Radius.circular(10),
                          ),
                        ),
                        trailing: const Icon(Icons.arrow_forward_outlined),
                        onTap: () => Routes.fullDialog(
                          () => PdfViewer(
                            path: regulation.ruta,
                            title: regulation.nombre,
                            network: true,
                          ),
                        ),
                      ),
                    ),
                    firstPageErrorIndicatorBuilder: (context) => CardRefresh(
                      title: Text("${paginationController.error}"),
                    ),
                    noItemsFoundIndicatorBuilder: (context) => const CardRefresh(
                      title: Text("No hay  reglamentos disponibles"),
                    ),
                  ),
                  separatorBuilder: (context, index) => const SizedBox(
                    height: 0,
                  ),
                ),
              ],
            );
          },
        ),
      ),
    );
  }
}
