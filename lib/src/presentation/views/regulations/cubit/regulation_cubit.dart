import 'package:appresort/src/data/model/regulation.dart';
import 'package:appresort/src/data/usecase/regulation.dart';
import 'package:bloc/bloc.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';

class RegulationCubit extends Cubit<int> {
  RegulationCubit({
    required PagingController<int, RegulationModel> pagination,
    required RegulationUseCase regulationUse,
  })  : _regulationUse = regulationUse,
        _pagination = pagination,
        super(0);

  final PagingController<int, RegulationModel> _pagination;
  PagingController<int, RegulationModel> get pagination => _pagination;
  static const _pageSize = 10;

  final RegulationUseCase _regulationUse;

  void init() {
    pagination.addPageRequestListener(getRegulations);
  }

  Future<void> getRegulations(int pageKey) async {
    final list = await _regulationUse.regulations(pageKey);
    if (list.message.isNotEmpty) {
      _pagination.error = list.message;
      return;
    }
    list.itemList.length <= _pageSize
        ? _pagination.appendLastPage(list.itemList)
        : _pagination.appendPage(list.itemList, pageKey + 1);
  }
}
