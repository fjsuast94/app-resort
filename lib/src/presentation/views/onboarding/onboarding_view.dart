import 'package:appresort/src/presentation/core/navigator/routes.dart';
import 'package:appresort/src/presentation/core/themes/app_colors.dart';
import 'package:appresort/src/presentation/views/onboarding/components/on_boarding_content.dart';
import 'package:appresort/src/presentation/views/onboarding/components/on_boarding_stepper.dart';
import 'package:appresort/src/presentation/views/onboarding/cubit/onboarding_cubit.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class OnBoardingView extends StatelessWidget {
  static const String routeName = '/onboarding';
  const OnBoardingView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocProvider(
        create: (context) => OnboardingCubit(
          pageViewController: PageController(initialPage: 0),
          auth: context.read(),
        ),
        child: BlocConsumer<OnboardingCubit, OnboardingState>(
          listener: (context, state) {
            if (state.skip) {
              Routes.offAndToNamed("/home");
            }
          },
          builder: (context, state) {
            final bloc = context.read<OnboardingCubit>();
            return Stack(
              alignment: AlignmentDirectional.bottomCenter,
              children: [
                PageView(
                  onPageChanged: bloc.nextPage,
                  controller: bloc.pageViewController,
                  children: onboarding,
                ),
                Container(
                  padding: const EdgeInsets.all(24.0),
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        child: OnBoardingStepper(
                          pages: onboarding.length,
                          currentPage: bloc.state.currentePage,
                        ),
                      ),
                      ClipOval(
                        child: Container(
                          color: Colors.white,
                          child: IconButton(
                            icon: Icon(
                              bloc.state.currentePage >= 2 ? Icons.done : Icons.trending_flat,
                              color: AppColors.kIconColor,
                            ),
                            onPressed: () => bloc.skipPage(),
                            padding: const EdgeInsets.all(13.0),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ],
            );
          },
        ),
      ),
    );
  }
}

final onboarding = <OnBoardingContent>[
  OnBoardingContent(
    title: "Todas las comodidades".toUpperCase(),
    image: "assets/slides/slide_1.png",
    isTop: true,
  ),
  OnBoardingContent(
    title: "Que imaginas.".toUpperCase(),
    image: "assets/slides/slide_2.png",
  ),
  OnBoardingContent(
    title: "En un solo lugar.".toUpperCase(),
    image: "assets/slides/slide_3.png",
  )
];
