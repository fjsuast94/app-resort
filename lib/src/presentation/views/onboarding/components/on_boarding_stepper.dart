import 'package:appresort/src/presentation/core/themes/app_colors.dart';
import 'package:flutter/material.dart';

class OnBoardingStepper extends StatelessWidget {
  final int pages;
  final int currentPage;

  const OnBoardingStepper({
    Key? key,
    required this.pages,
    required this.currentPage,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: _builPageIndicator(),
    );
  }

  List<Widget> _builPageIndicator() {
    List<Widget> list = [];
    for (int i = 0; i < pages; i++) {
      list.add(i == currentPage ? _indicator(true) : _indicator(false));
    }
    return list;
  }

  Widget _indicator(bool isActive) {
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 25.0),
      child: Container(
        height: 5.0,
        width: 40.0,
        decoration: BoxDecoration(
          color: isActive ? AppColors.kPrimaryColor : Colors.white,
          borderRadius: BorderRadius.circular(5.0),
        ),
        margin: const EdgeInsets.only(right: 5.0),
      ),
    );
  }
}
