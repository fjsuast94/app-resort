import 'package:appresort/src/presentation/core/themes/responsive.dart';
import 'package:flutter/material.dart';

class OnBoardingContent extends StatelessWidget {
  final String title;
  final String? subtitle;
  final String image;
  final bool isTop;

  const OnBoardingContent({
    Key? key,
    required this.title,
    required this.image,
    this.subtitle,
    this.isTop = true,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final responsive = Responsive.of(context);
    return Stack(
      alignment: AlignmentDirectional.bottomCenter,
      children: [
        Container(
          width: double.infinity,
          height: responsive.height,
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage(image),
              fit: BoxFit.cover,
            ),
          ),
        ),
        Positioned(
          top: isTop ? responsive.heightPercent(5) : null,
          bottom: isTop ? null : responsive.heightPercent(8),
          child: Center(
            child: Text(
              title,
              style: const TextStyle(
                color: Colors.white,
                fontSize: 30.0,
                fontWeight: FontWeight.w700,
              ),
              textAlign: TextAlign.center,
            ),
          ),
        )
      ],
    );
  }
}
