import 'package:appresort/src/data/usecase/auth.dart';
import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';

part 'onboarding_state.dart';

class OnboardingCubit extends Cubit<OnboardingState> {
  OnboardingCubit({
    required PageController pageViewController,
    required AuthUseCase auth,
  })  : _auth = auth,
        _pageViewController = pageViewController,
        super(OnboardingState());

  final AuthUseCase _auth;
  final PageController _pageViewController;
  PageController get pageViewController => _pageViewController;

  void nextPage(int page) {
    emit(state.copyWith(currentePage: page, skip: false));
  }

  void skipPage() {
    if (_pageViewController.page! > 1) {
      emit(state.copyWith(skip: true));
      _auth.setOnboarding(false);
    } else {
      _pageViewController.nextPage(
        duration: const Duration(milliseconds: 500),
        curve: Curves.ease,
      );
      emit(state.copyWith(skip: false));
    }
  }
}
