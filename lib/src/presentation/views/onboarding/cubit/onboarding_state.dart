part of 'onboarding_cubit.dart';

class OnboardingState {
  OnboardingState({
    this.currentePage = 0,
    this.skip = false,
  });

  final int currentePage;
  final bool skip;

  OnboardingState copyWith({
    int? currentePage,
    bool? skip,
  }) =>
      OnboardingState(
        currentePage: currentePage ?? this.currentePage,
        skip: skip ?? this.skip,
      );
}
