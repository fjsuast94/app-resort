import 'package:appresort/src/presentation/core/navigator/routes.dart';
import 'package:appresort/src/presentation/core/themes/responsive.dart';
import 'package:appresort/src/presentation/core/utils/my_icons.dart';
import 'package:appresort/src/presentation/widgets/sliver/refresh_scroll_view.dart';
import 'package:flutter/material.dart';

final List<Map<String, dynamic>> services = [
  {
    "title": "Pago en línea",
    "icon": "payment",
    "page": "/charge",
  },
  {
    "title": "Reservaciones de áreas comunes",
    "icon": "event",
    "page": "/reservation",
  }
];

class ServiceView extends StatelessWidget {
  static const String routeName = '/services';
  const ServiceView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final responsive = Responsive.of(context);
    return Scaffold(
      appBar: AppBar(
        title: const Text("Servicios"),
      ),
      body: RefreshScrollView(
        child: Container(
          height: responsive.height,
          margin: const EdgeInsets.symmetric(
            vertical: 10,
            horizontal: 10,
          ),
          child: ListView.separated(
            itemCount: services.length,
            itemBuilder: (context, index) {
              final item = services[index];
              return ListTile(
                tileColor: Colors.white,
                title: Text(item['title']),
                onTap: () => Routes.toNamed(
                  item['page'],
                ),
                trailing: MyIcons.name(
                  name: item['icon'],
                ),
              );
            },
            separatorBuilder: (context, index) {
              return const SizedBox(
                height: 10,
              );
            },
          ),
        ),
      ),
    );
  }
}
