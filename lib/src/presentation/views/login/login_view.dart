import 'package:appresort/src/presentation/views/login/components/login_banner.dart';
import 'package:appresort/src/presentation/views/login/components/login_form.dart';
import 'package:appresort/src/presentation/widgets/forms/un_focus_form.dart';
import 'package:flutter/material.dart';

class LoginView extends StatelessWidget {
  static const String routeName = '/login';
  const LoginView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: UnFocusForm(
        child: SingleChildScrollView(
          child: Column(
            children: const [
              LoginBanner(),
              LoginForm(),
            ],
          ),
        ),
      ),
    );
  }
}
