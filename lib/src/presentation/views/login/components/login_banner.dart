import 'package:appresort/src/presentation/widgets/app/informacion.dart';
import 'package:flutter/material.dart';

class LoginBanner extends StatelessWidget {
  const LoginBanner({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final info = AppInformation.of(context);

    return Stack(
      children: [
        info.banner(),
        Positioned(
          top: 30,
          child: info.logo(),
        ),
      ],
    );
  }
}
