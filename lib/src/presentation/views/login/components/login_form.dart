import 'package:appresort/src/presentation/core/navigator/routes.dart';
import 'package:appresort/src/presentation/core/utils/helper.dart';
import 'package:appresort/src/presentation/views/login/bloc/login_bloc.dart';
import 'package:appresort/src/presentation/views/login/components/login_biometric.dart';
import 'package:appresort/src/presentation/views/login/components/login_welcome.dart';
import 'package:appresort/src/presentation/widgets/app/informacion.dart';
import 'package:appresort/src/presentation/widgets/buttons/button_submit.dart';
import 'package:appresort/src/presentation/widgets/forms/input_text_cupertino.dart';
import 'package:appresort/src/presentation/widgets/loading/loading_apleeks.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';

class LoginForm extends StatelessWidget {
  const LoginForm({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => LoginBloc(
        auth: context.read(),
      ),
      child: Builder(
        builder: (context) {
          final bloc = context.read<LoginBloc>();
          return FormBlocListener<LoginBloc, String, String>(
            onSubmitting: (context, state) => LoadingApleeks.show(context),
            onSuccess: (context, state) {
              LoadingApleeks.hide(context);
              bloc.onboarding ? Routes.offAndToNamed('/onboarding') : Routes.offAndToNamed('/home');
            },
            onFailure: (context, state) {
              LoadingApleeks.hide(context);
              Helper.error(message: state.failureResponse!);
            },
            child: BlocConsumer<LoginBloc, FormBlocState>(
              buildWhen: (previous, current) =>
                  previous.runtimeType != current.runtimeType ||
                  previous is FormBlocLoading && current is FormBlocLoading,
              listener: (context, state) async {
                if (state is FormBlocLoaded && !bloc.loaded) {
                  bloc.loaded = true;
                  await Future<void>.delayed(const Duration(milliseconds: 800));
                  if (await bloc.authenticate()) {
                    bloc.onboarding
                        ? Routes.offAndToNamed('/onboarding')
                        : Routes.offAndToNamed('/home');
                  }
                }
              },
              builder: (context, state) {
                return Container(
                  margin: const EdgeInsets.symmetric(
                    vertical: 20,
                    horizontal: 10,
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const LoginWelcome(),
                      InputTextCupertino(
                        textFieldBloc: bloc.username,
                        placeholder: 'Usuario',
                        keyboardType: TextInputType.emailAddress,
                      ),
                      InputTextCupertino(
                        textFieldBloc: bloc.password,
                        placeholder: 'Contraseña',
                        suffixButton: SuffixButton.obscureText,
                        keyboardType: TextInputType.text,
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      Center(
                        child: ButtonSubmit(
                          submit: bloc.submit,
                          text: "Acceder",
                        ),
                      ),
                      const LoginBiometric(),
                      //Informacion.terminos(context),
                      AppInformation.of(context).footer(),
                    ],
                  ),
                );
              },
            ),
          );
        },
      ),
    );
  }
}
