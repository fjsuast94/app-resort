import 'package:appresort/src/presentation/core/themes/responsive.dart';
import 'package:appresort/src/presentation/views/login/bloc/login_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class LoginWelcome extends StatelessWidget {
  const LoginWelcome({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final bloc = context.read<LoginBloc>();
    final responsive = Responsive.of(context);
    return bloc.user.name.isNotEmpty
        ? Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                margin: const EdgeInsets.symmetric(vertical: 10),
                child: Text(
                  "Bienvenido de nuevo",
                  style: TextStyle(
                    fontSize: responsive.inchPercent(2.5),
                    fontWeight: FontWeight.bold,
                    color: Colors.black.withOpacity(0.7),
                  ),
                ),
              ),
              Text(
                bloc.user.name,
                style: TextStyle(
                  fontSize: responsive.inchPercent(2.5),
                  fontWeight: FontWeight.w700,
                ),
              ),
            ],
          )
        : const SizedBox.shrink();
  }
}
