import 'package:appresort/src/presentation/core/navigator/routes.dart';
import 'package:appresort/src/presentation/core/themes/app_colors.dart';
import 'package:appresort/src/presentation/views/login/bloc/login_bloc.dart';
import 'package:appresort/src/presentation/widgets/buttons/button_submit.dart';
import 'package:appresort/src/presentation/widgets/dividers/divider_custom.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';

class LoginBiometric extends StatelessWidget {
  const LoginBiometric({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final bloc = context.read<LoginBloc>();
    return FutureBuilder<bool>(
      future: bloc.isAndroid8OrInferiror(),
      initialData: false,
      builder: (context, AsyncSnapshot<bool> data) {
        return Column(
          children: [
            data.data! && bloc.user.name.isEmpty
                ? SwitchFieldBlocBuilder(
                    booleanFieldBloc: bloc.biometric,
                    padding: EdgeInsets.zero,
                    activeColor: AppColors.kPrimaryColor,
                    controlAffinity: FieldBlocBuilderControlAffinity.trailing,
                    body: Container(
                      alignment: Alignment.centerLeft,
                      child: const Text("Habilitar datos biométricos"),
                    ),
                  )
                : const SizedBox.shrink(),
            data.data! && bloc.hasbiometric
                ? Column(
                    children: [
                      const SizedBox(
                        height: 20,
                      ),
                      const DividerCustom(
                        width: double.infinity,
                        title: "O",
                      ),
                      const SizedBox(
                        height: 30,
                      ),
                      Center(
                        child: ButtonSubmit(
                          submit: () async {
                            if (await bloc.authenticate()) {
                              Routes.toNamed('/home');
                            }
                          },
                          text: "Entrar con datos biométricos",
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ],
                  )
                : const SizedBox.shrink()
          ],
        );
      },
    );
  }
}
