import 'package:appresort/src/data/model/user.dart';
import 'package:appresort/src/data/usecase/auth.dart';
import 'package:appresort/src/presentation/core/utils/validator_string.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';

class LoginBloc extends FormBloc<String, String> {
  LoginBloc({
    required AuthUseCase auth,
  }) : _auth = auth {
    init();
    addFieldBlocs(fieldBlocs: [password]);
  }

  final AuthUseCase _auth;

  late UserModel _user;
  UserModel get user => _user;

  bool onboarding = false;
  bool hasbiometric = false;

  bool loaded = false;
  final username = TextFieldBloc<Object>(
    initialValue: '',
    validators: [
      ValidatorString.required,
      ValidatorString.validateUser,
    ],
  );
  final password = TextFieldBloc<Object>(
    validators: [ValidatorString.required],
  );
  final biometric = BooleanFieldBloc(
    initialValue: true,
  );

  Future<void> init() async {
    _user = _auth.getUser();
    hasbiometric = await _auth.getBiometric();
    onboarding = await _auth.getOnboarding();

    if (await _auth.hasBiometrics()) {
      addFieldBlocs(fieldBlocs: [biometric]);
    }
    if (await _auth.hasUserBiometric()) {
      addFieldBlocs(fieldBlocs: [username]);
    }
    username.updateInitialValue(_user.user);
  }

  Future<bool> authenticate() async {
    return await _auth.authenticate();
  }

  Future<bool> isAndroid8OrInferiror() async {
    return await _auth.isAndroid8OrInferiror();
  }

  @override
  void onLoading() async {
    emitLoaded();
    super.onLoading();
  }

  @override
  void onSubmitting() async {
    final response = await _auth.login(username.value!, password.value!);
    if (response.isEmpty) {
      hasbiometric = !hasbiometric ? biometric.value! : hasbiometric;
      _auth.setBiometric(hasbiometric);
      emitSuccess();
    } else {
      emitFailure(failureResponse: response);
    }
  }
}
