import 'package:flutter/material.dart';
import 'package:appresort/src/presentation/core/navigator/routes.dart';
import 'package:appresort/src/presentation/core/themes/app_colors.dart';
import 'package:appresort/src/presentation/core/utils/helper.dart';
import 'package:appresort/src/presentation/views/initial/cubit/initial_cubit.dart';
import 'package:appresort/src/presentation/widgets/app/informacion.dart';
import 'package:appresort/src/presentation/widgets/loading/loading_apleeks.dart';
import 'package:appresort/src/presentation/widgets/qr/qr_scanner/qr_scanner_view.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class InitialView extends StatelessWidget {
  static const String routeName = '/initial';
  const InitialView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final app = AppInformation.of(context);

    return BlocProvider(
      create: (context) => InitialCubit(
        authCase: context.read(),
      ),
      child: BlocConsumer<InitialCubit, InitialState>(
        listener: (_, state) {
          if (state.status) {
            Routes.offAndToNamed('/login');
          } else {
            Helper.error(message: state.message);
          }
        },
        builder: (context, state) {
          final cubit = context.read<InitialCubit>();
          return Scaffold(
            body: SafeArea(
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    Stack(
                      children: [
                        app.banner(),
                        Positioned(
                          bottom: 10,
                          left: 10,
                          child: app.logo(),
                        ),
                      ],
                    ),
                    const SizedBox(height: 10),
                    Text(
                      "Bienvenido",
                      style: TextStyle(
                        fontSize: 20,
                        color: Colors.black.withOpacity(0.6),
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    const SizedBox(height: 10),
                    const Text(
                      "Escanea el QR de tu desarrollo\npara poder continuar",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    const SizedBox(height: 60),
                    GestureDetector(
                      behavior: HitTestBehavior.translucent,
                      onTap: () async {
                        final qrResponse = await Routes.to(
                          () => const QrScannerView(),
                        );
                        if (qrResponse != null) {
                          LoadingApleeks.show(context);
                          await cubit.getUrlSetting(qrResponse);
                          LoadingApleeks.hide(context);
                        }
                      },
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(80.0),
                        child: Container(
                          height: 120.0,
                          width: 120.0,
                          color: AppColors.kPrimaryColor,
                          child: const Icon(
                            Icons.qr_code_scanner_outlined,
                            color: Colors.white,
                            size: 50.0,
                          ),
                        ),
                      ),
                    ),
                    const SizedBox(height: 10),
                    const Text(
                      "Escanear",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          );
        },
      ),
    );
  }
}
