import 'package:bloc/bloc.dart';

import 'package:appresort/src/data/usecase/auth.dart';
import 'package:appresort/src/presentation/core/utils/validator_string.dart';

part 'initial_state.dart';

class InitialCubit extends Cubit<InitialState> {
  InitialCubit({
    required AuthUseCase authCase,
  })  : _authCase = authCase,
        super(InitialState());

  final AuthUseCase _authCase;

  Future<void> getUrlSetting(String url) async {
    final validator = ValidatorString.urlFormat(url);
    await Future.delayed(const Duration(milliseconds: 800));
    if (validator != null) {
      emit(state.copyWith(message: validator));
      return;
    }

    await _authCase.setUrlBase(url);
    emit(state.copyWith(message: "", status: true));
  }
}
