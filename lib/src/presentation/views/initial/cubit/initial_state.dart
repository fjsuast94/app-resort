part of 'initial_cubit.dart';

class InitialState {
  InitialState({
    this.status = false,
    this.url = "",
    this.message = "",
  });

  final bool status;
  final String url;
  final String message;

  InitialState copyWith({
    bool? status,
    String? url,
    String? message,
  }) =>
      InitialState(
        status: status ?? this.status,
        url: url ?? this.url,
        message: message ?? this.message,
      );
}
