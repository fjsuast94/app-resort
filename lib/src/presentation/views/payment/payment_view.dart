import 'package:appresort/src/data/model/charges_model.dart';
import 'package:appresort/src/presentation/core/themes/app_colors.dart';
import 'package:appresort/src/presentation/core/utils/helper.dart';
import 'package:appresort/src/presentation/views/payment/components/credit_card_view.dart';
import 'package:appresort/src/presentation/views/payment/components/oxxo_pay_view.dart';
import 'package:appresort/src/presentation/widgets/forms/un_focus_form.dart';
import 'package:flutter/material.dart';

class PaymentView extends StatefulWidget {
  static const String routeName = '/payment';
  const PaymentView({
    Key? key,
    required this.charge,
  }) : super(key: key);

  final ChargesModel charge;

  @override
  _PaymentViewState createState() => _PaymentViewState();
}

class _PaymentViewState extends State<PaymentView> with SingleTickerProviderStateMixin {
  late TabController _tabController;
  final List<Tab> _myTabs = <Tab>[
    Tab(
      icon: Image.asset(
        'assets/images/credit_card.png',
        width: 90,
      ),
    ),
  ];

  final List<Widget> _widgets = [];

  @override
  void initState() {
    _widgets.add(
      CreditCardView(
        charge: widget.charge,
      ),
    );

    if (widget.charge.total < 10000) {
      _myTabs.add(
        Tab(
          icon: Image.asset(
            'assets/images/oxxo_pay.png',
            width: 90,
          ),
        ),
      );
      _widgets.add(
        OxxoPayView(
          charge: widget.charge,
        ),
      );
    }

    _tabController = TabController(vsync: this, length: _myTabs.length);
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return UnFocusForm(
      child: Scaffold(
        appBar: AppBar(
          iconTheme: IconTheme.of(context).copyWith(
            color: Colors.white,
          ),
          title: const Text(
            "Proceso de pago",
            style: TextStyle(color: Colors.white),
          ),
          backgroundColor: AppColors.kPrimaryColor,
        ),
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              width: double.infinity,
              color: AppColors.kPrimaryColor,
              child: Column(
                children: [
                  Text(
                    widget.charge.concepto,
                    style: const TextStyle(
                      fontSize: 28,
                      color: Colors.white,
                    ),
                  ),
                  const SizedBox(height: 10),
                  Text(
                    Helper.moneyFormat(widget.charge.total),
                    style: const TextStyle(
                      fontSize: 30,
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  const SizedBox(height: 10),
                  Text(
                    widget.charge.fechaCargo,
                    style: const TextStyle(
                      fontSize: 23,
                      color: Colors.white,
                    ),
                  ),
                  TabBar(
                    tabs: _myTabs,
                    controller: _tabController,
                    indicatorColor: Colors.white,
                  ),
                ],
              ),
            ),
            Expanded(
              child: TabBarView(
                physics: const NeverScrollableScrollPhysics(),
                controller: _tabController,
                children: _widgets,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
