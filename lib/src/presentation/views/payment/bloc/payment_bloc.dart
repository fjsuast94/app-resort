import 'package:appresort/src/data/model/charges_model.dart';
import 'package:appresort/src/data/repository/remote/socket/socket_controller.dart';
import 'package:appresort/src/data/usecase/payment.dart';
import 'package:appresort/src/presentation/core/utils/validator_string.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';

enum PaymentType { oxxo, credit }

class PaymentBloc extends FormBloc<String, String> {
  PaymentBloc({
    required this.paymentType,
    required PageController pageViewController,
    required ChargesModel charge,
    required PaymentUseCase paymentUse,
    required SocketController socket,
  })  : _pageViewController = pageViewController,
        _charge = charge,
        _paymentUse = paymentUse,
        _socket = socket {
    switch (paymentType) {
      case PaymentType.oxxo:
        socket.connect();
        oxxoFieldBloc();
        break;
      case PaymentType.credit:
        socket.connect();
        creditFieldBloc();
        break;
    }
  }

  @override
  Future<void> close() {
    _socket.disconnect();
    return super.close();
  }

  final PaymentType paymentType;
  final PageController _pageViewController;
  PageController get pageViewController => _pageViewController;
  final ChargesModel _charge;
  final PaymentUseCase _paymentUse;
  final SocketController _socket;
  String message = "";

  final name = TextFieldBloc<Object>(
    validators: [
      ValidatorString.required,
    ],
  );

  final none = TextFieldBloc<Object>();

  final email = TextFieldBloc<Object>(
    validators: [
      ValidatorString.required,
      ValidatorString.emailFormat,
    ],
  );
  final phone = TextFieldBloc<Object>(
    validators: [
      ValidatorString.required,
      ValidatorString.validateCelPhoneFormate,
    ],
  );

  final cardNumber = TextFieldBloc<Object>(
    validators: [
      ValidatorString.required,
      ValidatorString.validateCardNumber,
    ],
  );
  final expired = TextFieldBloc<Object>(
    validators: [
      ValidatorString.required,
      ValidatorString.validateCardValidThru,
    ],
  );
  final cvv = TextFieldBloc<Object>(
    validators: [
      ValidatorString.required,
      ValidatorString.validateCardCvv,
    ],
  );

  @override
  void onSubmitting() {
    switch (paymentType) {
      case PaymentType.oxxo:
        oxxoPayment();
        break;
      case PaymentType.credit:
        creditPayment();
        break;
    }
  }

  void back() {
    _pageViewController.previousPage(
      duration: const Duration(milliseconds: 500),
      curve: Curves.ease,
    );
  }

  void next() {
    _pageViewController.nextPage(
      duration: const Duration(milliseconds: 500),
      curve: Curves.ease,
    );
  }

  Future<void> oxxoPayment() async {
    switch (state.currentStep) {
      case 0:
        final response = await _paymentUse.oxxoPago(
          email: email.value!,
          phone: phone.value!,
          name: name.value!,
          total: _charge.total,
          idCharge: int.parse(_charge.id),
          idUnidad: int.parse(_charge.idUnidad),
          idConcepto: int.parse(_charge.idTipoConcepto),
        );
        if (response.status) {
          message = response.message!;
          emitSuccess();
          next();
        } else {
          emitFailure(failureResponse: response.message);
        }
        break;
      case 1:
        emitSuccess();
        break;
    }
  }

  Future<void> creditPayment() async {
    switch (state.currentStep) {
      case 0:
        await Future.delayed(const Duration(seconds: 1));
        emitSuccess();
        next();
        break;
      case 1:
        final expire = expired.value!.split('/');
        final response = await _paymentUse.creditCard(
          cardNumber: cardNumber.value!,
          expirationMonth: expire[0],
          expirationYear: expire[1],
          cvv: cvv.value!,
          email: email.value!,
          name: name.value!,
          phone: phone.value!,
          total: _charge.total,
          idCharge: int.parse(_charge.id),
          idConcepto: int.parse(_charge.idTipoConcepto),
          idUnidad: int.parse(_charge.idUnidad),
        );

        if (response.status) {
          message = response.message!;
          next();
          emitSuccess();
        } else {
          emitFailure(failureResponse: response.message);
        }
        break;
      case 2:
        emitSuccess();
        break;
    }
  }

  void oxxoFieldBloc() {
    addFieldBlocs(
      step: 0,
      fieldBlocs: [email, name, phone],
    );
    addFieldBlocs(
      step: 1,
      fieldBlocs: [none],
    );
  }

  void creditFieldBloc() {
    addFieldBlocs(
      step: 0,
      fieldBlocs: [email, name, phone],
    );
    addFieldBlocs(
      step: 1,
      fieldBlocs: [cardNumber, expired, cvv],
    );
    addFieldBlocs(
      step: 2,
      fieldBlocs: [none],
    );
  }
}
