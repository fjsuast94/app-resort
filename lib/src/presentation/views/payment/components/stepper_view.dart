import 'package:appresort/src/presentation/core/themes/responsive.dart';
import 'package:appresort/src/presentation/views/payment/bloc/payment_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:timeline_tile/timeline_tile.dart';

class StepperView extends StatelessWidget {
  const StepperView({
    Key? key,
    required this.listSteps,
    required this.stepContents,
  }) : super(key: key);

  final List<StepItem> listSteps;
  final List<Widget> stepContents;

  @override
  Widget build(BuildContext context) {
    final responsive = Responsive.of(context);
    final _controller = ScrollController();
    LineStyle beforeLineStyle = const LineStyle(
      color: Colors.green,
    );

    _DeliveryStatus status;
    double indicatorSize = responsive.inchPercent(1);
    final bloc = BlocProvider.of<PaymentBloc>(context, listen: false);

    return Column(children: [
      BlocBuilder<PaymentBloc, FormBlocState>(
        builder: (context, state) {
          return SizedBox(
            height: responsive.inchPercent(10),
            child: ListView.builder(
              scrollDirection: Axis.horizontal,
              itemCount: listSteps.length,
              padding: EdgeInsets.zero,
              controller: _controller,
              itemBuilder: (BuildContext context, int index) {
                final step = listSteps[index];
                if (state.isLastStep) {
                  status = _DeliveryStatus.done;
                  indicatorSize = 30;
                } else if (index == state.currentStep) {
                  status = _DeliveryStatus.doing;
                  indicatorSize = responsive.inchPercent(4);
                } else if (index > state.currentStep) {
                  status = _DeliveryStatus.todo;
                  indicatorSize = responsive.inchPercent(4);
                  beforeLineStyle = const LineStyle(
                    color: Color(0xFF747888),
                  );
                } else {
                  status = _DeliveryStatus.done;
                  indicatorSize = responsive.inchPercent(4);
                }

                return TimelineTile(
                  axis: TimelineAxis.horizontal,
                  alignment: TimelineAlign.manual,
                  lineXY: 0.4,
                  isFirst: index == 0,
                  isLast: index == listSteps.length - 1,
                  beforeLineStyle: beforeLineStyle,
                  indicatorStyle: IndicatorStyle(
                    width: indicatorSize,
                    height: indicatorSize,
                    indicator: _IndicatorDelivery(
                      status: status,
                      icon: step.icon,
                    ),
                  ),
                  endChild: _EndChildDelivery(
                    text: step.title,
                    current: index == state.currentStep,
                  ),
                );
              },
            ),
          );
        },
      ),
      Expanded(
        child: PageView(
          physics: const NeverScrollableScrollPhysics(),
          controller: bloc.pageViewController,
          onPageChanged: (int i) {},
          children: stepContents.map<Widget>((e) {
            return _StepperContent(
              scroll: _controller,
              content: e,
            );
          }).toList(),
        ),
      ),
    ]);
  }
}

enum _DeliveryStatus { done, doing, todo }

class StepItem {
  String title;
  IconData icon;

  StepItem({
    required this.title,
    required this.icon,
  });
}

class _StepperContent extends StatelessWidget {
  final Widget content;
  final ScrollController scroll;

  const _StepperContent({
    Key? key,
    required this.content,
    required this.scroll,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final bloc = BlocProvider.of<PaymentBloc>(context);
    return SingleChildScrollView(
      child: Column(
        children: [
          content,
          Container(
            width: double.infinity,
            margin: const EdgeInsets.symmetric(
              horizontal: 10,
            ),
            child: Row(
              children: <Widget>[
                bloc.state.currentStep > 0 && !bloc.state.isLastStep
                    ? TextButton(
                        onPressed: () {
                          back(3);
                          bloc.back();
                        },
                        child: const Text(
                          "Regresar",
                          style: TextStyle(color: Colors.white),
                        ),
                      )
                    : const SizedBox.shrink(),
                bloc.state.currentStep > 0 && !bloc.state.isLastStep
                    ? const SizedBox(
                        width: 10,
                      )
                    : const SizedBox.shrink(),
                TextButton(
                  onPressed: () {
                    previus(3);
                    bloc.submit();
                  },
                  child: Text(
                    bloc.state.currentStep > 0 && bloc.state.isLastStep ? "Terminar" : "Siguiente",
                    style: const TextStyle(color: Colors.white),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  previus(i) => scroll.animateTo(
        20.0 * i,
        duration: const Duration(seconds: 2),
        curve: Curves.fastOutSlowIn,
      );
  back(i) => scroll.animateTo(
        20.0 / i,
        duration: const Duration(seconds: 2),
        curve: Curves.fastOutSlowIn,
      );
}

class _EndChildDelivery extends StatelessWidget {
  const _EndChildDelivery({
    Key? key,
    required this.text,
    required this.current,
  }) : super(key: key);

  final String text;
  final bool current;

  @override
  Widget build(BuildContext context) {
    return Container(
      constraints: const BoxConstraints(minWidth: 110),
      child: Center(
        child: ConstrainedBox(
          constraints: const BoxConstraints(maxWidth: 110),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Flexible(
                child: Text(
                  text,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: 14,
                    color: current ? const Color(0xFF2ACA8E) : Colors.black.withOpacity(0.6),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class _IndicatorDelivery extends StatelessWidget {
  final _DeliveryStatus status;
  final IconData icon;

  const _IndicatorDelivery({
    Key? key,
    required this.status,
    this.icon = Icons.check,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final responsive = Responsive.of(context);
    switch (status) {
      case _DeliveryStatus.done:
        return CircleAvatar(
          backgroundColor: const Color(0xFF2ACA8E),
          child: Icon(
            Icons.check,
            color: Colors.white,
            size: responsive.inchPercent(2),
          ),
        );
      case _DeliveryStatus.doing:
        return Container(
          decoration: const BoxDecoration(
            shape: BoxShape.circle,
            color: Color(0xFF2ACA8E),
          ),
          child: const Center(
            child: SizedBox(
              height: 20,
              width: 20,
              child: CircularProgressIndicator(
                strokeWidth: 2,
                valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
              ),
            ),
          ),
        );
      case _DeliveryStatus.todo:
        return Container(
          decoration: const BoxDecoration(
            shape: BoxShape.circle,
            color: Colors.grey,
          ),
          child: Center(
            child: Icon(
              icon,
              color: Colors.white,
              size: responsive.inchPercent(2),
            ),
          ),
        );
    }
  }
}
