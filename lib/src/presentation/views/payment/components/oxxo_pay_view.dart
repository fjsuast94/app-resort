import 'package:appresort/src/data/model/charges_model.dart';
import 'package:appresort/src/data/usecase/payment.dart';
import 'package:appresort/src/presentation/core/navigator/routes.dart';
import 'package:appresort/src/presentation/core/themes/responsive.dart';
import 'package:appresort/src/presentation/core/utils/helper.dart';
import 'package:appresort/src/presentation/views/payment/bloc/payment_bloc.dart';
import 'package:appresort/src/presentation/views/payment/components/personal_data_card.dart';
import 'package:appresort/src/presentation/views/payment/components/stepper_view.dart';
import 'package:appresort/src/presentation/views/payment/components/success_payment.dart';
import 'package:appresort/src/presentation/widgets/loading/loading_apleeks.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_bloc/flutter_form_bloc.dart';

class OxxoPayView extends StatelessWidget {
  const OxxoPayView({
    Key? key,
    required this.charge,
  }) : super(key: key);

  final ChargesModel charge;

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => PaymentBloc(
        socket: context.read(),
        charge: charge,
        paymentType: PaymentType.oxxo,
        paymentUse: PaymentUseCase(
          http: context.read(),
          storage: context.read(),
          conekta: context.read(),
        ),
        pageViewController: PageController(initialPage: 0),
      ),
      child: Builder(
        builder: (context) {
          return FormBlocListener<PaymentBloc, String, String>(
            onSubmitting: (context, state) => LoadingApleeks.show(context),
            onSuccess: (context, state) {
              LoadingApleeks.hide(context);
              if (state.stepCompleted == state.lastStep) {
                Routes.back();
              }
            },
            onFailure: (context, state) async {
              LoadingApleeks.hide(context);
              Helper.error(message: state.failureResponse!);
            },
            child: _OxxoViewer(),
          );
        },
      ),
    );
  }
}

class _OxxoViewer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final responsive = Responsive.of(context);
    return SingleChildScrollView(
      child: Column(
        children: [
          SizedBox(
            height: responsive.height,
            child: StepperView(
              listSteps: [
                StepItem(
                  title: "Información personal",
                  icon: Icons.person_add,
                ),
                StepItem(
                  title: "Éxito",
                  icon: Icons.check,
                ),
              ],
              stepContents: const [
                PersonalDataCard(),
                SuccessPayment(),
              ],
            ),
          )
        ],
      ),
    );
  }
}
