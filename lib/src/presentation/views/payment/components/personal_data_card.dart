import 'package:appresort/src/presentation/core/utils/formatters.dart';
import 'package:appresort/src/presentation/views/payment/bloc/payment_bloc.dart';
import 'package:appresort/src/presentation/widgets/forms/input_text_cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class PersonalDataCard extends StatelessWidget {
  const PersonalDataCard({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    final bloc = BlocProvider.of<PaymentBloc>(context);
    return Container(
      margin: const EdgeInsets.symmetric(
        horizontal: 10,
      ),
      child: Column(
        children: [
          InputTextCupertino(
            textFieldBloc: bloc.name,
            placeholder: 'Nombre completo',
            keyboardType: TextInputType.text,
          ),
          InputTextCupertino(
            textFieldBloc: bloc.email,
            placeholder: 'Correo eletrónico',
            keyboardType: TextInputType.emailAddress,
          ),
          InputTextCupertino(
            textFieldBloc: bloc.phone,
            placeholder: 'Número de celular',
            keyboardType: TextInputType.phone,
            inputFormatters: [
              MaskedTextInputFormatter(mask: "### #### ###", separator: " "),
            ],
          ),
        ],
      ),
    );
  }
}
