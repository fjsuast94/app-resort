import 'package:appresort/src/presentation/core/utils/formatters.dart';
import 'package:appresort/src/presentation/views/payment/bloc/payment_bloc.dart';
import 'package:appresort/src/presentation/widgets/forms/input_text_cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class CreditDataCard extends StatelessWidget {
  const CreditDataCard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final bloc = BlocProvider.of<PaymentBloc>(context);
    return Container(
      margin: const EdgeInsets.symmetric(
        horizontal: 10,
      ),
      child: Column(
        children: [
          InputTextCupertino(
            textFieldBloc: bloc.cardNumber,
            placeholder: 'Número de tarjeta',
            keyboardType: TextInputType.phone,
            maxLength: 19,
            maxLengthEnforced: true,
            inputFormatters: [
              MaskedTextInputFormatter(
                mask: 'xxxx xxxx xxxx xxxx',
                separator: ' ',
              ),
            ],
          ),
          Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                child: InputTextCupertino(
                  textFieldBloc: bloc.expired,
                  placeholder: 'MM/YY',
                  keyboardType: TextInputType.phone,
                  maxLength: 5,
                  maxLengthEnforced: true,
                  inputFormatters: [
                    MaskedTextInputFormatter(
                      mask: 'xx/xx',
                      separator: '/',
                    )
                  ],
                ),
              ),
              const SizedBox(
                width: 10,
              ),
              Expanded(
                child: InputTextCupertino(
                  textFieldBloc: bloc.cvv,
                  placeholder: 'CVV',
                  keyboardType: TextInputType.phone,
                  maxLength: 3,
                  maxLengthEnforced: true,
                ),
              ),
            ],
          )
        ],
      ),
    );
  }
}
