import 'package:appresort/src/presentation/core/utils/helper.dart';
import 'package:appresort/src/presentation/views/payment/bloc/payment_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class SuccessPayment extends StatelessWidget {
  const SuccessPayment({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final bloc = BlocProvider.of<PaymentBloc>(context, listen: true);
    return Container(
      margin: const EdgeInsets.symmetric(
        horizontal: 20,
        vertical: 10,
      ),
      child: Column(
        children: [
          const CircleAvatar(
            backgroundColor: Color(0xFF2ACA8E),
            radius: 30,
            child: Icon(
              Icons.check,
              color: Colors.white,
              size: 40,
            ),
          ),
          const SizedBox(height: 20),
          Center(
            child: Text(
              Helper.dateTimeFormat(),
              style: const TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 20,
              ),
              textAlign: TextAlign.center,
            ),
          ),
          const SizedBox(height: 20),
          Center(
            child: Text(
              bloc.message,
              textAlign: TextAlign.center,
            ),
          ),
        ],
      ),
    );
  }
}
