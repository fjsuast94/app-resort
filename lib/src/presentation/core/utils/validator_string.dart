import 'package:flutter_form_bloc/flutter_form_bloc.dart';
import 'package:get/get_utils/get_utils.dart';

class ValidatorString {
  static String? required(String? textField) =>
      (textField == null || textField.isEmpty) ? 'required'.tr : null;

  static String? checkRequired(bool? textField) =>
      (textField == null || !textField) ? 'required'.tr : null;

  static String? emailFormat(String? email) =>
      RegExp(r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$')
              .hasMatch(email!)
          ? null
          : 'email-format'.tr;

  static String? curpFormat(String? curp) =>
      RegExp(r'^([A-Z][AEIOUX][A-Z]{2}\d{2}(?:0[1-9]|1[0-2])(?:0[1-9]|[12]\d|3[01])[HM](?:AS|B[CS]|C[CLMSH]|D[FG]|G[TR]|HG|JC|M[CNS]|N[ETL]|OC|PL|Q[TR]|S[PLR]|T[CSL]|VZ|YN|ZS)[B-DF-HJ-NP-TV-Z]{3}[A-Z\d])(\d)$')
              .hasMatch(curp!)
          ? null
          : 'El formato de la curp es incorrecto';

  static String? rfcFormat(String? curp) =>
      RegExp(r'^([A-ZÑ&]{3,4}) ?(?:- ?)?(\d{2}(?:0[1-9]|1[0-2])(?:0[1-9]|[12]\d|3[01])) ?(?:- ?)?([A-Z\d]{2})([A\d])$')
              .hasMatch(curp!)
          ? null
          : 'El formato del rfc es incorrecto';

  static String? nssFormat(String? nss) => RegExp(r'^(\d{2})(\d{2})(\d{2})\d{5}$').hasMatch(nss!)
      ? null
      : 'El formato del número de seguro social es incorrecto';

  static String? validateUser(String? text) =>
      RegExp(r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$')
              .hasMatch(text!)
          ? null
          : RegExp(r'^\d{10}$').hasMatch(text)
              ? null
              : 'Número o correo no válido';

  static String? validateInputBirthDay(String? textField) =>
      RegExp(r'^(0[1-9]|1[0-9]|2[0-9]|3[0-1])\/(0[1-9]|1[0-2])\/([1-2][0-9][0-9][0-9])$')
              .hasMatch(textField!)
          ? null
          : 'El formato de fecha es incorrecta';

  static String? validateCelPhoneFormate(String? celphone) =>
      RegExp(r'^\d{3}\s\d{4}\s\d{3}$').hasMatch(celphone!) ? null : 'Número de celular no válido';

  static String? validatePhoneFormate(String? celphone) =>
      RegExp(r'^\d{4}\s\d{3}\s\d{3}$').hasMatch(celphone!) ? null : 'Número de teléfono no válido';

  static String? validateCelPhone(String celphone) =>
      RegExp(r'^\d{10}$').hasMatch(celphone) ? null : 'Número de celular no válido';

  static String? validateCodigoPostal(String? textField) =>
      RegExp(r'^\d{5}$').hasMatch(textField!) ? null : 'Código postal no válido';

  static String? validateOnlyNumber(String? textField) => textField!.isEmpty
      ? null
      : RegExp(r'^[0-9]+$').hasMatch(textField)
          ? null
          : 'Sólo número';

  static String? validateText(String textField) =>
      RegExp(r'^[a-zA-Z]').hasMatch(textField) ? null : 'Sólo texto';

  // Validaciones de número de tarjeta
  static String? validateName(String textField) =>
      RegExp(r'^[a-zA-Z]').hasMatch(textField) ? null : 'Ingresa un nombre válido';

  static String? validateContact(String textField) =>
      RegExp(r'^([0-9]+$)|(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$')
              .hasMatch(textField)
          ? null
          : 'Solo número de celular o correo eléctronico';

  static String? validateCardNumber(String? cardNumber) =>
      RegExp(r'^(?:4[0-9]{12}(?:[0-9]{3})?|[25][1-7][0-9]{14}|6(?:011|5[0-9][0-9])[0-9]{12}|3[47][0-9]{13}|3(?:0[0-5]|[68][0-9])[0-9]{11}|(?:2131|1800|35\d{3})\d{11})$')
              .hasMatch(cardNumber!.replaceAll(RegExp(r'\s+\b|\b\s'), ''))
          ? null
          : 'Ingrese un número de tarjeta correcta';

  static String? validateCardMonth(String textField) =>
      (textField.isNotEmpty && int.parse(textField) > 0 && int.parse(textField) < 13)
          ? null
          : 'Mes incorrecto';

  static String? validateCardYear(String textField) =>
      (textField.isNotEmpty && int.parse(textField) > 2000 && int.parse(textField) < 2028)
          ? null
          : 'Año incorrecto';

  static String? validateCardCvv(String? textField) =>
      (textField!.length == 3) ? null : 'CVV incorrecto';

  static String? validateCardValidThru(String? textField) =>
      RegExp(r'^(0[1-9]|1[0-2])\/([2-9][0-9])$').hasMatch(textField!)
          ? null
          : 'Fecha de expiración incorrecto';

  /* static Validator<String> minLength(TextFieldBloc<Object> TextFieldBloc<Object>, int length) =>
      (String textField) => (textField.length < length)
          ? "El campo debe tener minimo $length caracteres"
          : null; */

  static String? maxLength(String textField, int length) =>
      textField.length > length ? "El campo debe tener máximo $length caracteres" : null;

  static String? maxLength8(String textField) =>
      textField.length > 8 ? "El campo debe tener máximo 8 caracteres" : null;

  static String? maxLength6(String? textField) => textField!.length > 7 || textField.length < 7
      ? "El campo debe tener máximo 6 caracteres"
      : null;

  static String? urlFormat(String url) =>
      RegExp(r"^((((H|h)(T|t)|(F|f))(T|t)(P|p)((S|s)?))\://)?(www.|[a-zA-Z0-9].)[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,6}(\:[0-9]{1,5})*(/($|[a-zA-Z0-9\.\,\;\?\'\\\+&amp;%\$#\=~_\-]+))*$")
              .hasMatch(url)
          ? null
          : 'Formato de url no valido'.tr;

  static Validator<String> confirmPassword(TextFieldBloc<Object> password) =>
      (String? passwordConfirm) =>
          (passwordConfirm != password.value) ? "Debe ser igual a la contraseña" : null;
/*
  static Validator<String> confirmPassword(TextFieldBloc<Object> password) =>
      (String passwordConfirm) => (passwordConfirm != password.value)
          ? "Debe ser igual a la contraseña"
          : null; */
}
