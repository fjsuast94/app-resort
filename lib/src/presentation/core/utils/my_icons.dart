import 'package:appresort/src/presentation/core/themes/app_colors.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class MyIcons {
  static Icon name({
    String name = 'home',
    double size = 25.0,
    Color color = AppColors.kPrimaryColor,
  }) {
    Map<String, dynamic> names = {
      'home': Icon(Icons.home, color: color, size: size),
      'person_outline': Icon(Icons.person_outline, color: color, size: size),
      'announcement': Icon(Icons.announcement, color: color, size: size),
      'attach_money': Icon(Icons.attach_money, color: color, size: size),
      'assignment': Icon(Icons.assignment, color: color, size: size),
      'thumbs_up_down': Icon(Icons.thumbs_up_down, color: color, size: size),
      'library_books': Icon(Icons.library_books, color: color, size: size),
      'exit_to_app': Icon(Icons.exit_to_app, color: color, size: size),
      'cake': Icon(Icons.cake, color: color, size: size),
      'monetization_on': Icon(Icons.monetization_on, color: color, size: size),
      'settings': Icon(Icons.settings, color: color, size: size),
      'attach_file': Icon(Icons.attach_file, color: color, size: size),
      'send': Icon(FontAwesomeIcons.paperPlane, color: color, size: size),
      'activities': Icon(Icons.work, color: color, size: size),
      'stats': Icon(Icons.leaderboard, color: color, size: size),
      'info': Icon(Icons.info, color: color, size: size),
      'gavel': Icon(Icons.gavel, color: color, size: size),
      'bookmark': Icon(FontAwesomeIcons.bookmark, color: color, size: size),
      'balance_scale': Icon(FontAwesomeIcons.balanceScale, color: color, size: size),
      'photo_album': Icon(Icons.photo_album, color: color, size: size),
      'headset_mic': Icon(Icons.headset_mic, color: color, size: size),
      'support_agent': Icon(Icons.support_agent, color: color, size: size),
      'menu_book': Icon(Icons.menu_book, color: color, size: size),
      'live_help': Icon(Icons.live_help, color: color, size: size),
      'payment': Icon(Icons.payment, color: color, size: size),
      'download_sharp': Icon(Icons.download_sharp, color: color, size: size),
      'work': Icon(Icons.work, color: color, size: size),
      'receipt': Icon(Icons.receipt, color: color, size: size),
      'receipt_long': Icon(Icons.receipt_long, color: color, size: size),
      'trending_up': Icon(Icons.trending_up, color: color, size: size),
      'trending_down': Icon(Icons.trending_down, color: color, size: size),
      'event': Icon(Icons.event, color: color, size: size),
      'book': Icon(Icons.book, color: color, size: size),
      'card_giftcard': Icon(Icons.card_giftcard, color: color, size: size),
      'room_service': Icon(Icons.room_service, color: color, size: size),
      'golf_course': Icon(Icons.golf_course, color: color, size: size),
      'beach_access': Icon(Icons.beach_access, color: color, size: size),
      'restaurant': Icon(Icons.restaurant, color: color, size: size),
      'spa': Icon(Icons.spa, color: color, size: size),
      'hotel': Icon(Icons.hotel, color: color, size: size),
      'directions_car': Icon(Icons.directions_car, color: color, size: size),
      'local_bar': Icon(Icons.local_bar, color: color, size: size),
      'pool': Icon(Icons.pool, color: color, size: size),
      'new_releases': Icon(Icons.new_releases, color: color, size: size),
      'touch_app_outlined': Icon(Icons.touch_app_outlined, color: color, size: size),
      'post_add_outlined': Icon(Icons.post_add_outlined, color: color, size: size),
      'monetization_on_outlined': Icon(Icons.monetization_on_outlined, color: color, size: size),
      'attach_money_outlined': Icon(Icons.attach_money_outlined, color: color, size: size),
      'vpn_key_outlined': Icon(Icons.vpn_key_outlined, color: color, size: size),
    };
    return names[name] ?? Icon(Icons.home, color: color, size: size);
  }
}
