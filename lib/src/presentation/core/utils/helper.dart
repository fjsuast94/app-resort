import 'dart:math';

import 'package:appresort/src/presentation/shared/logger/logger_utils.dart';
import 'package:flutter/material.dart';
import 'package:get/route_manager.dart';
import 'package:intl/intl.dart';
//import 'package:get/get_utils/get_utils.dart';

class Helper {
  static const String _title = 'App Resort';
  static const int _duration = 3000;

  static Future<void> success({
    String title = _title,
    required String? message,
    int duration = _duration,
  }) async {
    if (!Get.isSnackbarOpen) {
      Get.snackbar(
        title,
        message!,
        duration: Duration(milliseconds: duration),
        backgroundColor: Colors.green.withOpacity(0.8),
        colorText: Colors.white,
      );
    }
  }

  static Future<void> error({
    String title = _title,
    required String message,
    int duration = _duration,
  }) async {
    if (!Get.isSnackbarOpen) {
      Get.snackbar(
        title,
        message,
        duration: Duration(milliseconds: duration),
        backgroundColor: Colors.red.withOpacity(0.8),
        colorText: Colors.white,
      );
    }
  }

  static Future<void> warning({
    String title = _title,
    required String message,
    int duration = _duration,
  }) async {
    if (!Get.isSnackbarOpen) {
      Get.snackbar(
        title,
        message,
        duration: Duration(milliseconds: duration),
        backgroundColor: Colors.yellow.withOpacity(0.8),
        colorText: Colors.white,
      );
    }
  }

  /* static Future<void> launchInBrowser(String url) async {
    url.isURL
        ? await canLaunch(url)
            ? await launch(url, forceSafariVC: false)
            : Helper.error(message: 'No se pudo acceder a esta url $url')
        : Helper.error(message: 'Favor de ingresar una url valida');
  } */

  static String moneyFormat(dynamic money) {
    final formatCurrency = NumberFormat.simpleCurrency();
    return formatCurrency.format(money);
  }

  static String dateTimeFormat() {
    DateTime now = DateTime.now();
    return DateFormat('yyyy-MM-dd – kk:mm').format(now);
  }

  static String numberFormat(dynamic number) {
    if (number == 0.0) {
      return "0.00";
    }

    final formatCurrency = NumberFormat("###.00");
    return formatCurrency.format(number);
  }

  static String dateFormat(String? date) {
    if (date == null || date.isEmpty) {
      return "Fecha no disponible";
    }

    final inputFormat = DateFormat('yyyy-MM-dd');
    final inputDate = inputFormat.parse(date);
    final outputFormat = DateFormat('dd/MM/yyyy');
    final outputDate = outputFormat.format(inputDate);
    return outputDate;
  }

  static String dateFormatHours(String? date) {
    if (date == null || date.isEmpty) {
      return "Fecha no disponible";
    }

    final inputFormat = DateFormat('yyyy-MM-dd HH:mm');
    final inputDate = inputFormat.parse(date);
    final outputFormat = DateFormat('HH:mm');
    final outputDate = outputFormat.format(inputDate);
    return outputDate;
  }

  static String dateFormatBackendFrontend(String date) {
    try {
      List<String> _fechArray = date.split("-");
      return "${_fechArray[2]}/${_fechArray[1]}/${_fechArray[0]}";
    } on RangeError catch (e) {
      debugPrint(e.toString());
      return date;
    }
  }

  static int getAnioByDate(String date) {
    if (date.isEmpty) {
      return 0;
    }
    List<String> _fechArray = date.split("-");
    final DateTime now = DateTime.now();
    return now.month >= int.parse(_fechArray[1])
        ? now.year - int.parse(_fechArray[0])
        : now.year - int.parse(_fechArray[0]) - 1;
  }

  static String dateFormatFrontendBackend(String date) {
    if (date.isEmpty) {
      return "";
    }

    try {
      List<String> _fechArray = date.split("/");
      return "${_fechArray[2]}-${_fechArray[1]}-${_fechArray[0]}";
    } on RangeError catch (e) {
      debugPrint(e.toString());
      return date;
    }
  }

  static String cellphoneFormate(String cellphone) {
    if (cellphone.isEmpty) {
      return "";
    }

    try {
      List<String> _cellphones = cellphone.split(" ");
      return "${_cellphones[0]}${_cellphones[1]}${_cellphones[2]}";
    } on RangeError catch (e) {
      debugPrint(e.toString());
      return cellphone;
    }
  }

  static String replaceString(String string, String pattern) {
    if (string.isEmpty) {
      return "";
    }

    try {
      return string.replaceAll(pattern, string);
    } on RangeError catch (e) {
      debugPrint(e.toString());
      return string;
    }
  }

  static String cellphoneFormateServe(String cellphone) {
    if (cellphone.isEmpty) {
      return "";
    }

    try {
      final String first = cellphone.substring(0, 4);
      final String middle = cellphone.substring(4, 7);
      final String last = cellphone.substring(7, 10);
      return "$first $middle $last";
    } on RangeError catch (e) {
      debugPrint(e.toString());
      return cellphone;
    }
  }

  static String stringUppercase(String data) {
    return data.toUpperCase();
  }

  static int intParse(String number) {
    try {
      return int.parse(number);
    } on FormatException catch (e) {
      Logger.write(e.message);
      return 0;
    }
  }

  static int randInt({int minLimit = 1, int maxLimit = 100}) {
    Random random = Random();
    return random.nextInt(maxLimit) + minLimit;
  }
}
