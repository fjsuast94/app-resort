import 'package:appresort/src/presentation/core/utils/helper.dart';
import 'package:appresort/src/presentation/views/assemblies/assemblie_view.dart';
import 'package:appresort/src/presentation/views/balances/balance_view.dart';
import 'package:appresort/src/presentation/views/charges/charge_view.dart';
import 'package:appresort/src/presentation/views/estado_cuenta/estado_cuenta_view.dart';
import 'package:appresort/src/presentation/views/home/home_view.dart';
import 'package:appresort/src/presentation/views/initial/initial_view.dart';
import 'package:appresort/src/presentation/views/login/login_view.dart';
import 'package:appresort/src/presentation/views/notifications/notification_view.dart';
import 'package:appresort/src/presentation/views/onboarding/onboarding_view.dart';
import 'package:appresort/src/presentation/views/posts/post_view.dart';
import 'package:appresort/src/presentation/views/regulations/regulation_view.dart';
import 'package:appresort/src/presentation/views/reservation/reservation_view.dart';
import 'package:appresort/src/presentation/views/services/service_view.dart';
import 'package:appresort/src/presentation/views/settings/setting_view.dart';
import 'package:appresort/src/presentation/views/splash/splash_view.dart';
import 'package:appresort/src/presentation/views/tickets/ticket_view.dart';
import 'package:flutter/material.dart';
import 'package:get/route_manager.dart';

class Routes {
  static List<GetPage> routes = [
    GetPage(
      name: SplashView.routeName,
      page: () => const SplashView(),
    ),
    GetPage(
      name: LoginView.routeName,
      page: () => const LoginView(),
    ),
    GetPage(
      name: HomeView.routeName,
      page: () => const HomeView(),
    ),
    GetPage(
      name: OnBoardingView.routeName,
      page: () => const OnBoardingView(),
    ),
    GetPage(
      name: RegulationView.routeName,
      page: () => const RegulationView(),
    ),
    GetPage(
      name: PostView.routeName,
      page: () => const PostView(),
    ),
    GetPage(
      name: BalanceView.routeName,
      page: () => const BalanceView(),
    ),
    GetPage(
      name: EstadoCuentaView.routeName,
      page: () => const EstadoCuentaView(),
    ),
    GetPage(
      name: ChargeView.routeName,
      page: () => const ChargeView(),
    ),
    GetPage(
      name: ServiceView.routeName,
      page: () => const ServiceView(),
    ),
    GetPage(
      name: SettingView.routeName,
      page: () => const SettingView(),
    ),
    GetPage(
      name: NotificationView.routeName,
      page: () => const NotificationView(),
    ),
    GetPage(
      name: AssemblieView.routeName,
      page: () => const AssemblieView(),
    ),
    GetPage(
      name: TicketView.routeName,
      page: () => const TicketView(),
    ),
    GetPage(
      name: ReservationView.routeName,
      page: () => const ReservationView(),
    ),
    GetPage(
      name: InitialView.routeName,
      page: () => const InitialView(),
    ),
  ];

  static toNamed(
    String name, {
    dynamic arguments,
    String message = "El módulo no esta disponible o no tienes acceso a ello.",
  }) {
    routes.any((e) => e.name == name)
        ? Get.toNamed(
            name,
            arguments: arguments,
            preventDuplicates: false,
          )
        : Helper.error(
            message: message,
          );
  }

  static offAndToNamed(
    String name, {
    dynamic arguments,
    String message = "El módulo no esta disponible o no tienes acceso a ello.",
  }) {
    routes.any((e) => e.name == name)
        ? Get.offAndToNamed(
            name,
            arguments: arguments,
          )
        : Helper.error(
            message: message,
          );
  }

  static offAllNamed(
    String name, {
    dynamic arguments,
    String message = "El módulo no esta disponible o no tienes acceso a ello.",
  }) {
    routes.any((e) => e.name == name)
        ? Get.offAllNamed(
            name,
            arguments: arguments,
          )
        : Helper.error(
            message: message,
          );
  }

  static Future<dynamic> fullDialog(dynamic page, {dynamic arguments}) async {
    return await Get.to(
      page,
      arguments: arguments,
      fullscreenDialog: true,
    );
  }

  static Future<dynamic> to(dynamic page, {dynamic arguments}) async {
    return await Get.to(
      page,
      arguments: arguments,
    );
  }

  static Future<dynamic> back({dynamic result}) async {
    return Get.back(
      result: result,
    );
  }

  static Future<dynamic> dialog(
    Widget widget, {
    bool barrierDismissible = true,
    Color? barrierColor,
    bool useSafeArea = true,
    bool useRootNavigator = true,
    Object? arguments,
    Duration? transitionDuration,
    Curve? transitionCurve,
    String? name,
    RouteSettings? routeSettings,
  }) async {
    return await Get.dialog(
      widget,
      barrierDismissible: barrierDismissible,
      barrierColor: barrierColor,
      useSafeArea: useSafeArea,
      arguments: arguments,
      transitionDuration: transitionDuration,
      transitionCurve: transitionCurve,
      name: name,
      routeSettings: routeSettings,
    );
  }
}
