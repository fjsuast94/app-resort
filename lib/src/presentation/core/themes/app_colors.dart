import 'package:flutter/material.dart';

class AppColors {
  static const kPrimaryColor = Color(0xFFC4A27D);
  static const kPrimaryLightColor = Color(0xFFEAD4A3);
  static const kPrimaryGradientColor = LinearGradient(
    begin: Alignment.topLeft,
    end: Alignment.bottomRight,
    colors: [kPrimaryColor, kPrimaryLightColor],
  );

  static const kSecondColor = Color(0xFFEAD4A3);
  static const kTextColor = Color(0xFFA3886C);
  static const kIconColor = Color(0xFFA3886C);
  static const kCancel = Color(0xFFFF4040);

  static const kHeaderCircle = Color.fromRGBO(255, 255, 255, 0.17);
  static const kHeaderBlueDark = Color.fromRGBO(255, 255, 255, 0.17);
  static const kHeaderBlueLight = Color.fromRGBO(255, 255, 255, 0.17);
  static const kHeaderGreyLight = Color.fromRGBO(225, 255, 255, 0.31);
  static const kGreenBackground = Color.fromRGBO(181, 255, 155, 0.36); // 36%

  static const kAnimationDuration = Duration(milliseconds: 200);

  static const headingStyle = TextStyle(
    fontSize: 24.0,
    fontWeight: FontWeight.bold,
    color: Colors.black,
    height: 1.5,
  );
}
