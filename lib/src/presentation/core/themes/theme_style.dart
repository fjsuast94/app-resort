import 'package:appresort/src/presentation/core/themes/app_colors.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class ThemeStyle {
  static late BuildContext _context;
  static init(BuildContext context) {
    _context = context;
  }

  static ThemeData _themeData = ThemeData();
  static const ColorScheme colorSchemeLight = ColorScheme(
    primary: AppColors.kPrimaryColor,
    secondary: Colors.amber,
    surface: AppColors.kPrimaryColor,
    background: AppColors.kPrimaryColor,
    brightness: Brightness.light,
    error: AppColors.kPrimaryColor,
    onBackground: Colors.black,
    onError: Colors.white,
    onPrimary: Colors.white,
    onSecondary: Colors.white,
    onSurface: Colors.black,
  );

  static final lightTheme = ThemeData.light().copyWith(
    appBarTheme: AppBarTheme(
      color: AppColors.kPrimaryColor,
      centerTitle: false,
      elevation: 0.0,
      iconTheme: const IconThemeData(
        color: Colors.white,
      ),
      toolbarTextStyle: GoogleFonts.abel(
        color: Colors.white.withOpacity(0.9),
      ),
      titleTextStyle: GoogleFonts.abel(
        color: Colors.white,
        fontSize: 20,
      ),
    ),
    //primaryColorBrightness: Brightness.light,
    //brightness: Brightness.light,
    textTheme: GoogleFonts.abelTextTheme(
      Theme.of(_context).textTheme.apply(
            bodyColor: AppColors.kTextColor.withOpacity(0.9),
          ),
    ),
    buttonTheme: ButtonThemeData(
      buttonColor: AppColors.kPrimaryColor,
      textTheme: ButtonTextTheme.primary,
      colorScheme: Theme.of(_context).colorScheme.copyWith(
            secondary: Colors.white,
          ), // Text color
    ),
    elevatedButtonTheme: ElevatedButtonThemeData(
      style: ButtonStyle(
        backgroundColor: MaterialStateProperty.all<Color>(
          AppColors.kPrimaryColor,
        ), //button color
        foregroundColor: MaterialStateProperty.all<Color>(
          const Color(0xffffffff),
        ), //text (and icon)
      ),
    ),
    outlinedButtonTheme: OutlinedButtonThemeData(
      style: ButtonStyle(
        side: MaterialStateProperty.all<BorderSide>(
          const BorderSide(color: Colors.white),
        ), //text (and icon),
        backgroundColor: MaterialStateProperty.all<Color>(
          Colors.transparent,
        ), //button color
        foregroundColor: MaterialStateProperty.all<Color>(
          const Color(0xffffffff),
        ), //text (and icon)
      ),
    ),
    textButtonTheme: TextButtonThemeData(
      style: ButtonStyle(
        backgroundColor: MaterialStateProperty.all<Color>(
          AppColors.kPrimaryColor,
        ), //button color
        foregroundColor: MaterialStateProperty.all<Color>(
          const Color(0xffffffff),
        ), //text (and icon)
      ),
    ),
    tabBarTheme: const TabBarTheme(
      labelColor: AppColors.kPrimaryColor,
    ),
    backgroundColor: const Color(0xFFF9F9F9),
    cardColor: const Color(0xFFFFFFFF),
    bottomAppBarColor: const Color(0xFFFFFFFF),
    primaryColorLight: const Color(0xFFF5F5F5),
    primaryColorDark: const Color(0xFFEEEEEE),
    primaryColor: AppColors.kPrimaryColor,
    iconTheme: const IconThemeData(
      color: AppColors.kIconColor,
      size: 18,
    ),
    floatingActionButtonTheme: const FloatingActionButtonThemeData(
      foregroundColor: AppColors.kPrimaryColor,
      splashColor: Colors.transparent,
      disabledElevation: 0,
      elevation: 0,
    ),
    //colorScheme: colorSchemeLight,
  );
  static final darkTheme = ThemeData.dark().copyWith(
    appBarTheme: AppBarTheme(
      color: Colors.white,
      centerTitle: false,
      elevation: 0.0,
      iconTheme: const IconThemeData(
        color: AppColors.kPrimaryColor,
      ),
      toolbarTextStyle: GoogleFonts.abel(
        color: Colors.white,
      ),
      titleTextStyle: GoogleFonts.abel(
        color: Colors.white,
      ),
    ),
    textTheme: GoogleFonts.abelTextTheme(
      Theme.of(_context).textTheme.apply(
            bodyColor: Colors.white,
          ),
    ),
    buttonTheme: ButtonThemeData(
      buttonColor: AppColors.kPrimaryColor, // Background color (orange in my case).
      textTheme: ButtonTextTheme.accent,
      colorScheme: Theme.of(_context).colorScheme.copyWith(secondary: Colors.white), // Text color
    ),
    elevatedButtonTheme: ElevatedButtonThemeData(
      style: ButtonStyle(
        backgroundColor: MaterialStateProperty.all<Color>(
          AppColors.kPrimaryColor,
        ), //button color
        foregroundColor: MaterialStateProperty.all<Color>(
          const Color(0xffffffff),
        ), //text (and icon)
      ),
    ),
    outlinedButtonTheme: OutlinedButtonThemeData(
      style: ButtonStyle(
        side: MaterialStateProperty.all<BorderSide>(
          const BorderSide(color: Colors.white),
        ), //text (and icon),
        backgroundColor: MaterialStateProperty.all<Color>(
          Colors.transparent,
        ), //button color
        foregroundColor: MaterialStateProperty.all<Color>(
          const Color(0xffffffff),
        ), //text (and icon)
      ),
    ),
    textButtonTheme: TextButtonThemeData(
      style: ButtonStyle(
        backgroundColor: MaterialStateProperty.all<Color>(
          AppColors.kPrimaryColor,
        ), //button color
        foregroundColor: MaterialStateProperty.all<Color>(
          const Color(0xffffffff),
        ), //text (and icon)
      ),
    ),
    tabBarTheme: const TabBarTheme(
      labelColor: Color(0xFFFFFFFF),
    ),
    backgroundColor: const Color(0xFF303030),
    bottomAppBarColor: const Color(0xFFFFFFFF),
    cardColor: const Color(0xFF333333),
    primaryColorLight: const Color(0xFF505050),
    primaryColorDark: const Color(0xFF404040),
    primaryColor: AppColors.kPrimaryColor,
  );

  //static MediaQueryData _mediaQuery;

  static ThemeData getTheme(BuildContext context) {
    //_mediaQuery = MediaQuery.of(context);
    //_themeData = _mediaQuery.platformBrightness == Brightness.light ? lightTheme : darkTheme;
    _themeData = lightTheme;
    return _themeData;
  }
}
